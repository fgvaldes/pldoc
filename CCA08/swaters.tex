\documentclass{article}
\usepackage{natbib}

\textwidth=6.25in
\oddsidemargin=0.0in
\evensidemargin=0.0in

\title{The NOAO High-Performance Pipeline System:\\Pipeline Applications}
\author{R. A. Swaters\thanks{Department of Astronomy, University of Maryland, College Park, MD 20742},
F. Valdes
\thanks{National Optical Astronomy Observatories, P.O. Box 26732, Tucson, AZ 85732}
}

\begin{document}

\maketitle

\title{.}

\begin{abstract}
The data taken with the optical and near-infrared wide-field imagers
at the 4-meter telescopes of the National Observatory are processed
with the NOAO High-Performance Pipeline System.  These pipelines
consists of two distinct components: a Quick Reduce Pipeline that
reduces data in near real-time at the telescope, and a Science
Pipeline that provides in-depth data reduction at the end of an
observing block. Here, we describe the distributed nature of the NOAO
Pipeline System, the calibration data that are applied, the data
quality metadata that are derived, and the data products that are
delivered.
\end{abstract}

\section{Introduction}

The NOAO High-Performance Pipeline System (which is described in
detail in an accompanying extended abstract submitted by Frank Valdes)
is used to process data obtained with the optical MOSAIC Camera and
the near-intrared NEWFIRM camera of the National Optical Astronomy
Observatory (NOAO). These instruments are used at the 4-meter
telescopes at Kitt Peak National Obsevatory (KPNO) near Tucson,
Arizona, and the Cerro Tololo Inter-American Observatory (CTIO) in
Chile.

The NOAO Mosaic Cameras (on each at KPNO and CTIO) consist of 8
$2048\times 4096$ CCD detectors arranged in a $4\times 2$ pattern in
the focal plane to provide a field of view of approximately
$0.5^\circ\times 0.5^\circ$ with a pixel size of 0.25''.

The NOAO Extremely Wide-Field Infrared Imager (NEWFIRM) is a 1 to 2.4
micron IR camera for both the Kitt Peak and CTIO 4m telescopes. The
focal plane of NEWFIRM consists of four 2048 by 2048 detectors laid
out in a 2 by 2 mosaic. The field of view is 27.6' by 27.6'. The pixel
size is 0.4''.

Each exposure taken with the MOSAIC camera produces a data file that
is a factor of four larger than each data file taken with the NEWFIRM
camera. However, because the data-taking cadence is much higher with
NEWFIRM than with MOSAIC, the nightly data flow is usually higher for
NEWFIRM.

The data processing carried out by the NOAO pipelines consists of two
components: a Quick Reduce Pipeline (QRP) that reduces data in near
real-time at the telescope (currently only for NEWFIRM), and a Science
Pipeline (SP) that provides in-depth data reduction at the end of an
observing block (for both NEWFIRM and MOSAIC data).

The goals of the NOAO pipelines are:

\begin{itemize}
\item Produce reduced data at the telescope, in near-real time (the
  Quick Reduce Pipeline; QRP)
\item Provide uniformly reduced data to the observer shortly after
  completion of an observing run (the Science Pipeline)
\item Populate an NOAO archive with uniformly reduced data products
  from the NOAO NEWFIRM images
\item Provide a facility to monitor, both for each exposure and as
  long-term trends, the data quality of NOAO data
\end{itemize}

\section{A Distributed Pipeline System}

Both the QRP and the SP make use of the NOAO High-Performance Pipeline
System (Scott et al.\ 2006; see also extended abstract submitted to
CCA08 by Frank Valdes). This system is designed to run on a cluster of
computers, and it allows easy and efficient use of CPU resources in
problems with inherent parallelism such as the processing of
observations from mosaic cameras.

In the pipeline system, the data reduction process has been organized
into a hierarchical structure of different individual pipelines. Each
of these pipelines deals with one aspects of the reduction process
e.g., constructing calibration data (such as darks, dome flats, or
master sky flats), applying these calibration data, collecting the
final data products, and controlling other pipelines. All of these
pipelines, in turn, are subdivided into a set of modules, each of
which carries out a small step of the data reduction process. These
modules can be written in any language. Because the pipeline system
uses the Image Reduction and Analysis Facility (IRAF, developed at
NOAO), most modules are IRAF scripts.

All of the pipelines and associated modules can be configured to run
on all available nodes (or any subset thereof), ensuring maximum use
of available resources.  Whenever a pipeline is to be started, a node
selection algorithm is called to find the best node by considering
issues such as load and minimizing network traffic.  Distribution of
the work across the nodes in the cluster also depends on the nature of
the steps in the data reduction process, and processing may be
distributed for example by detector or by multi-extension fits (MEF)
files.  An example of distribution by detector is the calculation of
the master sky flats from a group of observations. In this case, the
data from N different detectors are sent to N different
nodes, each of which will process the data from one unique CCD. When
applying calibration data to MEF files, however, it is more efficient
to leave the individual CCDs on the same node, because this
significantly reduces transfer of data between nodes. In this case,
the data are distributed across nodes by MEF files.

To make sure available resources are used efficiently, any node can
run multiple instances of a pipeline. Thus, on a node with two CPUs,
two instances of a pipeline that apply calibration data to science
observations will run in parallel.

\section{Calibration}

Both the QRP and SP apply the major calibrations to the raw data:

\begin{itemize}
\item Apply a correction for the inherently nonlinear nature of the
  NIR detectors
\item Removal of the signature of electronic cross-talk between
  individual amplifiers in the array (for MOSAIC)
\item Individual bias, dome flat, and twilight flat observations are
  checked and combined
\item Subtract the bias and dark structure
\item Apply flat fields
\item Flag any bad pixels (saturated pixels or persistent images)
\item For object exposures, the world coordinate system and a rough
  photometric zeropoint are determined by matching objects in the
  field against the USNO-B catalog
\end{itemize}

For the QRP, the emphasis is on speed of processing while still
producing data that are reduced thoroughly and allow the users to
evaluate the quality and depth of their data. Some time-consuming
reduction steps are left out of the QRP but are part of the Science
pipeline:

\begin{itemize}
\item 2nd pass sky subtraction (masks are used to remove objects
  when exposures are combined to create sky images; for NEWFIRM only)
\item Removal of pupil ghost images and fringes
\item Construction of a master sky flat to correct for differences in
  illumination of the detector and differences in color between the
  dome flats. It is created by combining suitable science exposures
  with objects masked out and divided into the data
\end{itemize}

\noindent The pupil ghost, fringe, and dark sky flat calibration data
are derived by combining and filtering large groups of exposures. This
optimal grouping is determined as part of the pipeline. The default is
to group exposures by night are preferred, but groups spanning shorter
or longer time intervals are used as needed, depending on the number
of available exposures.  Not all exposures are suitable to determine
the pupil ghost, fringe and dark sky calibration data. How suitable
exposures are selected is described in detail in Valdes \& Swaters
(2007).

\begin{itemize}

\item{The pupil ghost is caused by reflections off the secondary mirror. A
pupil ghost template is constructed by combining suitable science
exposures with the objects masked out. This template is then scaled
for each exposure and subtracted}

\item{The fringe template is also created by combining suitable science
exposures with objects masked out, but large scale structures are
also removed by subtracting a median filtered version of the
combined images. The resulting template is then scaled for each
exposure and subtracted}

\item{The construction of a master sky flat to correct for differences in
illumination of the detector and differences in color between the
dome flats. It is created by combining suitable science exposures
with objects masked out and divided into the data}

\end{itemize}

\noindent All calibration images that are created by the pipeline are
stored in the pipeline calibration database, along with a date range
over which these calibration data are useful. These calibration data
can be retrieved by the pipeline if it is not possible to construct
the necessary calibration data from a given dataset. Calibration data
from this database can be used, for example, if a dome flat for a
particular filter is not available, or if there are too few exposures
to construct e.g., a fringe template or a dark sky flat.

\section{Data Quality}

The NOAO Mosaic Camera Pipeline verifies, checks, and characterizes
the data being processed throughout the reduction process, and the
resulting metadata is stored in a database. This is being done for
several reasons:

\begin{itemize}
\item{Immediately after ingesting the data, the fits files and headers
  are verified and data that cannot be processed are
  rejected. Examples are: corrupt or incomplete fits files, or fits
  files with missing critical header keywords. Whenever possible,
  missing keywords are reconstructed.}
\item{Statistics and other numerical characterizations of individual
  calibration exposures are compared against expected values and
  outliers are rejected.}
\item{Critical metrics that help users evaluate the quality of the
  data, such as seeing, photometric depth, sky level, and WCS are
  recorded. In the case of the QRP, these metrics are presented to the
  user both for the most recent data, and also for prior observations
  for trending purposes.}
\end{itemize}

For the Science pipeline, a more exhaustive set of metrics is measured
for calibration and science data. This metadata, derived from the raw
data, intermediate steps, and the final pipeline data products are all
stored in the pipeline metadata database. This database can be queried
by the pipeline itself to carry information from one module to the
next, but it also provides a means to investigate long term trends in
the instrument's performance.

\section{Rules}

During the reduction process the pipeline needs to make decisions on
the fly. In the NOAO pipelines, these decisions have been separated
from the code in the modules.  Configuration files control
syntactically simple decision (e.g., boolean variables, or comparing
variables against a threshold). These files can be used to configure
the pipeline in a global sense, and also for specific datasets.  Rules
can be more complex in nature, and can consist of complex scripts that
take several variables as input. An example is the evaluation of dome
flats, in which the statistics are evaluated differently depending on
the filter.

\section{Data products}

After the data have been fully calibrated, the data are reprojected to
the same orientation and pixel size, and to the closest tangent point
on a predefined grid (thus ensuring that spatially close exposures
have the same tangent point). These exposures are then combined into
one stacked image, which could span a large field of view, go deep, or
both.  The original reduced image, the resampled one, and the stacked
one are end products of the pipeline.  In addition, the pipeline
produces masks for each of these.

\section{Performance}

The NOAO Mosaic Camera Pipeline can fully reduce and create all data
products at a rate of on average 35 Mosaic images per hour on a
cluster of 8 nodes (each with dual 3.0 GHz Xeons), and also 1 node
(with a dual Xeon 2.8 GHz) which hosts the calibration and metadata
database. The processing rate depends on the nature of the
observations and ranges from 30 exposures per hour to as high as 50
images per hour. The average of 35 MOSAIC images per hour corresponds
to approximately 5.0 GB per hour, or 0.12 TB per day.

\begin{thebibliography}{}
\bibitem[~]{} Scott, D. et al. 2006, ASP Conf. Series, Vol. 376, p. 265
\bibitem[~]{} Valdes, F., \& Swaters, ASP Conf. Series, Vol. 376, p. 273

\end{thebibliography}

\end{document}
