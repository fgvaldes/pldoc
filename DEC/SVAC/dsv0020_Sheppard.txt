From: "Scott S. Sheppard" <sheppard@dtm.ciw.edu>
Date: April 15, 2012 8:13:12 PM PDT
To: decamsv@ctio.noao.edu
Subject: DECam Proposal: Beyond the Kuiper Belt Edge


Sheppard, Scott, sheppard@dtm.ciw.edu, DTM Carnegie
Trujillo, Chad, trujillo@gemini.edu, Gemini Observatory


Beyond the Kuiper Belt Edge


Science Justification

The dynamical and physical properties of small bodies offer one of the key constraints on the formation and evolution of our solar system and migration of the planets. The Kuiper Belt is thought to be a relic from the original protoplanetary disk. It contains some of the least processed material in the solar system. The Kuiper Belt has been found to be dynamically structured with several observed dynamical classes.

Currently there are over 1000 Kuiper Belt objects known with perihelion between about 30 and 50 AU. Only one object is known to have a perihelion significantly beyond 50 AU (Sedna at 76 AU). How was the Kuiper Belt edge and Sedna formed? Sedna is an intermediate object between the Kuiper Belt and more distant Oort cloud. Though unexpected, the discovery of Sedna has given us a new understanding of our solar system's chaotic history (Brown et al. 2004). Further Sedna type objects need to be discovered to allow us to understand this populations dynamics and size to gain insight into the formation of our solar system (Schwamb et al. 2010; Kaib et al. 2011). Just like in the Kuiper Belt, the orbital dynamics and surface properties of a large number of Sedna type objects will constrain their origins and the formation mechanics of the solar system.

Sedna's large perihelion distance (76 AU) along with its large eccentricity (e=0.84) are hard to explain with what we currently know about the solar system. If Sedna formed in its current location it must have initially been on a circular orbit otherwise accretion would not have been possible because of the large relative velocities (Stern 2005). If Sedna obtained its large eccentricity through interactions with the currently known giant planets, somehow its perihelion must have been raised. Galactic tides are too weak to raise the perihelion of an object like Sedna and only work for distant Oort cloud objects around 10,000 AU. Several theories on Sedna's history have been put forth and most are observationally testable by discovering more Sedna type objects (see Morbidelli and Levison 2004). These theories range from Sedna scattering from an unseen planet, scattering from a stellar encounter or even being a captured planetesimal from another solar system.

Based on the main asteroid belt and Kuiper Belt populations few Sedna like objects are expected in the 1000 km size range, of which Sedna is one, but there should be orders of magnitudes more Sedna like objects with radii in the 100 km range. The strong size and heliocentric distance dependence of the optical flux density of sunlight scattered from a solid object requires a survey to obtain faint magnitudes (r > 24) in order to access the population of objects of radii 100 km beyond 50 AU. Most Kuiper Belt surveys to date have focused on close bodies (<50 AU) and have either covered large areas but been to shallow depths (< 23th mag) or have gone deep but covered a very small area of sky (a few square degrees).

A deep wide-field outer solar system survey, which we propose for DECam, will allow us to determine if the objects beyond 50 AU are fainter than expected, if there is truly a dearth of objects, or if the Kuiper Belt continues again after some sizable gap possibly caused by a planet sized object. The proposed survey would be the deepest widest survey for such distant objects ever obtained. We will be able to probe the origin of Sedna and determine if this eccentric, distant body is unique (as once believed for Pluto) or just the first of a new class of object in the outer Solar System. If so what are the dynamical and size distribution of the population? This will help us determine the Sedna population origins and possibly the formation environment of our Sun.

Through a deep, wide-field survey we wish to make a definitive observational statement about the Sedna population: 1) Does it exist?  2) Is it significantly larger than the Kuiper Belt's? 3) Is there a large population of high inclination versus low inclination objects?  4) Do they have similar orbital dynamics such as perihelion distance or longitude of perihelion? 5) What is the size distribution?

The survey will also access the scattered disk population, high inclination population, Neptune Trojans and possibly find additional members of the Haumea Kuiper Belt collisional family. Scattered disk objects have perihelia close to Neptune and thus were likely scattered out through Neptune interactions. The scattered disk objects size distribution and total numbers are still uncertain (Gomes et al. 2008). We will detect several scattered disk objects beyond 50 AU and determine their dynamics and population size. This will help determine if the scattered disk population is consistent with supplying the short-period comets. Objects discovered with inclinations around 28 degrees could be members of the Haumea Kuiper Belt collisional family (Ragozzine and Brown 2007). Only eight Neptune Trojans are known (most found by us in previous observations) and this survey will determine if the faint ones exist at all, which may not be the case (Sheppard and Trujillo 2010).



Technical Justification:

The main reason only one Sedna-like object is known is probably because most surveys of the outer Solar System don't efficiently detect objects beyond the Kuiper Belt edge because they either don't go faint enough, don't have the required long cadence or covered too small of an area of sky. Taking into account the only object known with a perihelion well beyond 50 AU and assuming a Kuiper Belt like size distribution, we expect about one Sedna-like object brighter than 24.5 mags about every 50 square degrees. This is of course based on very low number statistics and is the main reason we are proposing this survey. We expect to find one Sedna-like object every 50 square degrees and if not, we can put a 95 percent confidence limit that the Sedna-like population is smaller than the Kuiper Belt population if no Sedna-like objects are found in 150 square degrees to 24.5 mags.  Currently, it is believed that the Sedna-like population is likely larger than the Kuiper Belt population (Brown et al. 2004), so this would be a strong constraint that this is not true.

The outer solar system objects of interest that we want to discover will likely be around 24.5 mags in the r-band. Integration times must be short in order to prevent trailing losses in the moving objects.  Based on experience with the 4 meter MOSAIC imagers at CTIO and Kitt Peak and using the exposure time calculator for DECam, it will take about 360 seconds to reach 24.5 magnitudes in the r-band with a SNR of 5. This assumes moderate seeing conditions and moderate moon with 1x1 binning. Our object detection software has routinely found objects in past surveys with SNR around 3.5. Our detection software is highly optimized to find very faint slow moving objects.

At opposition and at the distance of Sedna an object will appear to move about 1.5 arcseconds per hour. We want to probe out to at least 300 AU which is the distance Pluto would be when its 24.5 magnitudes.  An object at this distance would move about 0.5 arcseconds per hour.  Thus our cadence between the first and last image of each field must be at least three hours in order to have distant outer solar system objects move multiple pixels between images. If there is one 1000km Sedna at 80 AU we expect many more 100 km Sedna-like bodies at 80 AU.

The DECam imager covers about 3 square degrees of sky in one field.  We would like to cover at least 50 square degrees to 24.5 mags in the r-band to likely find a Sedna-like object, but would want to eventually cover 150 square degrees of sky to put 95 percent confidence constraints on the Sedna-like population compared to the Kuiper Belt population if a null result is obtained. We need three observations of each field to allow our detection algorithm to find moving objects efficiently (two images are not adequate because noise fluctuations will cause too many false positives, hundreds per chip versus a handful per chip with three images). We need 17 fields to cover 50 square degrees (or 50 fields to cover 150 square degrees), 3 times each, at 7 minutes per exposure including overheads and readout.  We thus require about 6 hours (or 18 hours) of telescope time with the DECam imager to obtain our minimum goal of covering 50 (or 150 total goal) square degrees to 24.5 magnitudes. We thus request 6 hours minimum (or 18 hours maximum) of dark time with the DECam imager on the CTIO 4 meter to survey for Sedna-like objects beyond the Kuiper Belt edge.

Since our objects of interest are moving so slow, one night arcs would allow the object to have an uncertainty of less than a few arcminutes for one or two months. Normally we would recover interesting objects one or two months after discovery in order to allow the object to have an uncertainty of only a few arcminutes a year later. After one year we can determine if an object's orbit is Sedna-like or not. At discovery we can tell an objects inclination and heliocentric distance but require a year arc to determine the objects eccentricity and semi-major axis. As S. Sheppard has access to the du Pont 2.5 meter telescope and Magellan telescopes at Las Campanas we will recover anything interesting there after discovery. As DECam has such a wide-field, recovery a few months after discovery is not very important as the uncertainty a year later based on only the discovery images will be about 1 degree, still easily recovered by DECam a year later. All needed recovery will be done at Las Campanas in order to allow the more important wide field of view telescope to not waste time with recovery and thus focus on obtaining the all important survey fields.



DECam System Testing:

Our survey will test DECam in several ways. We will be discovering hundreds of Kuiper Belt objects in this survey, though only a few Sedna-like objects are expected because they are much more distant and thus fainter. The discovery of these outer solar system objects will test the image quality of the CCDs and PSF across the field, as we should find similar numbers of KBOs per CCD if all CCDs have similar image quality.

Astrometry of all detected sources, including all stars and outer solar system objects, will be determined through our data analysis.  This will allow us to recover some of these objects using the 2.5 meter du Pont telescope and 6.5 meter Magellan telescopes at Las Campanas. Recovery and orbit fitting of these objects will tell us how well the astrometry is at DECam. Astrometry over the whole field will allow us to determine chip distortions how the PSF varies over the whole field.

As our observations need to image each field three times over at least a three hour period but can be up to a 24 hour period, we will test how well the telescope comes back to the exact same spot on the sky and how will the images compare for the same exact field at different airmasses and hour angles. This will also test the repeatability of guiding on the exact same field at different times.

As we require the telescope to move after each 360 second integration, there is a lot of telescope movement. This telescope movement can be just a few degrees to the next adjacent field or tens of degrees if needed for testing. This rapid and constant movement will test the ability of the telescope to move during readout of the image and acquire guide stars and be ready to image in a timely manner. It will measure the open shutter efficiency and measure the overhead of the telescope.

Finally, we note that the public is highly interested in solar system science. In our survey we are likely to find the most distant object ever discovered in our solar system and will be pushing the boundaries of our known solar system. This will easily capture the publics imagination and be a nice write-up in a popular magazine.


Reduction and Analysis Plans:

We have extensive experience using wide-field mosaic imagers to find outer solar system objects. We have found hundreds of Kuiper Belt objects, tens of new outer satellites of Jupiter, Saturn, Uranus and Neptune as well as four of the eight known Neptune Trojans.  C. Trujillo is one of the co-discoverers of Sedna as well as Eris (sometimes called the tenth planet) and most of the known large KBOs.

We have a robust pipeline that quickly reduces CTIO and Kitt Peak 4 meter MOSAIC data. Our algorithm reduceds the data and finds all sources in an image, does astrometry for all the sources and finds all the moving objects of interest. Our experience with the CTIO and Kitt Peak 4 meter MOSAIC cameras found us able to reduce and search a full nights data before the start of the next night. Thus by the time the observing run was over the full data set had been reduced and searched for interesting objects. With DECam, we would expect to be able to analyze one night of data in near real time. As noted above, recovery can wait up to one year, so quick access to the data is not imperative, but would be nice.


Scott S. Sheppard
Department of Terrestrial Magnetism
Carnegie Institution of Washington
5241 Broad Branch Rd. NW
Washington, DC 20015
phone: 202-478-8854
sheppard@dtm.ciw.edu
