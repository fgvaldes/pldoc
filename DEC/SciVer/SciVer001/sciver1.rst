==============================
Science Verification Report #1
==============================
-----------------------
F. Valdes, May 18, 2012
-----------------------

Summary
=======

Based on visual examination of a single detrended exposure the basic
detrending appears satisfactory.  Crosstalk, masking, saturation, and
bleed trail handling is good.  The small problems I noted were bad
pixels indicated at the amplifier joins and some false masking which
may be related to bleed trail thresholds.

The flat fielding is hard to evaluate since relative gain
normalization of the CCDs from the dome flat are not included in the
version tested.  The illumination correction was applied and, in this
case, doesn't have much effect.  However, visually it has some artifacts
which are concerning.

The NSA conversions for bias and detrended images was checked and used for
the verification.  Dome flat, illumcor, and other products are for a future
version.  The file structure is validated.  The metadata is largely yet
to be done and those fields which are filled in have FITS syntax errors.

CP Version
==========

This report is based on CP 1.4.0 installed at NOAO.

Data
====

This report is based on processing of Mosaic2 g and r data from 2004-09-12
to 2004-09-17.  The specific image is ct053848 from a g band exposure.

Visual Examination
==================

Cross-talk
----------

Cross-talk is properly removed.  Figure 1 shows the detrended exposure
at the top and the raw exposure on the bottom.

Bleed Trail Detection, Masking, and Replacement
-----------------------------------------------

Detection of bleed trails, getting the detections into the mask, and
replacing by interpolation is working.  Figure 2 shows this where the
bottom is the raw, the middle is detrended with the mask overlayed,
and the top without the overlay.

I saw one example that probably indicates some threshold tuning is
required.  Figure 3 shows something marked that does not appear to be
the signature of a bleed trail.

Masking
-------

I find that the mask has flagged pixels at the amplifier joins.  This
appears at all the amplifier joins I looked at.  Figure 4 shows and
example as does figure 2.

Illumination Correction
-----------------------

The illumination correction for the g band is shown in figure 5. As
mentioned in the summary, the effect of this correction on this
particular data is small.  It must be acknowledged that improvements to
the illumination correction are planned for a future version.  This report
notes something that is not related to the change (filtering of expsures
going into the correction).  In particular, the vertical stripes (other
than potential half-CCD gain differences from the amplifiers) cannot be
representative of actual illumination effects.  The rest of the structures
are reasonable.  For comparison, figure 6 is the illumination correction
derived from the same data by the Mosaic pipeline.  The figures are
reasonably similar if we ignore the stripes.  So the things to look at
are a) what causes the stripes and b) should the smoothing parameters or
algorithm be tweaked to be smoother.
