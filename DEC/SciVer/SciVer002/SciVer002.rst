==============================
Science Verification Report #2
==============================
------------------------
F. Valdes, July 27, 2012
------------------------

Summary
=======

*Significant improvements in CP 1.5 for basic flux calibration.*

Verified the dome flat gain normalization is applied.  Verified the
basic flux calibration through illumination correction looks good
in this relatively uncrowded, and not very complex, set of exposures
for illumination correction.

Questions are raised about what happens to bad columns and in the fringe
template at the amplifier joins.  It was previously noted that amplifier
gaps are flagged in the masking which is likely the source of the fringe
template behavior.

Change Requests
===============

1.  Don't flag the amplifier joins unless there really is a problem
    with bad data.  Check if this also eliminates the behavior in the
    fringe template reported below.

CP Version
==========

This report is based on CP 1.5.0 installed at NOAO.

Data
====

This report is based on processing of Mosaic2 g and r data from 2004-09-12
to 2004-09-17.  The specific data is ct046404 from a g band exposure.

Visual Examination
==================

All the verification noted here was done by visual inspection of a
single dataset and a single exposure.

Dome Flat Fielding
------------------

The exposure after crosstalk and detrending was displayed with all
extensions having the same display scaling.  This is shown in figure 1.
The image looks nice with no noticible gain differences between amplifiers
or CCDs.  Good work!

Bleed Trail Detection, Masking, and Replacement
-----------------------------------------------

As reported previously, displaying an exposure after bleed trail
masking showed good handling of this effect.  I report this as part
of the systematic examination of an image after each processing step.
Again, good work on the bleed trail handling!

Fringe and Illumination Generation
----------------------------------

A fringe template and illumination flat field were created from the dataset.
These are shown in figure 2.  As a caveat, it is understood that work is
continuing on a change request this process to appear in a future release.

There is no noticible fringing in the fringe template.  Whether it is
appropriate to do fringe correction on this band and, in the case of DECam,
whether a fringe correction will be needed for any filter is an operational
issue.

There was one concern noted for the fringe template.  Looking at
figure 3, which is full scale around the amplifier gap in CCD 1, we
can see the location of the gap.  This should not be the case and could
potentially introduce an artifact in the data, though in this case it
is not noticible in the calibrated data.  My interpretation, based on
an earlier report, is that the amplifier join is entered as bad in the
masking and then the fringe generation avoids this region in the same
way as the other feature noted in figure 2.  I also wonder if the
fringe generation code should produce such a wide feature around bad
columns.

The illumination correction is reasonable and is improved over
processing of the same field in CP 1.4 as shown in figure 4 taken from the
previous report.  It shows the stripes are not present and it looks
similar to the Mosaic pipeline version.  Good work!

One item from a previous change request is whether the fringe template
and illumination dome flat should be derived in one step; that is,
separating low and high frequency structures from the dark sky stack. 
The alternative is to extract the fringe template from the dark sky
stack, apply it to all the exposures, make a new dark sky stack, and
then generate the illumination flat field.  Of course, in the absence
of fringing there is not difference.

Fringe and Illumination Corrections
-----------------------------------

The fringe and illumination corrections to an exposure appear to be
correctly applied as seen in figure 5.  The question I have is why the
bad column disappears in the process?

Figures
=======

**Figure 1: Full field after crosstalk and detrending with biases and
dome flats generated from the dataset.**

.. image:: detrend.jpg

**Figure 2: Fringe template and illumination flat field generated from
the dataset.**

.. image:: FringeIllum.jpg

**Figure 3: Small region of the fringe template from CCD 1.**

.. image:: ampgap.jpg

**Figure 4: Illumination flat fields from CP 1.4 and the NOAO Mosaic
pipeline from the same dataset.**

.. image:: sciver1fig5.jpg
.. image:: sciver1fig6.jpg

**Figure 5: Before and after fringe and illumination correction showing
the disappearance of a bad column.**

.. image:: badcol.jpg
