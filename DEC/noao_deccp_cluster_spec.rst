==================================================================
Dark Energy Camera Community Pipeline Cluster Derived Requirements
==================================================================
--------------
Joshua Hoblitt
--------------

Introduction
============

Requirements commented on below were derived from the [DRAFT hardware
requirements document](https://desweb.cosmology.illinois.edu/confluence/display/CP/DRAFT+hardware+requirements+document)

This is not the only document in which hardware requirements have been identified
but it is the one that Irene Barg identified as the document to reference for
requirements

Other known places where a requirement or specification is listed:

* [NOAO Community Pipeline Requiremnts][https://desweb.cosmology.illinois.edu/confluence/download/attachments/19071772/NOAO_CommunityPipelineRequirements.pdf) under section "1.e Processing time" lists the the CP system will reduce a full night in 12 hours on roughly 20x2GHz processors.

* [DECam Community Pipeline Software Requirements and Technical Specifications](https://desweb.cosmology.illinois.edu/confluence/download/attachments/12421072/community_pipeline_v3.1.pdf) under section "6.a. Cluster of Linux OS Computers" lists the baseline configuration as 4 nodes with 8cores@2.5GHz and 8GB of RAM.

Size of the compute cluster
===========================

Number of nodes
---------------

Specifies 22 "Lonestar4" like compute nodes with 12 cores @ 3.33GHz.  A simple
web search turns up this [University of Texas press
release](http://www.utexas.edu/news/2010/08/30/tacc_lonestar_upgrade/) that
specifies this system as using "Westmere" Xeon 5600 series CPUs.

The based on the stated information about product series and clock rate the
CPUs used are likely the [Intel "Westmere-EP" Xeon
X5680](http://ark.intel.com/products/47916/Intel-Xeon-Processor-X5680-%2812M-Cache-3_33-GHz-6_40-GTs-Intel-QPI%29).

The NOAO system was built with Intel Xeon X5675s.  Which is the same
"Westmere-EP" core as the X5680 but at a lesser base clock rate of 3.06GHz.
This was a conscious design decision because the specified max TDP (Thermal
Design Power) for the X5680 is 130W while the same rating for the X5675 is
95W.  Environmental regulation of the NOAO computer room is a concern during
the summer months and this difference in CPU parts reduces theoretical max
power density by 70W per U of cabinet space.

Repeating the Abe to Lonestar4 naive scaling calculation but with the clock
rate of the X5675s gives us:

    | ceiling(40 / (3.06 / 2.33 * 11 / 7 ) * 22/18) 
    |                    = 24 NOAO class nodes (288 cores)

The NOAO deccp cluster at present has 15 compute nodes (180 cores) or is ~62% of the
requested cluster size.  It is planned that the system will be supplemented
during the current subsequent fiscal year on a 'as-needed' basis based on
testing of the delivered CP codes.

    *Summary: meets ~62% of requirement; will be expanded as needed*

Memory per core
---------------

3GB per core is requested.  The NOAO system has 48GB per compute node of which
8GB is permanently allocated as a GPFS cache. 

    | (48 - 8) / 12 = 3.33GB / core

*Summary: meets requirement*

IO to the compute cluster
=========================

1.5GB/sec of parallel IO is specified.  IOPS, sequential vs random I/O, etc. is
not stated.  It was assumed this number refers to highly sequential reads and
writes of relatively large fits files.  Naive testing of the aggregate
throughput from 4 compute nodes with the standard utility `dd` gives the
numbers:

    | write: 1581 MB/s
    | read:  1905 MB/s

The actual performance will be highly application dependent.  It is
anticipated the I/O subsystem will need to be supplemented and funds have been
allocated for this during the present fiscal year.  The cost of storage has
already dropped substantially since the NOAO system was acquired during the high
of the Thailand flooding disk crisis so there is strong motivation to differ
this cost.

    *Summary: meets requirement under synthetic test conditions*

Shared Filesystem Sizes
=======================

The requirement is listed as

    | 0.5TB + 0.2TB + (4.5TB + 1TB) * nights_campaign

Frank has stated a preference for the nights per campaign to be defined as 4.
Yielding a total space requirement of 22.7TB.  Presuming that a TB is defined
as 2^40 bytes, the NOAO system provides ~18TiB of storage meeting 79% of the
spec.   This is planned to be doubled by the end of the current fiscal year.

    *Summary: meets 79% of requirment; expansion planned*

Database machine
================

References 8 core/16GB memory and 2? (suspect typo; likely should be 12)
core@3.2GHz/48 systems as being adequate.  Specs 1TB of storage.

The NOAO system database host (decdb1) is 2 x Intel X5675s (12 cores) and 48GB
of ram identical to the compute nodes.  It has an LSI 9265 RAID controller w/
BBU and 10 x Western Digital WD6000BLHX disks in a raid 1/0 array + 1 hot
spare.  Total capacity is 2.4TiB.

    *Summary: meets requirement*
