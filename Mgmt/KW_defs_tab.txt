FITS Keyword Definitions for NOAO Imaging Devices											
Version 2008-Nov-21											
Revised 2009-Jan-25: Valdes											
Name	Location	Internal DataType	Display Format	Units	Default	Origin	Case Sensitive?	Remed?	FITS Comment	Description/Comment	Query Case
AIRMASS	PHDU	float	%6.4f	none	Null	Raw		TBD	Airmass	Pathlength through the atmosphere of target at observation start	
ASTRMCAT	PHDU	string(68)	%32s	none	"""N/A"""	Pipeline	no	No	Astrometric catalog used for WCS solution		no
BIASFIL	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Bias structure reference file name		yes
DQMASK	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Name of corresponding Data Quality Mask		yes
BUNIT	PHDU	string(68)	see FITS 3.0	none	""" """	Pipeline	yes	Yes	Brightness units	"Brightness units for the image pixel array, which is ""adu"" for raw data; ""adu/s"" for calibrated data"	yes
CDi_j	EHDU	double	%20g	none	Null	Raw		TBD	WCS transformation matrix	Linear transformation matrix (with scale factor) betw pixel axes j and coordinate axes i. Maximum of WCSAXES**2 terms for sky images. 	
CORNnDEC	EHDU	double	%20g	deg	Null	Remed		Yes	Dec of image corners	"Declination of image corners 1-4, where CORN1RA, CORN1DEC are the RA, Dec coordinates of image corner 1, etc."	
CORNnRA	EHDU	double	%20g	deg	Null	Remed		Yes	RA of image corners	"Right ascension of image corners 1-4, where CORN1RA, CORN1DEC are the RA, Dec coordinates of image corner 1, etc."	
CRPIXi	EHDU	int	%6d	pixel	Null	Raw		TBD	WCS coordinate of reference point	Coordinate of the reference point along pixel axis i. Maximum of WCSAXES keywords. 	
CRVALi	EHDU	double	%20g	deg	Null	Raw		TBD	WCS coordinate value at reference point of axis i.	Maximum of WCSAXES keywords. 	
CTYPEi	EHDU	string(8)	%8s	none	""" """	Raw	no	TBD	WCS coordinate type	Type for intermediate coordinate axes i. Maximum of WCSAXES keywords.	no
DARKFIL	PHDU	string(68)	%64s	none	""" """	Pipeline	yes	No	Name of dark reference file		yes
DATE-OBS	PHDU	DateTime	ISO8601	none	Null	Raw		Yes	Date/Time of exposure start		no
DEC	PHDU	double	dd:mm:ss	deg	Null	Raw		TBD	Declination of image center	Sexagesimal: corresponds roughly to center of FPA	
DOMEFLAT	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Name of dome flat-field reference file		yes
FLATFIL	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Primary flat-field reference file		yes
DS_IDENT	PHDU	string(68)	%64s	none	""" """	Remed	yes	Yes	ADS dataset identifier	Identifier used by Astrophysics Data System to refer to this dataset; see STR 2007-02.	yes
DTACQNAM	PHDU	string(68)	%64s	none	""" """	iSTB	no	No	File name supplied at telescope	File name supplied by observer during observations	no
DTCALDAT	PHDU	string(10)	yyyy-mm-dd	none	""" """	iSTB	no	No	DTS calendar date of observation	"Date of observation in local timezone, with noon pivot"	no
DTINSTRU	PHDU	string(68)	%16s	none	""" """	iSTB	no	Yes	DTS instrument designation		no
DTNSANAM	PHDU	string(68)	%68s	none	""" """	iSTB	no	No	NSA unique file identifier		no
DTOBSERV	PHDU	string(8)	%8s	none	""" """	iSTB	no	Yes	DTS observatory designation	Institution responsible for scheduling this observation; one of: NOAO|WIYN|SOAR|SMARTS	no
DTPI	PHDU	string(68)	%40s	none	""" """	iSTB	no	No	Name of PI for observing program	Name of Principal Investigator for program specified in DTPROPID	no
DTPIAFFL	PHDU	string(68)	%64s	none	""" """	iSTB	no	No	PI affiliation	PI institutional affiliation	no
DTPROPID	PHDU	string(16)	%16s	none	""" """	iSTB	no	Yes	DTS proposal identification	"Observing program designation, taken from observing schedule for semester ""S"" [A|B]. Strict format yyyyS-nnnn applies only for NOAO proposals."	no
DTSITE	PHDU	string(8)	%2s	none	""" """	iSTB	no	Yes	DTS observatory location designation	One of: ct|cp|kp	no
DTTELESC	PHDU	string(8)	%8s	none	""" """	iSTB	no	Yes	DTS telescope designation	DTS abbreviation for telescope used to obtain these data	no
DTTITLE	PHDU	string(68)	%68s	none	""" """	iSTB	no	No	Title of observing program	Title of program specified in DTPROPID	no
DTUTC	PHDU	DateTime	ISO8601	none	Null	iSTB		Yes	DTS time of datafile creation	DateTime following completion of file creation	
EFFTIME	PHDU	float	%8.2f	s	Null	Pipeline		No	Effective exposure time	A scaled or normalized exposure time that applies to this image	
EQUINOX	PHDU	float	%6.1f	yr	2000.0	Raw		Yes	Epoch of mean equator and equinox	Always '2000.0'	
EXPTIME	PHDU	float	%8.2f	s	Null	Raw		Yes	Duration of exposure		
EXTNAME	EHDU	string(68)	%8s	none	""" """	Raw	yes	No	Name of FITS extension HDU	Value uniquely identifies FITS extensions	yes
FILTER	PHDU	string(68)	%20s	none	""" """	Raw	yes	Yes	Name of filter in use	"May contain short name, description, and serial number"	yes
FILTID	PHDU	string(16)	%10s	none	""" """	Remed	no	Yes	Unique filter identification	Filter serial number	no
HA	PHDU	double	hh:mm:ss.ss 	Hr	Null	Raw		TBD	Angle E or W of meridian	Sexagesimal: angle at observation start	
INSTRUME	PHDU	string(68)	%20s	none	""" """	Raw	no	Yes	Instrument used to collect these data	"Appears for select instruments, incl. NEWFIRM"	no
MAGZERO	EHDU	float	%6.2f	none	Null	Pipeline		No	Magnitude corresponding to 1 ADU	Added by pipeline	
MIMETYPE	PHDU	string(32)	%40s	none	""" """	Pipeline	yes	No	Mimetype of this data file	"One of: ""image/fits""|""application/fits""|""image/png"""	yes
MJD-OBS	PHDU	double	%13.7f	d	Null	Raw		Yes	MJD of observation start	"Modified Julian Day, including fractional day, derived from DATE-OBS"	
NAXIS	All	int	%3d	none	Null	Raw		Yes	Dimensionality of data array	"Generally 2 for images, but must be >= WCSAXES."	
NAXISi		int	%8d	none	Null	Raw		Yes	Number of elements along dimension i	Must be non-negative; i increases from most to least rapidly varying array index.There are NAXIS indexes. 	
OBJECT	PHDU	string(68)	%64s	none	""" """	Raw	no	No	PI designation of target		no
OBS-ELEV	PHDU	float	%6.1f	m	Null	Remed		Yes	Observatory elevation	Accurate to ~10m	
OBS-LAT	PHDU	double	dd:mm:ss	deg	Null	Remed		Yes	Observatory latitude	Accurate to ~1 arcsec	
OBS-LONG	PHDU	double	ddd:mm:ss	deg	Null	Remed		Yes	Observatory East longitude	Accurate to ~1 arcsec	
OBSERVAT	PHDU	string(68)	%8s	none	""" """	Raw	no	Yes	Observatory name		no
OBSTYPE	PHDU	string(32)	%16s	none	""" """	Raw	no	Yes	Type of target observed	One of: object|zero|bias|dark|flat|dome flat|sky flat	no
PHOTBW	PHDU	float	%7.2f	nm	Null	Pipeline		Yes	Photometric bandpass width		
PHOTCLAM	PHDU	float	%7.2f	nm	Null	Pipeline		Yes	Bandpass central wavelength		
PHOTDPTH	EHDU	float	%5.2f	none	Null	Pipeline		No	Photometric depth of this image		
PIPELINE	PHDU	string(68)	%32s	none	""" """	Pipeline	no	No	Name of calibration pipeline	"Blank unless PROCTYPE is not ""Raw"""	no
PLVER	PHDU	string(68)	%64s	none	""" """	Pipeline	no	No	Version ID of pipeline	"Blank unless PROCTYPE is not ""Raw"""	no
PHOTFWHM	PHDU	float	%6.2f	nm	Null	Pipeline		Yes	Bandpass full-width at half-max.		
PROCTYPE	PHDU	string(16)	%20s	none	""" """	Pipeline	no	Yes	Processing type	Processing type for this data product. One of: Raw|InstCal|MasterCal|Resampled|Stacked	no
PRODTYPE	PHDU	string(16)	%20s	none	""" """	Pipeline	no	Yes	Type of data product	Logical organization of this data product. One of: image|png|mask|expmask	no
RA	PHDU	double	hh:mm:ss.ss	Hr	Null	Raw		TBD	Right ascension of image center	Sexagesimal: corresponds roughly to center of FPA	
RADESYS	PHDU	string(8)	%8s	none	FK5	Remed	yes	Yes	Principal coordinate reference frame	One of: FK5|ICRS	yes
RAWFILE	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Name of raw source file		yes
SB_RECNO	PHDU	int	%10d	none	Null	iSTB		Yes	DTS sequential record number	"This kw replaces RECNO, but is not present in early image headers"	
SEEING	PHDU	float	%4.2f	arcsec	Null	Pipeline		No	Average FWHM of point sources		
SFLATFIL	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Sky flat-field reference file		yes
SKYFLAT	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Name of sky flat-field reference file	"Sky delta-flat, which is a multiplicative correction to the dome flat."	yes
TELESCOP	PHDU	string(32)	%20s	none	""" """	Raw	no	Yes	Telescope used to collect these data		no
TIME-OBS	PHDU	Time	hh:mm:ss.ss	none	Null	Raw		Yes	Time of observation start	Sexagesimal	
TIMESYS	PHDU	string(3)	%3s	none	UTC	Raw	no	Yes	Principal time system	Reference system for all time-related keywords. Always UTC.	no
TWIFLAT	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Name of twilight flat-field reference file		yes
VO_IDENT	PHDU	string(68)	%64s	none	""" """	Remed	yes	Yes	IVOA dataset identifier	See STR 2007-02	yes
WAT0_001	EHDU	string(68)	%68s	none	""" """	Raw	no	TBD	Polynomial coefficients for WCS description	Definition of a coordinate system (which includes distortion). Not required to be present. 	no
WAT1_nnn	EHDU	string(68)	%68s	none	""" """	Raw	no	TBD		"High-order distortion terms for Axis1 of WCS representation. Not required to be present. 5 keywords expected to represent all terms--i.e., nnn=001 to 005. "	no
WAT2_nnn	EHDU	string(68)	%68s	none	""" """	Raw	no	TBD		"High-order distortion terms for Axis2 of WCS representation. Not required to be present; 5 keywords expected to represent all terms--i.e., nnn=001 to 005. "	no
WCSAXES	EHDU	int	%1d	none	2	Raw		Yes	WCS dimensionality	Value is 2 for images	
ZD	PHDU	float	%7.4f	deg	Null	Raw		TBD	Zenith distance	Angle between zenith and target at observation start	
Keywords restricted to Mosaic Cameras											
CCDSUM	EHDU	string(8)	%6s	none	""" """	Raw	no	No	"Binning factor in image X, Y"	Valid only for data from CCD detectors	no
FRNGFIL	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Fringe template reference file	Valid only for some data from CCD detectors	yes
PUPLFIL	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Pupil ghost reference file	Used only for data from CCD cameras at present	yes
SKYMAG	EHDU	float	%5.2f	none	Null	Pipeline		No	Sky brightness in magnitudes		
Keywords restricted to NEWFIRM											
LINCOEF	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Linearity correction coefficients file	Used only for data from IR cameras at present	yes
LINCONST	PHDU	float	%12.4f	none	1.0	Pipeline		No	Default linearity coefficient	Used only for data from IR cameras at present. Not used if LINCOEF file is defined. 	
NOCDPAT	PHDU	string(8)	%10s	none	""" """	Raw	no	No	Dither sequence pattern designation		no
NOCMPAT	PHDU	string(8)	%10s	none	""" """	Raw	no	No	Map sequence pattern designation		no
SATCOEF	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Saturation definition file	Used only for data from IR cameras at present	yes
SATCONST	PHDU	float	%12.4f	adu	0.0	Pipeline		No	Default saturation value	Used only for data from IR cameras at present. Not used if SATCOEF file is defined. 	
Keywords Not Yet Implemented											
IMCMBnnn	PHDU	string(68)	%64s	none	"""N/A"""	Pipeline	yes	No	Name of constituent image in stack	"Valid only for data products where PROCTYPE=""Stacked"". The ""nnn"" is composed of 3 digits, and has values in the range 1 to 999. "	yes
