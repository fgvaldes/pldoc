- Section 3 on the important Workflow Subsystem Design is very poor.
  The structure of the exposition is not clear, it is a variable
  level, and in places attempts to describe things that the writer
  does not really understand.  In particular, any discussion about the
  processing aspects of the workflow and how it is structure need to
  be written by Frank or Rob.

- The writing of some sections has very poor grammar.  Tenses are
  constantly wrong and articles are missing.  A native english
  speaking reviewer reading this will form a poor opinion of the
  document.  Definitely not ready for review.

- Section 6 also suffers considerably from grammatical problems.

- The requirements sections should be appendices or in a different
  document.  It interferes with flow of the document and understanding
  the design and architecture.
