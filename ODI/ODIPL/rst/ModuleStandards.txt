=============================================================
IRAF Module Coding Standards and Conventions for ODI Pipeline
=============================================================

Introduction
------------

This document codifies the standard and conventions to be used when
writing pipeline modules which are IRAF scripts.  We are taking the
opportunity to codify this as much as possible before beginning on a
new pipeline application to ensure consistency and some desirable
features.  One of the desirable features is ease of running the
modules within multiple execution frameworks such as NHPPS and OGCE.

Style
-----

A standard coding style is desirable for multiple developers to easily
read and reuse code.  Since we currently expect no more than three
developers to contribute to ODI pipeline IRAF modules this is not a
high priority.  Some highly recommended style elements are:

* liberal use of indents as in most typical styles
* no top level indent
* indents of 4 characters
* any potential command-line arguments have variable declarations
  with prompt or comment
* primarily lower case variable names - mixed case such as myNewVar
  acceptable
* all caps for image header keywords
* whitespace
  - after commas
  - before or after parenthesis
  - around equal sign in assignments alone on a line
  - before {
* newline
  - after { and before final }

Optional recommended style elements are:

* a target of 80 characters per line before continuation; i.e. avoid
  automatic wrap arounds with windows of 80 characters
* use of whitespace may be reduced to keep to 80 characters for lines
  nearly that length

The basic order of the style:

* full comments at the top
* commandline variable declarations
* local scope variable declarations
* loading and defining packages and tasks
* parsing command line arguments
* last line is a logout

Comments and Documentation
--------------------------

Comments are, naturally, highly encouraged.

For documentary blocks, normally at the beginning of a module before
any script code, use the identifier '## ' ( double sharp and a space)
at the beginning of a line.  These lines will be extracted, stripped
of the identifier and interpreted as restructured text.

::

  ## =======
  ## OTAPROC
  ## =======
  ## ---------------------------------------
  ## Basic CCD calibrations for a single OTA
  ## ---------------------------------------
  ##
  ## Input Files
  ## -----------
  ##
  ## $ODI_OTA/input/<dataset>.ota
  ##     File with path uncalibrated OTA file (MEF) to be calibrated.
  ##
  ## Output Files
  ## ------------
  ##
  ## $ODI_OTA/data/<dataset>/<shortname>.fits
  ##     Calibrated OTA file (simple image).
  ##
  ## $ODI_OTA/data/<dataset>/<shortname>_bpm.pl
  ##     Bad pixel mask.  This includes the gaps.
  ##


Module Initialization
---------------------

All modules must be interpreted by the ``pipecl2`` command::

    #!/bin/env pipecl2

The developer must be familiar with what the initialization script
``login2.cl`` does.  These means things like what packages are automatically
loaded, what global and parameter set variables are defined, and what
directory the script is left in, and what common steps are performed.

The current functions of ``login2.cl`` includes:

* packages: images, proto, utilities, lists, mario
* variables: application configuration parameters
* pset variables: nhpps, plmodule, names, <pipe>names (if available)
* restart steps: delete and restore files recorded previously
* other steps: standard log output to ``stdout`` and dataset log file

Exit Status
-----------

There are a few basic exit status values::

     0 = Unspecific error
     1 = Unspecific success
    99 = Debugging stop

All other status values are currently not mandated.

Restartability
--------------

All modules must be restartable if they crash or exit with an error.
This is possible provided the module keeps track of temporary files
created and makes backups of files that are changed or deleted.  It is
also necessary that no subsequent module changes or deletes the input
files.  This is faciliated by making use of two initialization
features of the standard module startup which delete and restore files
from a previous execution as recorded in standard files.

The standard files are defined in the pset variables ``plmodule.fdelete``
and ``plmodule.frestore`` which are in specified relative to the dataset
data directory.  When a module starts these files are checked and the files
are deleted or restored.  If the files do not exist that is fine and is the
case when a module is run for the first time.

Then as the module runs it must be sure to:

* record any temporary files in ``plmodule.fdelete`` immediately before or
  after creation
* back up and record the path in ``plmodule.frestore`` any files which are
  modified or deleted such that a restart will behave incorrectly


File Naming Conventions
-----------------------

The same file naming conventions as used in the MOSAIC and NEWFIRM
pipeline applications will be used.  This is facilitated by using the
standard ``names`` parameter set and any pipeline specific names
scripts.  Some characteristics of this naming convention are:

* daughter datasets begin with the parent dataset name
* long names include the pipeline hierarchy with ``-XXX`` where
  XXX is the pipeline abbreviation
* shorter names eliminate the pipeline hierarchy information
* some standard extensions are ::
    fits, log, list, txt, cat, tmp, XXX (the pipeline abbreviation)
* when in dataset working directories working names make use of the module
  name and the ``tmp`` extension: e.g. foostage1.tmp

Any tmp files are not propagated and are cleaned up in *done* or
*cleanup* stages.
