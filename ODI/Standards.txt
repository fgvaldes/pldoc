These are just my notes and may be cryptic.  Frank


Areas to consider:

    ENVIRONMENT
    MODULE FILE LIST
    MODULE ARGUMENTS
    MODULE EXIT STATUS
    IRAF STANDARDS

Some functional requirements:

- Modules must be re-runable
	- Do not modify input list
	- Look for and delete any files left from previous execution;
	  most notably temporary files left after a crash


** This seems like the most likely thing to easily incorporate.
** As I said in our phone meeting my thought is that any environment
** variables required by the module (other than those standard ones,
** such as for running IRAF, can be specified on the command line.

- Modules support standard 'keyword=value' arguments which take
  priority over environment variables.
  	dataset
	pipeline
	stagename
	success
	error
	nodata

# This just codifies our existing convention.
Dataset name convention:
	queue_name_procid[-pipeline[name]]...

- All modules except the dataset creation stage use the dataset data
  directory as the working directory.


MODULE FILE LIST

# This just codifies our existing convention.
To allow multiple module languages (MML) filenames and file lists may
be language specific but within a language standard apply.

IRAF -  [node!][logicaldir$]



Standard file extensions:

tmp - are never propagated as a data product


IRAF Standards

ENVIRONMENT VARIABLES CONVERTED TO CL VARIABLES:

OSF_DATASET
OSF_FLAG
NHPPS_SUBPIPE_NAME
NHPPS_MODULE_NAME

NHPPS_SYS_NAME
*_DM_NODE
*_DM_PORT
*_CM_NODE
*_CM_PORT
NHPPS_PIPEAPPSRC

[Let's add a way to set these outside of uparm]
clobber=yes
imclobber=yes
verify=no
verbose=no
show=no

A trailing slash in directories specified by environment variables is
optional; i.e. any references in modules should use "VARNAME$/".
