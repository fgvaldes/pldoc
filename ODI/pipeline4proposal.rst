Tier 1: Transforming Raw Data to Standard Data Products
-----------------------------------------------

This section gives a high-level, astronomer-oriented description of what the
fully automated Tier 1 pipeline does to transform raw data to calibrated
data products. These standard data products are ready for science analysis,
possibly with user driven Tier 2 operations and Tier 3 workflows (see below
for a brief description of these higher level steps).  A much more detailed
description of the Tier 1 pipeline, which descends into the highest level of
the pipeline workflow structure, is provided by Valdes [REF].

The ODI pipeline operates on data sets consisting of all raw exposures
(except for test and focus exposures) from a block of nights.  A block of
nights is used rather than a single night to allow for instrumental and
weather problems and different observing strategies.  The component which
defines the data sets is called the *Pipeline Scheduling Agent (PSA)*.  Given
information about the current time, telescope schedule, proposals, and
priorities it identifies and schedules data sets to submit to the pipeline
executor.

The pipeline first separates exposures into the various types of dome
calibrations (e.g. bias, dark, flat field) and on-sky exposures.  The
orchestration logic then processes all exposures of each type in the usual
CCD calibration dependency order.  For example, a sky exposure depends on
dome flat field calibrations which, in turn, depends on bias calibrations,
so all the bias exposures are processed first.  More precisely, the
dependency order is:

    1. bias
    2. dark
    3. dome flat fields
    4. twilight flat fields
    5. non-OTA guided exposures
    6. OTA guided exposures

In each group all exposures in the data set are processed before going on to
the next group.  This means that calibrations from a later night (in the
data set block) are available to the next group.  Each group is further
subdivided into calibration sequences, filters, and nights.

An important component of the system is the *Calibration Library (CalLib)*.
Calibrations are entered into the library with various indexing parameters
as they are produced by the pipeline or made available by the pipeline
scientist.  Note that not all calibrations are images and some calibrations
are derived on a slower semester or yearly schedule.  Subsequently the
pipeline requests a calibration by type, date of exposure, filter, etc.  The
CalLib includes some rule-based intelligence in selecting the best
calibration based on the various indexing parameters such as important data
quality parameters.

Getting calibrations from the CalLib is broken up into two steps.
Responding to a request with the appropriate metadata and, for file-based
calibrations, providing the URI for the pipeline to access the file.

An important aspect of the calibration data flow is that as the calibration
sequences are transformed from raw exposures to master calibrations they are
entered into the calibration library.  At the next level requests for a
master calibration will typically return a calibration just produced from
the same night.  However, if the calibration is missing or of poor quality,
another calibration from the library will be used.  Since the PSA will
generally operate chronologically this means a calibration from an earlier
observing block.  But if a reprocessing is done this may also be from a
later block.

The first science exposures to be processed are the non-OTA guided
exposures.  This is because they may be used to derive dark sky calibrations
such as fringe templates and flat fields.  They are then available to be
applied to both the non-OTA guided exposures and to the guided exposures as
convolved by the guiding shift history.

OTA-guided exposures will be novel to many observers.  ODI provides
several tip-tilt guiding modes performed by shifting the charge on
the detectors based on guide stars rapidly read out of parts of
the detector.  They primarily differ by how widely the shifts are
applied across the full detector.  Because of the charge shifting
the static calibrations must be shifted in software, namely convolved,
by the shift history associated in the raw data with such exposures.
This is a algorithmically not very complex and affects the standard
calibrations simply in preparing the master calibrations prior to
apply the standard operations of subtraction and dividing.

The other area the guiding affects in standard processing is the
astrometric calibration.  Essentially a solution for the world
coordinate system (WCS) function must support a degree of "rubber
sheet" distortions when independent guiding is performed on different
regions of the field of view.

As note above, guided exposures are not used for dark sky calibrations
because the on-detector shifts complicate the extraction of individual pixel
responses.  More sophisticate used of guided exposure, such as for
illlumination corrections at scales much larger than the shifts, may be
possible but are beyond the scope of the PASRD requirements.  However, those
derived from unguided exposures can then be convolved and applied to guided
exposures in the same way a the static dome calibrations.

The calibrations applied by the pipeline are those required by the
PASRD and possibly others identified as needed. A list of these
are:

    - crosstalk
    - overscan
    - bias
    - dark current
    - flat fielding
    - pupil pattern removal
    - fringe pattern removal
    - bad pixel replacement (detector, saturation, cosmic rays, etc)
    - astrometric solution
    - photometric characterization

In addition to operations affecting the science images the pipeline
generates data qualilty information in the form of metadata and
associated maps.

After individual exposures are calibrated the pipeline combines exposures
which spatially overlap; e.g.  dither sequences, cosmic ray splits, multiple
visits.  In addition to identifying the overlapping exposures the pipeline
evaluates the image quality of each exposure to avoid including poor
exposures.  The selection uses a rule-based algorithm which produces good
quality stacks.  Depending on the variablity of the atmosphere this will
statisfy all observers or those without the need to optimize the image
quality at the expense of depth.  The combining uses a two pass sequence of
steps to first detect transient sources before making a final "stack" with
those sources removed.

The pipeline may also evaluate the transient sources identified during
stacking and extract the scientifically interesting ones from the more
common cosmic rays using algorithms developed by NOAO and LSST.

At the end of the Tier 1 processing a number of data products are produced.
These are:

    - master calibrations
    - instrumentally calibrated but not geometrically rectified exposures
    - geometrically rectified exposures on a common sky grid
    - stack exposures based on the rectified exposures
    - source catalogs (as used for astrometic and photometric calibration)
    - transient catalogs
    - associated data quality, weight, and exposure maps


Tier 2: Customizing the Standard Data Products
----------------------------------------------

The Tier 1 pipeline is oriented to performing well-understood
calibrations and producing standard data products.  Tier 2 processing
is about customizing and extending the standard data products for specific
science goals.  Note that this is not analysis which is the area of Tier 3
processing.  Because Tier 2 processing is usually about applying
operations over a whole dataset this will generally require high
performance support by providing a selection of capabilities that are
found to be desired by a subset of the user community.  These
capabilities are extracted from use case surveys.  These are
somewhat sketchy but this proposal identifies a path of providing
such capabilities from the user's work area and utilizing high
performance infrastructure.

The main types of Tier 2 capabilities that are relatively clear are:

    - optimizing stacks for high resolution
    - filtering imaging data to enhance features of interest
    - model subtraction
    - calibrations that are hard to automate; i.e. standard star
      calibrations
    - extracting common science data; i.e. generating catalogs

What follows is a bit of elaboration on these areas.

Optimizing stacks is an extension of the standard stacking produced by
the Tier 1 pipeline.  That pipeline does eliminate bad exposures
relative to some mean quality by removing exposures with particularly
poor seeing or transparency.  However, some observers will wish to be
more stringent and also combine data across bigger time intervals than
provided during standard processing.  A capability to select exposures
to stack, with more control over stacking parameters, using standard
data products is provided in a form that is suitable for high
performance over large data sets.

Filtering is used to remove some features and enhance others.  It also
attempts to regularize things like the PSF.  This can be accomplished
with somewhat generic image processing tools.  As one example,
"unsharp masking" can be used to remove large, low-frequency features
such as extended galaxies.

Model subtraction is somewhat like filtering.  This includes PSF and
galaxy modeling and subtraction.

Precise photometric calibration often requires user interaction.  The
Tier 1 pipeline will provide some level of calibration but it may not
use all the data the observer collected or have support for the less
widely used filters.

Generating catalogs with astrometric and photometric information is a
common operation.  Tier 1 provides some simple source catalogs but
these are not photometrically accurate for typical science.  Tier 2
can provide access to common cataloging (e.g. SExtractor) and
photometric functions (e.g. DAOPhot).


Tier 3: Doing Custom Science
----------------------------

This proposal does not cover application of arbitrary user algorithms
to be applied to Tier 1 and Tier 2 data products.  Tier 3 basically
means individual program algorithms applied to ODI data.  The users,
PIs or archive researchers, may download some or all of the data to
their computing environments to do custom processing.  The main
support for this we propose is a flexible "cut-out" capability to
allow extracting parts of large datasets for download rather than the
whole dataset.

In some cases researchers may be able to work with ODI personnel to convert
an algorithm to a Tier 2 capability or even contribute to the Tier 1
processing.  Since this is outside of the scope of this proposal we do not
say more or promise support for this type of processing.

