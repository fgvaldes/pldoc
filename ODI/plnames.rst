======================================
ODI PPA Data Product Naming Convention
======================================
--------------------------------
Valdes & Swaters, April 17, 2012
--------------------------------

Introduction
============

This is a first proposal for the way ODI PPA data products will be
named when provided to the data engine through the final output
data products manifest.  While the downstream software may chose to
impose its own naming convention, this proposal is recommended.  It
has the virtues of uniqueness across processing runs and ties to the
original exposures and the PSA dataset definitions.

As a caveat, I expect this proposal is subject to modification as
needs dictate.  It is based on experience with what has worked with
the NOAO Mosaic pipeline but considerably shortened.

The format is shown below and each component is then described.
Examples are shown.

::

     AAAAAAAA_BBBBBBB-C..._C[C]-DDDDDDDDDDDDDD-E..._FG.HHH

AAAAAAAA
========

This is the PSA dataset identifier.  A PSA dataset can be any
combination of nights.  In principle the identifier can be any string.
However, in NOAO pipeline usage this is the calendar date,
as YYYYMMDD, of the first night in the set of nights making up the
dataset.  

    Example:
        20120701

BBBBBBB
=======

This is the PSA processing identifier.  This changes everytime the
PSA submits a dataset.   This allows processing of the the same
dataset, or subset of the dataset, to produce unique data products.
The convention at NOAO, which we would also use for PPA, is the Unix
timestamp (seconds from 1970) converted to hex.

    Example:
        81a976d

C..._C[C]
=========

This is the subset identifier which depends on whether it is
calibration or science, the filter, and night subgrouping.  Night
subgrouping depends on the distributions of science exposures in a
filter over the set of nights being processed.  Often all exposures
are grouped as a single filter subset.

The first part is the filter ID.  This is some reasonable and commonly
used abbreviation of the FILTER string; e.g.

::

    U, B, V, R, I, u, g, r, z, ha.

Note that for zero and dark exposures the filter is not significant
though a the filter in the light path will still be indicated.

The second part is the type of exposures and, if needed, an index to
a subgrouping.  The subgrouping types are:

    Z:
        zero
    D:
        dark count
    F:
        dome flat
    T:
        twilight flat field
    I:
        fringe (interference) template
    S:
        dark sky illumination flat
    O:
        object data with static guiding
    G:
        object data with common or local OT guiding

Following this is an optional subgrouping.  This will mostly be used
to break large groups of science exposures in the same filter across
the block of nights into smaller groups.  It could also be used to
separate short exposures.


DDDDDDDDDDDDDDD
===============

This is an exposure identifier.  This is the DATE-OBS rounded to the
nearest second.   Note that this is also the same as the base part of the
raw ODI exposure name without the first character observation type
indicator.  For data products which are formed by
combining exposures -- such as dome calibration sequences, dither
sequences, or pointing stacks -- the convention is to use the exposure
identifier of the first exposures taken.

    Example:
        20120701T034512, 20120631T234515

E...
====

When the data product is composed of several parts -- such as OTA files
or tiles of a big stack -- this is the part identifier.  The most
common example is the data products for the instrumentally calibrated
exposures will retain the structure of one file per OTA (even though
the cell extensions will have been merged into a single image).

    Examples:
        00, 77

F
=

The science data product identifier.  Since there are typically several
associated science data products for a particular type of calibrated science
data product this identifies the type.  For the NOAO Mosaic these are
zero to several characters.  These can be similar to Mosaic or we can
use one character identifiers.

    o:
        science exposure not remapped
    r:
        science exposure remapped (not a standard data product)
    d:
        dither stack
    s:
        sky pointing stack
    c:
        calibration sequence stack
    i:
        individual calibration (not a standard data product)

G
=

Each science data product typically has associated data.  This
identifies the type.

    i:
        science image
    c:
        catalog
    d:
        data quality map
    w:
        weight map
    e:
        exposure (coverage) map

extn
====

The extension defines the usual file types.

    fits:
        FITS file (image or binary table)
    fits.fz:
        compressed FITS file
    png:
        PNG
    jpg:
        JPG
    txt:
        ASCII text

Examples
=========

::

    20120701_81a976d-V_Z-20120631T234512-34_ci.fits
    20120701_81a976d-V_Z-20120631T234512-34_cw.fits
    20120701_81a976d-V_D-20120631T235013-34_ci.fits
    20120701_81a976d-V_D-20120631T235013-34_cw.fits
    20120701_81a976d-V_F-20120631T235532-34_ci.fits
    20120701_81a976d-V_F-20120631T235532-34_cw.fits
    20120701_81a976d-U_T-20120631T235812-34_ci.fits
    20120701_81a976d-U_T-20120631T235812-34_cw.fits
    20120701_81a976d-V_O-20120701T001843-34_oi.fits
    20120701_81a976d-V_O-20120701T001843-34_oi.jpg
    20120701_81a976d-V_O-20120701T001843-34_od.fits
    20120701_81a976d-V_O-20120701T001843-34_ow.fits
    20120701_81a976d-V_O-20120701T001843-34_oc.fits
    20120701_81a976d-V_S-20120701T001843-34_ci.fits
    20120701_81a976d-V_S-20120701T001843-34_cd.fits
    20120701_81a976d-V_S-20120701T001843-34_cw.fits
    20120701_81a976d-V_G-20120701T001843_di.fits
    20120701_81a976d-V_G-20120701T001843_di.jpg
    20120701_81a976d-V_G-20120701T001843_dd.fits
    20120701_81a976d-V_G-20120701T001843_dw.fits
    20120701_81a976d-V_G-20120701T001843_de.fits
    20120701_81a976d-V_O-20120701T001843_si.fits
    20120701_81a976d-V_O-20120701T001843_sc.fits
    20120701_81a976d-V_O-20120701T001843_si.jpg
    20120701_81a976d-V_O-20120701T001843_sd.fits
    20120701_81a976d-V_O-20120701T001843_sw.fits
    20120701_81a976d-V_O-20120701T001843_se.fits
