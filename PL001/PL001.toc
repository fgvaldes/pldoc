\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Pipeline Applications}{3}
\contentsline {section}{\numberline {3}NHPPS Core System}{4}
\contentsline {subsection}{\numberline {3.1}Messaging}{5}
\contentsline {subsection}{\numberline {3.2}Directory Server}{5}
\contentsline {subsubsection}{\numberline {3.2.1}Externally Callable Methods}{6}
\contentsline {subsection}{\numberline {3.3}Node Manager}{6}
\contentsline {subsubsection}{\numberline {3.3.1}Externally Callable Methods}{6}
\contentsline {subsection}{\numberline {3.4}Pipeline Manager}{7}
\contentsline {subsubsection}{\numberline {3.4.1}Module Manager}{7}
\contentsline {subsubsection}{\numberline {3.4.2}Scheduling}{9}
\contentsline {subsubsection}{\numberline {3.4.3}Blackboards}{9}
\contentsline {subsection}{\numberline {3.5}Pipeline Selection Utility}{10}
\contentsline {subsection}{\numberline {3.6}NHPPS host commands}{10}
\contentsline {subsection}{\numberline {3.7}Pipeline Description Language}{11}
\contentsline {subsubsection}{\numberline {3.7.1}Settings}{11}
\contentsline {subsubsection}{\numberline {3.7.2}Actions}{12}
\contentsline {subsubsection}{\numberline {3.7.3}Events}{12}
\contentsline {subsubsection}{\numberline {3.7.4}Application Level}{13}
\contentsline {subsubsection}{\numberline {3.7.5}Pipeline Level}{13}
\contentsline {subsubsection}{\numberline {3.7.6}Module Level}{13}
\contentsline {subsection}{\numberline {3.8}Performance}{13}
\contentsline {section}{\numberline {4}NHPPS Optional Components}{13}
\contentsline {subsection}{\numberline {4.1}Data Manager}{15}
\contentsline {subsubsection}{\numberline {4.1.1}Pipeline Metadata Archive System}{15}
\contentsline {subsubsection}{\numberline {4.1.2}Calibration Library}{15}
\contentsline {subsubsection}{\numberline {4.1.3}Externally Callable Methods}{16}
\contentsline {subsection}{\numberline {4.2}Pipeline Monitor}{17}
\contentsline {subsection}{\numberline {4.3}Message Monitor}{18}
\contentsline {subsection}{\numberline {4.4}Switchboard Server}{18}
\contentsline {section}{\numberline {5}Implementation}{18}
