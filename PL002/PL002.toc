\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}The Element Hierarchy}{2}
\contentsline {section}{\numberline {3}System PDL File}{2}
\contentsline {section}{\numberline {4}Pipeline PDL Files}{2}
\contentsline {section}{\numberline {5}Macros}{2}
\contentsline {section}{\numberline {6}Variable Substitution}{3}
\contentsline {section}{\numberline {7}Exit Codes}{3}
\contentsline {section}{\numberline {8}Templates}{4}
\contentsline {section}{References}{4}
\contentsline {section}{Appendix A: PDL Technical Specification}{5}
\contentsline {subsection}{PDL Elements}{5}
\contentsline {subsection}{PDL Attributes}{9}
\contentsline {section}{Appendix B: Example}{13}
