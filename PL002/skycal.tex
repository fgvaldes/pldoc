\section{Dark Sky Self-Calibrations\label{skycal}}

Dark sky self-calibraion refers to a technique that uses a set of
disregistered observations to eliminate astronomical sources and produce an
approximation of a blank sky exposure.  Under the assumption that a blank
sky exposure should be uniform, any structure is due to instrumental
artifacts  which must be removed either as extraneous light or as
non-uniform response.  The calibrations derived from this are generally
better than dome flat fields because the color (the spectrum across the
filter band pass) and illumination of the dome flat field is a poorer match
to the science observations than blank sky.

Because of astronomical sources in any given exposure only a subset of the
pixels are exposed to blank sky.  The principle behind dark sky
self-calibration is that in the ensemble of exposures there are some
exposures that sample blank sky for every pixel.  When this is not the case
for the most pixels the data cannot be self-calibrated and either a
calibration from another time (such as from an earlier program) must be used
or the data is calibrated using only dome calibrations.

A consideration in determining the calibrations is whether they degrade the
data because of noise in the calibration is worse than the instrumental
pattern being removed or because unresolved sources and large-scale extended
light introduce artifacts.  Ideally every pixel will have a large number of
clean samples of blank sky.  But when this is not the case, there is an
intermediate level of calibration where large scale instrumental patterns
may still be removed even if individual pixel responses may not.  Therefore,
another aspect of the dark sky self-calibration is deciding whether a full
pixel response, a large scale pattern, or no calibration is best.

Observers who self-calibrate their data in this fashion examine the
individual exposures and apply their judgement in selecting a set of
exposures for the calibration.  The challenge for the Mosiac pipeline is to
heuristically determine the set of exposures to use or, conversely, not use
and which level of calibration is supported by the data.  This is difficult
because the criteria are subjective and observers apply complex trade offs
and the ability to recognize the content of the exposures.  This is made
more difficult because, as noted in the introduction, the observational
programs and protocols are unconstrained so that some sets of data are not
suited to this type of self-calibration.

Nevertheless we have attempted to develop a set of criteria and algorithms
to automatically determine whether self-calibration may be performed, at
what scale, and with what set of exposures.  This section describes these
methods.

There are currently three types of dark sky self-calibrations performed by
the Mosaic pipeline.  These are pupil ghost removal, fringe subtraction, and
flat fielding.  The first two are structures which must be subtracted
and the last is a response behavior.

\subsection{The Approach}

The dark sky calibration portion of the pipeline begins with a dataset
consisting of science exposures from one or more consecutive nights taken with
the same filter.  The exposures are winnowed through a series of tests.  At
the end of these tests a set of exposures for creating blank sky images is
identified.

If there are insufficient exposures then self-calibration is not done and,
instead, a library calibration is used.  When no suitable library
calibration is available the exposures are left calibrated only by dome
calibrations.

When there are enough exposures the exposures are scaled and combined
using rejection methods based on statistics and source segmentation
masks to eliminate data which does not sample blank sky.  The
instrumental structure in the blank sky exposure is used to calibrate
each exposure.

For the response calibration, the dark sky flat field, the signal-to-noise
of the calibration is evaluated and when it is poor a filter is applied to
suppress the pixel noise while retaining the large scale illumination
response calibration.

\subsection{General Considerations in the Dark Sky Self-Calibrations}

This section discuss various general considerations in the dark sky
calibration method and algorithmic and implemenation choices that apply to
all the instrumental effects.

The problem of calibrating exposures which are affected by both additive
(pupil ghost and fringe patterns) and multiplicative (pixel responses and
illumination patterns) is difficult and not strictly decomposable.  What is
done is to use the dark sky calibration process iteratively.  This means
forming a dark sky exposure, using it to extract the strongest instrumental
feature to be removed, calibrating all of the exposures, and then repeating
the steps for the next calibration.  For the Mosaic camera the calibrations
are done in the following order.  For those datasets affected by a pupil
ghost (Mayall telescope exposures in certain filters) the pupil pattern is
determined and subtracted from the exposures.  For those datasets affected
by fringing (red filters from both telescopes) the fringe pattern is
extracted and subtracted from the exposures.  Finally, all datasets create a
dark sky flat field which is used to calibrate the pixel responses (if
appropriate) and large scale illumination patterns across the field.

In the process of evaluating each exposure there are quantities that are
measured independently, and in parallel, for each CCD of the mosaic; for
example the maximum number of pixels segmented as part of a single object.
One might consider accepting and rejecting a different set of exposures for
each CCD.  However it is possible for subtle continuity effects to occur
between CCDs when the calibrations derived for each CCD are based on
different exposures.  Continuing with the large single object example, when
a large source such as a galaxy or very bright star falls in one or two CCDs
while the rest are relatively sparse and might be good candidates for dark
sky self-calibration.  The Mosaic pipeline currently takes the conservative
approach of rejecting an entire exposure if only a subset of CCDs fails to
meet some of the criteria described below.  The criteria reported in the
diagnostic then are the maximum or minimum over the set of CCDs.

Making a dark sky image consists of scaling each contributing exposure using
the sky mean, computed earlier, and averaging the pixels with rejection of
non-sky pixels.  There are two aspects to the rejection.  The first is use
of the object segmenation masks for each exposure.  The second is
statistical rejection from the ensemble of pixel values.  The rejection
algorithm is a parameter with the default being a percentile clipping
method.  In brief, this method determines a measure of the distribution
width from the pixels below the mean or median and clips pixels above the
mean or median which significantly exceed this width.  The implementation
used for this operation is IRAF's {\tt imcombine}.  This is a complex
application with many options and features.  For more information consult
the documentation for this program.

As will be described later each of the calibration patterns are
extracated from the dark sky image.  These patterns are weak and so
they are sigma clipped to identify outlier pixels.  The outlier pixels
are replaced the minimum or maximum value of the remaining pixels.
This is done to avoid having extreme pixels affect the application of
the calibration to the data.  In particular, the pupil ghost and
fringe pattern removal must scale the weak pattern to each exposure
and the scaling calculation can be biases by a few extreme values in the
pattern.

Each of the calibrations have a common pattern in their use of the
calibration library.  One feature of the pipeline is a configuration
parameter that allows the generation of calibrations even if the analysis of
the exposures determines that the dataset is inadequate for creating dark
sky self-calibrations.  These are generated from all the data [ARE ANY NOT
USED?] but a quality parameter flags this as an "evaluation" calibration.
These appear in the final review documentation and data products but are not
stored in the calibration library nor applied to the data.

Another common pattern is that if a self-calibration cannot be derived
from the dataset a calibration from the calibration library is used if
available.

In principle, a different set of images may suitable for each calibration --
pupil ghost, fringe, and flat field -- and the pipeline is designed to allow
this.  However, currently the set of exposures for all dark sky calibration
are the same.

Before describing the criteria a note about the mechanics.  Ultimately
whether a particular image is used for a particular type of calibration
depends on image header keywords.  There are two levels of specification;
one that cannot be overridden ("Y!" or "N!") and one that can ("Y" or "N").
The former is intended for observers, operators, upstream software to
prejudge the data.  However, the usual case is that various distributed
pipeline stages can veto the image.

\subsection{Identifying Exposures for Dark Sky Calibrations}

This section describes a variety of requirements and heuristics that lead to
a subset of exposures from a dataset for creating a blank sky exposure.  As
noted previous, the approach is mostly one of winnowing exposures at each
step in a series of steps.  The steps are presented in the order in which
the are evaluated and, unless otherwise indicated, excluded exposures are
not considered in subsequent steps.

{\bf External acceptance}: For special cases, particularly for
reprocessing datasets for privileged investigators, the
acceptance flag can be set to "Y" or "N" in the input data externally or
in a special processing stage at the beginning of the pipeline.  Those
set to yes will be used in all subsequent test and not rejected by
those tests while those set to no will never be used.

{\bf Dome calibrations}: If the basic dome calibrations -- overscan,
bias, dark, and dome flat -- fail for some reason the image is marked
for exclusion.  Currently the only data quality criteria of this type
are a bad amplifier as identified by too many zero ADU overscan values
and saturation as identified by the fraction of pixels above a
saturation threshold.

{\bf Exposure time}: Short exposures provide poor measures of blank
sky because of low counts.  A rule, which could be indexed by filter
but is not currently, is applied to the exposure time.  The current
rule is a simple threshold of 120 seconds that is independent of any
other factor such as filter.

Note that an exposure criteria is also implicit in the initial
definition of a dataset.  The pipeline divides short and long
exposures into different datasets with the long exposures being
processed first.  The current division uses the same threshold
exposure time of 120 seconds though the this can be easily changed.
The consequence of using the same time is that all exposures in the
long exposure dataset are accepted and all exposures in the short
exposure dataset are rejected when creating dark sky calibrations.
There for only long exposure datasets can produce calibrations.  This
is why the short exposures datasets are processed by the pipeline only
after the long exposures so that any dark sky calibration exposures
added to the calibration library for that night are available to be
applied to the short exposures.

{\bf Fraction of sky pixels}: Exposures with a high source density
have fewer pixels observing blank sky.  As part of the image
segmentation step the fraction of pixels not associated with sources
is determined.  As mentioned earlier, this is computed independently for
each CCD.  The current rule is that the fraction of sky pixels must be
greater than 85\% in all CCDs.

{\bf Maximum object area}: In addition to eliminating a large number
pixels from sky, exposures with large sources are poor
candidates for sky calibrations because they have large regions of low
surface brightness wings that typically show through when making the
master blank sky image.  The current rule is that exposures with any single
segmented source having more than 500,000 pixels in any CCD are
excluded.  Note that large sources often span more than one CCD.  The
Mosaic pipeline does not currently identify this situation so if
the parts of a single source spanning multiple CCDs do not have more than
500,000 pixels in any CCD then the exposure will not be exclude based
on this criterion.

{\bf Sky region}: Certain regions of the sky have little if any blank
sky.  These are regions of nebulosity or very large galaxies.  The
Mosaic pipeline has the capability of consulting a database of
positions on the sky and rejecting exposures that overlap these
positions.  There is currently no such database in the calibration
library.

{\bf Magnitude zeropoint discrepancy}: For filters that have an expected
magnitude zeropoint for photometric conditions stored in the calibration
library, the difference in magnitude zeropoint for the exposure relative to
this photometric value provides an indication of clouds.  The current
rule is no brighter than 1 magnitude from the photometric value.

{\bf Positive mean sky rate}:  Exposures whose mean sky rate is less
than 0.01 electrons/second are excluded.  Partly this is because some
sky is needed to make blank sky calibrations but this is required
for scaling exposures by the reciprical of this rate.

{\bf Empirical magnitude zeropoint discrepancy}:  The previous check of the
magnitude zeropoint was against a calibration reference value which may only
be available for certain filters.  There is also an empirical test for
exposures with anomalously low zeropoints within a dataset.  This consists
of iteratively computing the mean zeropoint and standard deviation and
excluding values more than twice the standard deviation.  When the mean
converges, meaning that no more values are reject, then exposures with
zeropoints more than one magnitude brighter than the mean are excluded.

{\bf Sky brightness criteria}: A set of exposures may include a
minority inadvertantly taken through significant clouds or too far into
twilight.  These are identified and excluded by sigma clipping on
the mean sky rates as measured over all CCDs.  The sigma clipping
is performed as with the magnitude zeropoint using an interative
two standard deviation clipping to determine the typical sky rate.
Exposures are then excluded which are more than three standard
deviations from the mean.

{\bf Low resolution sky structure}: Low resolution sky maps are produced by
segmentation of the images.  Note that this is done after dome calibrations
but before the dark sky calibrations stages.  The sky maps for the CCDs are
tiled together to make a single small image of the field and normalized to
unit mean sky rate.  The sky maps are sensitive to large, low surface
brightness light from large sources or clusters of bright source.  In this
context large sources also includes very bright stars.

Sky maps, and therefore the exposures, are used to exclude those with
significantly different structure from the average.  The average sky map is
formed by fitting and subtracting a planar gradient across the whole field
from each map and then averaging the results.  This average is subtraated
from each sky map and the root mean square (RMS) over all map pixels is
computed.  An iterative rejection of large RMS values is performed on
the list of RMS values using a two standard deviation threshold above
the mean RMS.  The exposures corresponding to the sky maps whose RMS
value are rejected are then excluded from consideration.

{bf Rejection of low resolution, non-flat field, structures}:  A new average
of the surviving, normalized and gradient subtracated, sky maps is formed.
This is treated as a low resolution dark sky flat field which is divided
into each of the sky maps.  This flat fielding is done because some of the
structure in the sky maps is the flat field and pupil pattern features we
are trying to address by dark sky calibration.  One feature that is easy to
see in some data is the pupil ghost pattern which has not yet been removed.
Though this feature must be subtracted for a correct scientific calibration
in this step of identifying suitable images for a dark sky calibration it is
removed by flat fielding.

What is left in the sky maps after this flat fielding are structures
caused by undetected faint light from large sources in the exposures.  While
many exposures with large galaxies or extremely bright stars were
removed earlier using the full images, there are some situations that
are not caught.  These include sources spanning more than one CCD or
groups of moderately large sources, especially bright stars.

To identify the exposures with this large scale faint light, the ACE source
detector is run on the sky maps with parameters appropriate for the small
size and low resolution of the maps.  A fairly large threshold (7
sigma) with no convolution is used to find only significant
structures.  Those maps detected source are rejected.  However,
if fewer than four exposure survive then the result of this rejection
step are ignored.

{\bf Overlap statistics}: Once a tentative set of exposures is
identified, the number of exposures that sample blank sky at each
pixel is determined using the object segmentation masks.  A cumulative
histogram is created of the number of pixels with fewer than one sky sample,
fewer than two sky samples, and so forth up to the number of
exposures.  This cumulative histogram is expressed in terms of the
percentage of exposures and the percentage of the total number of
pixels.  Figure~\ref{fig:overlaphist} shows a histogram for the case
of eight exposures along with the acceptance threshold.  To be
accepted the points have to all lie below the curve as in this
example.

\begin{figure}
\begin{center}
\includegraphics[width=.8\columnwidth]{Figs/overlaphist.eps}
\end{center}
\caption{
Example overlap histogram for eight Mosaic exposures.  The solid line is
from the sky pixels identified in the exposures and the dotted line is the
upper limit for acceptance of the exposures for a dark sky stack.  Each bin
represents the percentage of the total number of pixels, where 100\% is
$8192^2 = 67108856$ pixels, which have fewer than X percent of exposures
sampling sky, where X is ordinate at the right of the bin.  In this example
41\% of the pixels have fewer than 100\% of sky pixels which means
59\% of the pixels have no sources in any exposure.
}
\end{figure}

{\bf Minimum number for deep sky stack}: The last criteria is a rule based
on the number of surviving exposures.  There may be different rules for each
class of dark sky calibration and the rules may depend on the filter.
Currently there is a single, simple rule that there be a minimum of five (5)
exposures.  If there are any externally selected exposures then this rule is
not applied.

\subsection{Pupil Ghost and Fringe Pattern Subtraction\label{pgr}}

In this section we describe the removal of pupil ghost and fringe
patterns from the science exposures.  These are combined in one
section because they require similar methods.  The basic idea is that
a pattern template is extracted from the dark sky image and the
template is scaled in amplitude to each exposure and subtracted.
The challenges are extracting the templates from the background and
scaling these faint templates to the science exposures in the presence
of astronomical sources and background.

The Mosaic cameras have optical surfaces which can reflect a small fraction
of the light such that a large out-of-focus ghost pattern, called the pupil
ghost pattern, reaches the detector.  The amplitude of this ghost depends on
the optics path and coatings of the camera and telescope and on the field
illumination.  The first dependency results in the pattern only being
significant at Kitt Peak, only in some filters, and only affecting
some CCDs.

As with many high performance, "thinned", astronomical CCDs, the Mosaic
camera detectors exhibit fringing patterns at some wavelengths.  This
pattern is also filter dependent and occurs in data from both cameras
and all CCDs.

A commonality of these effects is that they are detector and filter
dependent.  Whether these calibrations are required for a particular dataset
is defined by a calibration parameter in the calibration library.  If the
calibration is not required the pipeline simply skips the operations.

When the calibration is required, the pipeline determines whether a suitable
dark sky self-calibration image can be created from the dataset as described
previously.  If it can then the pattern template extraction steps described
below are performed.  But if a self-calibration is not possible the pattern
removal may still be accomplished using templates previously stored in the
calibration library from other datasets.  The nearest in time with the
highest quality is retrieved and the template scaling and subtraction
methods described later in this section are carried out.

Extraction of a pattern template from a dark sky image consists of
specifying the location of the pattern, removal of the background, and
minimizing the effects of any residual, non-pattern features which may
remain.  The location of the pattern is provided by a mask.  This is
only necessary for the pupil ghost pattern which occurs near the
center of the mosaic and affects only four of the CCDs.

\begin{figure}[h]
\begin{center} 
\includegraphics[width=.45\columnwidth]{Figs/raw.eps}
\hfil
\includegraphics[width=.45\columnwidth]{Figs/cal.eps}
\end{center}
\caption{ The two panels are negative contrast displays from the same
central 4K x4K region (covering 4 or the 8 CCDs) of a KPNO Mosaic z'-band
exposure.  On the left is the result after dome calibration and on the right
after dark sky self-calibration.  The instrumental effects seen in the left
panel are the pupil ghost pattern, fringing, and an illumination pattern.
This is a challenging example because bright sources overlap the pupil ghost
pattern.  The bright lines on the edges of the CCDs are an anti-aliasing
display artifact. \label{fig:rawcal}}
\end{figure}

\begin{figure}[h]
\begin{center} 
\includegraphics[width=.3\columnwidth]{Figs/pgr.eps}
\hfil
\includegraphics[width=.3\columnwidth]{Figs/frg.eps}
\hfil
\includegraphics[width=.3\columnwidth]{Figs/sft.eps}
\end{center}
\caption{These panels show the three calibrations extracted,
successively, from a dark sky image produced from KPNO Mosaic z'-band
exposures taken on one night.  The exposure shown in
figure~\ref{fig:rawcal} is from this dataset and the calibrations
shown here were used on that exposure.  The left panel shows the
pupil ghost template occupying the central region of the exposure.
The middle panel shows the fringe template for two of the eight CCDs.
The right panel shows the illumination flat field for the whole field.
The left and middle panels cover one-fourth of the field (4K x 4K).
Note the presence of fringes in the pupil template and the
corresponding absense of fringes in the upper left corner of the
middle panel where the pupil pattern was removed.  This illustrates
the difficulty of disentangling effects.\label{fig:skycal}}
\end{figure}


If the pupil ghost is to be removed from the exposures for the dark
sky calibration are combined using their associated object masks and
scaled to a common mean sky.  A template for the pupil pattern is
extracted using a algorithm developed for this purpose and described
in the IRAF documentation.  Basically a data file defines the location
of the pattern, a background is determined and subtracted based on
inner and outer rings,
Because the pattern is faint and, despite the object masks, there may
be light from sources or cosmic rays it is important to exclude any
large features.  This is done by s

The pixels within the pupil pattern, as defined by a mask are,
clipped (replaced by an upper or lower threshold value) where
the thresholds are determined by an three iterations of sigma clipping
method using a 3 sigma lower rejection and a 5 sigma upper rejection.
three iterations. The clipping excludes pixels from the statistics
in each iteration and then any pixels outside the final limits are
set to 

The outlier pixels are replace in the template by the threshold values
when storing the template in the calibration library.  This is because
if this template is used by other data where the pupil pattern
template cannot be determined there will be no mask for these pixels.

Once a pupil pattern is determined by self-calibration or, failing
that, from the calibration library.  Note that if there is no suitable
library template then the pupil ghost removal is not performed and the
pipeline data products will not be fully calibrated.

While the structure of the pupil pattern is relatively stable the
amplitude may vary from exposure to exposure.  This is because the
amount of reflected light forming the ghost will depend on the amount
of source, sky, and even light from outside the field of view.
Therefore, the pupil pattern template must be scaled and subtracted
from each exposure.  The pipeline allows this to be done per CCD but
normally a common scaling is determined from all the affected CCDs
even though this results in a synchronization in the data parallel
processing.  This is compensated for by processing multiple datasets
in parallel.

The fitting of the template to an exposure is hard because the pattern
is faint and diffuse and the exposure may have sources anywhere in the
pattern.  The fitting is optimized by using the template mask to look
only at pixels containing the pattern and using a weighting that is
gives more weight to places where the template is stronger.  The
result of the fitting is a single number that is multipled into the
pattern prior to subtracting from the exposure.

\subsection{Dark Sky Flat\label{sft}}

Dark sky flat fields provide the ultimate response calibration on all
scales.  At one extreme is the calibration of individual pixels to a
uniform response to the sky background and at the other is correction
for illumination patterns.  At an intermediate scale there is
normalizing the response of the amplifiers and CCDs to the sky.

The calibrations must avoid introducing more noise.  This is hardest
with the pixel response.  The pipeline provides the possiblitly of
applying only a large scale calibration when the data are not adequate
for the individual pixel calibration.

{\bf Forced decision}:  There is a pipeline configuration parameter
for requiring smoothing, requiring no smoothing, or make the decision
based on data quality criteria.

<\bf Smoothing}:  If smoothing of the dark sky flat field is selected,
either by a fixed requirement or by the result of the data quality
analysis, another pipeline configuration parameter defines a median
filtering window.

Once a dark sky flat field is identified (or not) it is applied to
each exposure within the data set.

If a new dark sky flat field was produced and used it is saved
in the calibration library,
made available to the observers, and made public.


Smoothing verse no smoothing

\section{Photometric Characterization\label{photcal}}

