\contentsline {section}{\numberline {1}Dark Sky Self-Calibrations}{2}
\contentsline {subsection}{\numberline {1.1}Identifying Exposures for Dark Sky Calibrations}{3}
\contentsline {subsection}{\numberline {1.2}General Considerations in the Dark Sky Self-Calibrations}{6}
\contentsline {subsection}{\numberline {1.3}Pupil Ghost and Fringe Pattern Subtraction}{6}
\contentsline {subsection}{\numberline {1.4}Dark Sky Flat}{9}
\contentsline {section}{\numberline {2}Photometric Characterization}{9}
