\documentclass{dpp_doc}
\usepackage{graphicx,url,natbib}

\title{NOAO Mosaic Pipeline Operator's Guide}
\author{F. Valdes$^1$}
\otheraffiliation{
$^1$NOAO Sciece Data Management, P.O. Box 26732, Tucson, AZ 85732\\
\pubhistory{DATE}
\reportnumber{PL020}
\keywords{pipeline, mosaic, operator}
\runningtitle{Mosaic Pipeline Operator's Guide}

\begin{document}
\frontmatter

\tableofcontents
\newpage 

%\listoffigures
%\newpage

%\listoftables
%\newpage

\mainbody

%\begin{abstract}
%
%ABSTRACT
%
%\end{abstract}

\section{Introduction}

An important operator resource is the pipeline FAQ maintained in the
NOAO help desk.  Operators and developers are strongly encouraged to
add the FAQ.

\section{Running the Pipeline}

\subsection{plstatus}

It is a good idea to check that no processes are initially running.

\begin{verbatim}
pipedmn% plstatus
pipedmn% ps x
\end{verbatim}

\subsection{plrun}

The {\tt plrun} command is used to start the pipeline.  It issues
various commands to the pipeline cluster as defined by the environment
variables {\tt DMNODE, PROCNODES}.  It cleans up the working
directories, starts the servers, and starts the pipelines.  The output
indicates what is being done and finishes with a call to {\tt
plstatus}.

The pipeline may be started to either automatically (with the -p flag) or
manually (no -p flag) submit datasets to the pipeline.  Submitting a dataset
means sending a trigger to the first stage of the pipeline.  When
automatically submitting datasets the queue state and dataset status fields
from the PSQ are used by the PSA which is one of the servers started by {\tt
plrun}.  In manual mode the PSA is still used but the {\tt psa} command is
used to select datasets to be submitted to the pipeline.

If the pipeline is stopped before a dataset completes the raw data
will still be on the pipeline cluster.  To restart the pipeline so
that it uses this staged data rather than retrieving it again from the
archive use the {\tt -c restart} option.  Without this option the
clean-up step will remove the raw data.

\subsubsection{Failure modes and Diagnostics}

\begin{itemize}
\item {\tt tail -f $MarioHome/ps.dat}
\end{itemize}

\section{Pipeline Scheduling Queue: PSQ}

The pipeline scheduling queue, PSQ, is a database of datasets defined for
processing.  There are two operator interfaces to this database.  The high
level interface is through a browser.  The URL is
http://pipedmn.tuc.noao.edu/cgi-bin/psq.  Access to this URL is limited to
within the NOAO domain.  However, an NOAO VPN account can be used to access
it from anywhere.  domain

The second interface is through the {\tt psqdb} command which
interprets SQL commands.  This can be done interactively or by making use of standard
input redirection to use command files or pipes.  Because this
provides complete read/write access the operator needs to fully
understand the structure of the PSQ tables and the SQL language.
Therefore, this is recommended only for advanced operators.  This
interface
PSQ and 

The {\tt psqdb} command is a wrapper to hide the specific database server.
However, it is still necessary to know database server client being used
because of variations in the SQL and commands understood.  Currently the PSQ
is implemented in {\tt mysql}.



\begin{thebibliography}{}
\bibitem[Author(DATE)]{REF} Author, A., et al., DATE, PUB, 1234, 56 
\end{thebibliography} 

\end{document}
