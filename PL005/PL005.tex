\documentclass{dpp_doc}
\usepackage{graphicx,url,natbib}

\title{Mosaic Pipeline Observing Protocol}
\runningtitle{Mosaic Pipeline Observing Protocol}
\author{M. Dickinson$^1$, B. Jannuzi$^2$, T. Abbott$^3$}
\otheraffiliation{
$^1$Pipeline scientist: NOAO Data Products Program, P.O. Box 26732, Tucson, AZ 85732\\
$^2$KPNO Mosaic scientist: NOAO Kitt Peak National Observatory, P.O. Box 26732, Tucson, AZ 85732\\
$^3$CTIO Mosaic scientist: NOAO Cerro Tololo Interamerican Observatory, P.O. Box 26732, Tucson, AZ 85732
}
\pubhistory{November 2, 2006}
\reportnumber{PL005}
\keywords{mosaic imager, mosaic pipeline, observing, calibrations}

\begin{document}
\frontmatter

\tableofcontents
\newpage 

%\listoffigures
%\newpage

%\listoftables
%\newpage

\mainbody

\section{Introduction}

All exposures with the NOAO Mosaic Imager are captured by the Observatory
data handling system.  This serves several purposes.

\begin{itemize}
\item Provide a permanent archive of observations from NOAO
facilities.
\item Provide a fail-safe and alternate mechanism for observers to
access their data.
\item Provide community access to NOAO observations after they become
non-proprietary.
\item Provide input to observatory pipelines to provide reduced and
calibrated data products for observers and the community, again after
the proprietary period.
\end{itemize}

This document is concerned with the last item though in principle the
community could start with raw data themselves.  In order to be able to
extract the best reduced and calibrated data products for the community it
is important to acquire appropriate calibrations.  We recognize that some
programs do not require the same level of calibrations or the observing
strategies needed for self-calibration.  So this document describes the
minimal calibrations expected from an observing run as well as what would be
of longer term use for the community.

\section{The NOAO Mosaic Pipeline}

The NOAO Mosaic Pipeline is provides instrumental calibration, both
photometric and astrometric, for each exposure.  Its data products include a
calibrated but unresampled version of the data, as well as a resampled
single image in a standard orientation and format.  In addition it provides
pixel maps of non-linear pixels, saturated pixels, and bleed trail pixels.
In addition to flat fielding, including removal of corrector reflection and
fringing effects, the data are photometrically characterized with an
approximate zero point, photometric depth, and seeing from reference
photometry catalogs; primarily the USNO-B catalog.  The pipeline will evolve
and potentially produce other data products including identification of
transient detections and catalogs, though these are not in the current
pipeline.  For a full description of the current Mosaic Pipeline processing
see \citep{PL003}.

The NOAO Mosaic Pipeline is designed to handle the wide variety of
programs undertaken with the Mosaic Imagers.  Therefore, it is
tolerant of most observing styles.  It also has the advantage of
having access to calibration data from other programs.  However,
the best calibrations are those obtained during the same night or
series of nights.

\section{Daytime Calibrations}

The key requirement on observers is to obtain good sequences of bias and
dome flat exposures.  The pipeline recognizes sequences by consecutive
exposures of a particular type with a minimal time between exposures.  In
order to facilitate this recognition use the appropriate calibration
sequence exposure commands; i.e. {\tt zeros} and {\tt flat} [check this].
The sequences should be at least 10 for the biases and 5 for the dome flats.

Taking bias and dome flat exposures are generally common practice.  However,
occasionally observers plan to reduce their data with solely with sky flats
and not take dome flats.  Even if you do intend to depend only on sky flats
we require that you take a minimal dome flat sequence for all filters you
will use during the night or run.

\section{Nighttime Calibrations}

Nighttime calibrations are those taken ``on the sky''.  These will, of
course, have an impact on the science program.  For this reason we do not
require these but request you consider the broader and longer term use of
the exposures.

\subsection{Sky Flats}

The Mosaic pipeline will recognize twilight sky flats if they are taken with
the appropriate command.  The pipeline will use sky flats but only if there
is no suitable dome flat.  In other words, dome flats are preferred and even
if you plan to use sky flats yourself these will not be used for pipeline
calibration.

\subsection{Standard Stars}

The Mosaic pipeline currently does not make use of standard star
exposures.  In the future it may make use of these.  However, it makes
your exposures of greater potential utility if a minimal number of
standard star exposures are taken in good conditions.

\subsection{Night Sky Sampling for Dark Sky Flat Fields}

The Mosaic pipeline gives great emphasis to trying to derive and use
dark sky stacks to self-calibrate the data.  A dark sky flat is one
where all of the exposures for the night (or possibly several nights)
are coadded to obtain dark sky samples for every pixel in the
detector.  This requires that the observations are dithered and
have relatively low source density or large bright sources.  Clearly,
different programs cannot fulfill this requirement but if it makes
sense to visit fields with the right characteristics and without
requiring sources to remain on the same pixels repeatedly this will
allow better calibration of the entire data set.

\section{Mosaic Pipeline Calibration Data}

The Mosaic pipeline uses a library of calibration data for processing
exposures.  The calibrations are indexed by time as well as detector,
filter, CCD, amplifier as appropriate.  These include:

\begin{description}
\item[crosstalk:] Crosstalk coefficients between pairs of amplifiers.
\item[bad pixels:] Masks of CCD pixels to be labeled as non-linear or
bad for scientific use
\item[WCS solutions:] World coordinate solutions for the non-linear
(distorted) mapping between CCD pixels and relative celestial
coordinates.  The absolute zero point is considered variable and
effects of rotation and refraction are part of the calibration against
reference sources.
\item[processing options:] List of processing steps required for a
particular filter.
\item[readout noise:] The readout noise of the amplifiers.
\item[gain:] The gain of the amplifiers.
\item[magnitude zero point:] The expected magnitude zero point.
\item[saturation:] The raw counts at which non-linear saturation
begins.
\end{description}

These are the more static calibrations which are either used
as is or used as a starting point for refined calibration.  There are
other calibrations in the library consisting of biases, dome flats,
sky flats, pupil ghost templates, fringe templates, and dark sky flats
that are derived, if possible, from each observing run.

We request that people who derive their own calibrations in the areas noted
in the list above consider providing them to NOAO to incorporate in the
pipeline.  This not only will benefit others but potentially your own
ability to use the pipeline data products.  Furthermore, this will aid us in
trend analysis of the camera.

\begin{thebibliography}{}
\bibitem[PL003]{PL003} Valdes, et al., 2006, The NOAO Mosaic Camera Pipeline:
Methods, Data Products, and Science Verification
System, NOAO DPP Document PL003
\end{thebibliography} 

\end{document}
