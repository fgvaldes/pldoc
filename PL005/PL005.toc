\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}The NOAO Mosaic Pipeline}{1}
\contentsline {section}{\numberline {3}Daytime Calibrations}{1}
\contentsline {section}{\numberline {4}Nighttime Calibrations}{2}
\contentsline {subsection}{\numberline {4.1}Sky Flats}{2}
\contentsline {subsection}{\numberline {4.2}Standard Stars}{2}
\contentsline {subsection}{\numberline {4.3}Night Sky Sampling for Dark Sky Flat Fields}{2}
\contentsline {section}{\numberline {5}Mosaic Pipeline Calibration Data}{2}
