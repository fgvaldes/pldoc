\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Pipeline Management System}{2}
\contentsline {subsection}{\numberline {2.1}Pipeline Description Language}{2}
\contentsline {subsubsection}{\numberline {2.1.1}Actions}{2}
\contentsline {subsubsection}{\numberline {2.1.2}Events}{3}
\contentsline {subsection}{\numberline {2.2}Pipeline Manager}{3}
\contentsline {subsubsection}{\numberline {2.2.1}Module Manager}{3}
\contentsline {subsubsection}{\numberline {2.2.2}Blackboards}{4}
\contentsline {section}{\numberline {3}Services}{4}
\contentsline {subsection}{\numberline {3.1}Node Manager}{4}
\contentsline {subsection}{\numberline {3.2}Directory Server}{4}
\contentsline {subsection}{\numberline {3.3}Pipeline Selection Utility}{5}
