\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Science Pipeline Operations Model}{2}
\contentsline {section}{\numberline {3}Pipeline Datasets}{3}
\contentsline {section}{\numberline {4}Pipeline Scheduling System}{3}
\contentsline {subsection}{\numberline {4.1}Pipeline Scheduling Queue Database}{4}
\contentsline {subsection}{\numberline {4.2}Pipeline Scheduling Agent}{4}
\contentsline {subsection}{\numberline {4.3}Populating the PSQDB}{5}
\contentsline {subsection}{\numberline {4.4}Managing the PSQDB}{7}
\contentsline {subsection}{\numberline {4.5}Operations with the PSA/PSQDB}{7}
\contentsline {subsubsection}{\numberline {4.5.1}Submitting datasets}{7}
\contentsline {subsubsection}{\numberline {4.5.2}Completing a dataset}{8}
\contentsline {section}{\numberline {5}Pipeline Parameters}{9}
\contentsline {section}{\numberline {6}Pipeline Data Manager and Calibration Library}{9}
\contentsline {section}{\numberline {7}Data Management and Science Support}{10}
\contentsline {section}{\numberline {8}Pipeline FTP Distribution}{11}
