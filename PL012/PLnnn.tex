\documentclass{dpp_doc}
\usepackage{graphicx,url,natbib}

\title{NOAO Pipelines Operational Model}
\author{
F. Valdes$^1$}
\otheraffiliation{
$^1$NOAO Data Products Program, P.O. Box 26732, Tucson, AZ 85732}
\pubhistory{December 25, 2007}
\reportnumber{PL012}
\keywords{pipeline, operations}
\runningtitle{Pipeline Operational Model}

\begin{document}
\frontmatter

\tableofcontents
\newpage 

%\listoffigures
%\newpage

%\listoftables
%\newpage

\mainbody

\begin{abstract}

This document describes the operational model and components for the NOAO
pipelines applications, which currently operate on Mosaic and NEWFIRM camera
data.

\end{abstract}

\keywords{techniques: image processing}

\section{Introduction}

This technical report provides information about external interfaces and
databases used by the NOAO pipeline applications.  

The intended audience for this report are pipeline operators,
collaborators, and developers.  This includes pipeline developers for
other projects who may be interested in the architectural details,
interfacing with the databases, or even potential reuse of components.

The report provides a high level description of the interfaces and
databases along with some fairly specific details.

In order to describe the purposes and roles of the interfaces and
databases it is necessary to understand the operational model and
context of the NOAO pipeline system and applications.

The pipelines are applications of the
the NOAO High-Performance Pipeline System \citep{NHPPS}.

\section{Pipeline Operations}

The NOAO science pipeline applications operate as follows.

The science pipelines run continuously on a cluster of machines.  One
component of the pipeline, the pipelines scheduling agent, has knowledge of
the telescope and instrument schedules.  After a block of one or more nights
of observations with a pipeline supported instrument is completed this
component initiates a query to NOAO Data Management and Science Support
services for the data identifiers (a kind of URI).  These URIs are used to
trigger the appropriate pipeline application.  The pipeline application
interacts with the DMaSS to get the raw data, processes the data, and
finally queues the pipeline processed data products for the DMaSS to
incorporate as well as staging tar files in a password protected FTP staging
area.  The end user, or customer, gets the pipeline data products through
the NOAO Portal into the DMaSS or through the FTP Portal staging area.

The pipeline applications make use of a number of a number of
databases.  These are
and file




\section{Pipeline Datasets}

The NOAO pipelines operate on datasets.  These are arbitrary collections
data, possibly of different types, which are processed as a group.
The pipeline applications process datasets in a kind of hierarchical
decomposiiton.  This means that a top level dataset is broken down
into smaller datasets for processing by various pipelines which may,
in turn, further decompose the dataset for even lower level pipelines.
The applications also regroup data into different kinds of datasets
such as for basic calibration and for stacking.

For the NOAO science pipelines the top level datasets consist of blocks of
consecutive nights irrespective of the proposal.  The size of the
blocks is a trade-off between active disk space and the potential for
cloudy nights and shared calibrations between data from different
proposals.

Using more than one night in a block allows the pipeline application to
decide at a lower level how to break up data depending on the number of
exposures.  This is primarily important for defining data from which dark
sky self-calibrations (eg. sky flats) are made.  For example, suppose one
night was clouding and for one filter the observers took only a few
exposures per night in the other nights.  The pipeline could then make
datasets for a single night for those cases where there were 20 exposures
for a filter in night and group several nights for filters with just one or
a few exposures per night.

The strategy for making the top level datasets is described further
in the section on the Pipeline Scheduling System.  Briefly, the
current operational model is to define datasets in blocks of three
to four nights while attempting to keep proposal runs together.  Since
the NOAO telescopes are currently scheduled "classically" with
typically runs of between 3 and 8 nights this leads to the use of
the 3 to 4 night dataset blocks.  However, if the telescope schedule
warrents the pipeline may process single nights (such as from
engineering nights) or 2 night runs.

\section{Pipeline Scheduling System}

There are two approaches to running a science pipeline.  One is as a
service that some external system or agent directs to process data.
The second is for the pipeline to actively discover and process data.
In discussions within DPP it was decided that the DPP science
pipelines use the second approach.  Philosophically, this is
equivalent to assigning responsibility to the pipeline developers
for an operational system to feed the pipeline.

The DPP pipeline team developed a pipeline scheduling system built
around a {\em Pipeline Scheduling Queue Database} (PSQDB).  The components
that interact with the PSQDB are a {\em Pipeline Scheduling Agent}
(PSA), an SQL interface, a browser UI, and a pair of tools that extract
information from the NOAO telescope schedule database and insert
this information into the PSQDB.

The system has a great deal of flexibility and does not impose many
constraints or how data is processed.

\subsection{Pipeline Scheduling Queue Database}

The Pipeline Scheduling Queue Database has a three layered hierarchy of
tables.  The top-level table defines logical {\em queues} ({\tt psqname})
for the operator to conveniently control multiple instruments or large scale
data groupings.  Each of these queues references a dataset table ({\tt
queue}) and a data table ({\tt data}).  he main operational queues are
defined as data from an instrument at a telescope for a semester.  For
example, the queue C4M07B is for C(TIO) 4(-meter) M(osaic camera) data for
the (20)07B semester.  There are queues for KPNO Mosaic and NEWFIRM data for
this and other semesters.  Table 1 shows the schema for this table.

\begin{figure}[h]
\caption{PSQDB queues table: PSQ.\label{fig:PSQtable}}
\begin{verbatim}
    psqname     char(8)         C4M07B
    queue       char(8)         C4M07B
    data        char(8)         C4M07BD
    application char(8)         MOSAIC
    pipeline    char(8)         dir
    state       char(16)        enabled
    query       varchar(256)    instrument='mosaic_2'
\end{verbatim}
\end{figure}

The dataset table has records with a 

\begin{figure}[h]
\caption{PSQDB dataset table: e.g. C4M07B.\label{fig:Qtable}}
\begin{verbatim}
    dataset     char(32)	20071222	
    priority    int		1
    status      char(16)	completed
    submitted   char(16)	2007-12-25T02:54
    completed   char(16)	2007-12-25T12:21
\end{verbatim}
\end{figure}

The data tables have records indexed by a dataset name ({\tt name})
which links to one or more records in the dataset tables through the
dataset name ({\tt dataset}).  The other fields of the data table
contain 

\begin{figure}[h]
\caption{PSQDB data table: e.g. C4M07BD.\label{fig:QDtable}}
\begin{verbatim}
    name        char(32)	20071222
    start       int		20071222
    end         int		20071224
    subquery    varchar(256)	date between 20071222 and 20071224
\end{verbatim}
\end{figure}

\subsection{Pipeline Scheduling Agent}

The {\em Pipeline Scheduling Agent} (PSA) is the key component for
autonomous operation of the pipeline applications and satisfying the
priorities and requirements of NOAO management and
customers.  The current priorities are 1) process data shortly after
the proposal "run" is over and 2) process any other runs which have
been queued.

The PSA checks the PSQDB for pending datasets in the active queues of
the PSQDB when it is first started, when it recieves a dataset processing
completed event, and at times when a new dataset is scheduled to be
available.

The PSA has a configuration parameter that defines how many datasets
may be submitted to a particular pipeline application at one time.
At this time the PSA does not directly check for active dataset but
only uses the status flag of the PSQDB which records submitted
datasets.  If the pipeline is reinitialized for some reason, such as
a crash, the operator must use one of the PSQDB interface tools to
reset the dataset status flag back to pending.

Figure~\ref{fig:PSAlog} shows output from the PSA log.  In this example the
the PSA has just been started and there are no back datasets pending.  It
simply sets a wake up time when the next dataset is expected to be
available.  Note that the offset between the end of the observing block and
the wake up time is set to allow time for the DPP data transport system to
get the data to the archive and for the archive to ingest the data so that
it can be queried.  This delay is expected to decrease in the future.

\begin{figure}[h]
\caption{Example PSA log output.\label{fig:PSAlog}}
\begin{verbatim}
PSA SUBMIT: Thu 18:08:01 27-Dec-2007
Selected Queue(s), Application(s), Pipeline(s):
        K4M07B, MOSAIC, dir
        C4M07B, MOSAIC, dir
Processing pipeline: MOSAIC:dir
        Currently submitted 0/1
        No real-time datasets queued at the moment
        Submitting 0 entries:
Looking for datasets with end dates greater than 20071225
Checking queue K4M07B K4M07BD
        No future end dates found
Checking queue C4M07B C4M07BD
        Next end date: 20080101
                selected as new next end date...
Scheduled to wake up and submit real-time datasets with end date of
20080101 on 20080102 22:00 UTC
Current time: 20071227 18:08 UTC
\end{verbatim}
\end{figure}


\subsection{Populating the PSQDB}

The tools ({\tt getruns} and {\tt updPSQ}) in this component are what
implement the operational processing model or policy.  The approach is to
have an operater run a tool run once a semester on the final telescope
schedule database to automatically define initial datasets.  The output is
text which an operator can then manually adjust as needed.  Since this is
only done once a semester the interactive stage is justified.  The final
adjusted output is then fed into another tool that creates and inserts the
data and queues into the PSQDB.

The telescope extraction tool is design around the model described
in the Pipeline Dataset section.  It identifies the lengths of runs
for each proposal and divides them into groups of three to
four nights if the runs are longer than four nights.  Shorter runs
are their own datasets which the operator may merge if appropriate.
Split night proposals are treated as single proposals for this
operations.

The use of generic SQL allows other possiblities.  

\subsection{Managing the PSQDB}

The most general way to manage the PSQ database is through the native
SQL interface of the DBMS.  The current DBMS is {\bf \tt mysql} though
clearly any DBMS may be used.  Use of this interface requires modest
proficency with SQL and understanding of the PSQDB tables.  While
an understanding of the available NSA queries is useful for creating
new entries or making specialized entries, an operator may not need
this.

There are many possiblities for creating specialized GUIs
using systems which provide a programmatic interface to the DBMS.  DPP
currently provides a browser interface through ??? which allows
examining the tables, navigating to linked tables through HTML links,
and modifying some fields through menus.  In particular this
interfaces allows changing the {\tt state} of a queue and the {\tt
status} of a dataset.

Backup and restoring the database falls under the tools provided by
the DBMS.

\section{Pipeline Data Manager}

The {\em Pipeline Data Manager} (DM) is a server that interfaces distributed applications to data management services.  Communications is through a socket using our simple messaging protocol.  In our system the Data Manager provides access to two primary services; a database, called the Pipeline Metadata Archive System (PMAS), and a Calibration Library.

The primary purpose of the DM is to support pipeline applications. It is a key component of the NOAO pipelines.  A secondary purpose is to support management of processing information. Pipeline modules may be configured to communicate their status to the PMAS.  In this configuration, the PMAS logs the start and end of module execution along with information about the triggers and nodes.  This information may be used for diagnostics, reports, and various pipeline module purposes.

The current DM interfaces to a single database system.  The database supports both the calibration library and the Pipeline Metadata Archive System.  The cuurent DM also includes support for distributed proxy DMs which maintain copies of the data synchronized with a master DM.  This allows an integrated data management system when pipeline applications run in several locations, such as at a telescope and at a data processing centers, and improves the scalability of this function for a large number of pipeline nodes needing frequent access to the DM.

As with the core components, there are a number of client programs and methods associated with this component.

\subsubsection{Pipeline Metadata Archive System}

The Pipeline Metadata Archive System (PMAS) serves the role of a metadata management system for the distributed pipeline system and applications.  Access is through the data manager server interface and clients.  It is ultimately a general SQL database.  There are tables specific logging the status of modules in a pipeline as well as tables specific to the pipeline application.

The data manager server provides convenience methods for getting and putting information as well as a method to pass through SQL queries and return one or more records.  The pipeline system provides client applications for getting and putting information from pipeline modules.

\subsubsection{Calibration Library}

The purpose of the calibration library is to maintain calibration information indexed by a set of attributes used to select an appropriate calibration for a particular observation.  There are two distinct types of calibration information; parameters and files.  The calibration library therefore requires two elements; a database and a file repository.  The database has multiple roles.  It provides the indexing as well as the calibration parameter information.

The indexing attributes in our calibration library are: detector, image identifier, filter, a quality rank, and starting and ending valid dates.  The detector and image identifier attributes are needed to support multiple instruments and mosaics and the quality rank is used to give greater weight to calibrations which have been deemed of higher quality.

The date attributes are a fundamental characteristic of a calibration library.  It allows evolution of the calibrations due to changes in the hardware and performance of the instrument.  There are two main types of date dependence which occur in our library.  One is for self-calibrations needed to account for day-to-day variation in the characteristics of the detector.  Typical examples of this are bias and dome flat calibrations.  The other is for significant changes in the hardware, such as replacement of a detector, for changes in the processing recipe, such as processing rules, and improvements in the instrument characterization.  As part of the query to the calibration library the date of the observation is provided.  Only calibrations with date ranges that include the observation are considered.  When multiple choices are available the quality ranking and proximity to the middle of the range are used to select the best calibration.  In future releases, the data manager will be enhanced to provide more sophisticated selection rules.

The calibration library supports an arbitrary set of calibration classes.  The required classes are defined by the needs of the pipeline.  Examples of classes are dome flats and crosstalk coefficients. A typical query is for a particular class given a date and date range, detector, image identifier, and filter.  The result of the query is to return the parameter value or the path, in network syntax, to the client.  For files there is additional information provided consisting of the file size and modification time.  The purpose of this extra information is to allow the client to maintain a cache and only ``pull'' a copy from the calibration file repository when it does not have a current copy.

Our system provides client applications for getting and putting calibration information through the data manager server interface.  The applications are used within pipeline modules.  The ``get'' application implements a local cache to minimize file transfers.  It may be driven from images where the selection attributes are determined from the image header and the return file and parameter information are recorded in the header.

\section{Data Management and Science Support}

The {\em Data Management and Science Support} (DMaSS) component of the
NOAO/DPP integrated system is the source and sink for the pipeline
applications.  There are three services required by these pipeline
applications; a query for data holdings, a request for staging the data, and
queuing of pipeline data products for assimilation.  The end consumer of the
pipeline data products obtains them through a portal into the DMass.

The first is a data holdings query service.  As described in
section~/ref{PSQDB}, the PSA initiates a request for data from a
particular dataset (block of nights) based on the telescope schedule.
The query encapsulated in the PSQDB is sent to the DMaSS QS.  The
services returns a list of identifiers for the raw data constituting
the observational dataset.  The possible results may be a list of
identifiers, an empty list, or status messages such as data not yet
available.  The null list is, unfortunately, an occassional result for
NOAO instruments due to bad weather or instrument problems.

When the list of identifiers is not empty the pipeline applications
submits the identifiers to the DMaSS replication service to retrieve
and stage the data.  There are two current versions of this process,
one where the pipeline applications pulls the data to its staging area
and one where the RS stages the data to the pipeline staging area.
In both approaches, the pipeline applications is ready to process the
dataset when the data has been placed in its staging area.

The third service is for queuing the pipeline data products for
assimilation into the DMaSS system.  There are two components that
provide this interface.  The interface for the pipeline applications
is simple.  Data files are queued through the standard Unix printer
queue commands.  As a brief note, since what happens beyond this point
is outside the pipeline system, there is a daemon that handles the
queued files by ultimately data to a DMaSS ingestion service.

\subsection{Pipeline FTP Distribution}

There are circumstances where it is desirable for the pipeline to more
directly provide data products to consumers.  This is accomplished by
a pipeline application stage that packages data products in a user
oriented file format -- FITS, HTML, PNG, or TAR files -- and deposits
them in an FTP or HTTP staging area.

This requires the pipeline to be concerned with proprietary data
issues normally handled by the portal interface to the DMaSS.  This is
accomplished by segregating data products by the proposal identifier
associated particular data products.  The pipeline uses a proposal
identifier included in the raw observation data.

The pipeline has another database, the Pipeline FTP Database, which
indexes FTP staging directories by the proposal identifier.  The
pipeline application places the formated data products in the
location specified in the database.  DPP operations provides
password protected access to these staging areas where proposal
principle investigators are provided with a means to obtain the
password for their proposal.

The Pipeline FTP Database is like the PSQDB in that it can be prepared
in advance based on the accepted proposal database which defines the
link between proposal identifiers used during observing and the PIs.

\section{Dummy}

The operational components for the pipelines consist of

\begin{verbatim}
PSA
PSQDB
PSQMON
NSA
DM
FTPDB
Catalog services
PROBDB
\end{verbatim}

\begin{thebibliography}{}
\bibitem[Cline(2007)]{adass1} Cline, T., Pierfederici, F., Swaters, R.,
Thomas, B., \& Valdes, F., 2007, ASP Conf. Series, in preparation
Pierfederici, F. \& Miller, M., 2007, ASP Conf. Series, in preparation
\bibitem[NOAO: Muller(1998)]{MosaicImager} Muller, G., 1998 procspie, 3355, 577
\bibitem[Swaters(2007)]{adass2} Swaters, R. \& Valdes, F., 2007,
ASP Conf. Series, in preparation
\bibitem[Valdes(2007a)]{adass3} Valdes, F. \& Swaters, R., 2007a,
ASP Conf. Series, in preparation
\bibitem[Valdes(2007b)]{pasp2} Valdes, F., Swaters, R., Pierfederici,
F., Miller, M., \& Zarate, N., 2007b, pasp, in preparation
\bibitem[Valdes(2007c)]{pasp3} Valdes, F., Swaters, R. \& Dickinson, M.,
2007c, pasp, in preparation
\bibitem[NHPPS: Valdes(2007)]{NHPPS} Valdes, F., Cline, T.,
Pierfederici, F., Miller, M., Thomas, B., \& Swaters, R., 2007, NOAO
DPP Document PL001,
http://chive.tuc.noao.edu/noaodpp/Pipeline/PL001.pdf
\end{thebibliography} 

\end{document}
