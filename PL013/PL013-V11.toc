\contentsline {section}{Purpose of this Document}{2}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {subsection}{\numberline {1.1}Pipeline Applications versus Pipelines }{3}
\contentsline {subsection}{\numberline {1.2}Datasets }{4}
\contentsline {subsection}{\numberline {1.3}The MOSAIC and NEWFIRM Pipelines}{5}
\contentsline {subsection}{\numberline {1.4}Pan-STARRS, DECam, and LSST Pipelines}{6}
\contentsline {subsection}{\numberline {1.5}Pipeline Frameworks}{6}
\contentsline {section}{\numberline {2}Calibration Processing }{7}
\contentsline {section}{\numberline {3}Limiting the Parallelization}{8}
\contentsline {section}{\numberline {4}ODI Data Flow Overview }{9}
\contentsline {section}{\numberline {5}ODI Data Flow Details and Pipeline Decomposition }{12}
\contentsline {subsection}{\numberline {5.1}TOP Pipeline}{12}
\contentsline {subsubsection}{\numberline {5.1.1}Staging the Data}{14}
\contentsline {subsubsection}{\numberline {5.1.2}Sequencing the Dataset}{14}
\contentsline {subsection}{\numberline {5.2}CAL Pipeline}{15}
\contentsline {subsection}{\numberline {5.3}SEQ Pipeline}{15}
\contentsline {subsection}{\numberline {5.4}OCL Pipeline}{15}
\contentsline {subsection}{\numberline {5.5}FTR Pipeline}{16}
\contentsline {subsection}{\numberline {5.6}EXP Pipeline}{16}
\contentsline {subsubsection}{\numberline {5.6.1}Astrometric Calibration}{16}
\contentsline {subsubsection}{\numberline {5.6.2}Photometric Characterization}{17}
\contentsline {subsection}{\numberline {5.7}OTA Pipeline }{17}
\contentsline {subsubsection}{\numberline {5.7.1}Crosstalk}{18}
\contentsline {subsubsection}{\numberline {5.7.2}Overscan}{19}
\contentsline {subsubsection}{\numberline {5.7.3}Masking and Replacement}{19}
\contentsline {subsubsection}{\numberline {5.7.4}Merging}{19}
\contentsline {subsubsection}{\numberline {5.7.5}Bias or Dark Subtraction}{19}
\contentsline {subsubsection}{\numberline {5.7.6}Flat Fielding}{20}
\contentsline {subsubsection}{\numberline {5.7.7}Cosmic Ray Masking}{20}
\contentsline {subsubsection}{\numberline {5.7.8}Pupil Pattern Subtraction }{20}
\contentsline {subsubsection}{\numberline {5.7.9}Catalog Generation and Astrometric Calibration}{21}
\contentsline {subsubsection}{\numberline {5.7.10}Resampling }{21}
\contentsline {subsection}{\numberline {5.8}SKY Pipeline}{21}
\contentsline {subsection}{\numberline {5.9}PGR and SPG Pipelines}{22}
\contentsline {subsection}{\numberline {5.10}FRG and SFR Pipelines}{22}
\contentsline {subsection}{\numberline {5.11}SFT and SSF Pipelines}{23}
\contentsline {subsection}{\numberline {5.12}RSP Pipeline}{23}
\contentsline {subsection}{\numberline {5.13}STK Pipeline}{24}
\contentsline {subsubsection}{\numberline {5.13.1}PSF Matching }{24}
\contentsline {subsubsection}{\numberline {5.13.2}Harsh and Final Science Stacks}{24}
\contentsline {subsection}{\numberline {5.14}OSK Pipeline}{24}
\contentsline {subsubsection}{\numberline {5.14.1}Matching the Harsh Stack to an OTA}{25}
\contentsline {subsubsection}{\numberline {5.14.2}Difference Detection}{25}
\contentsline {subsection}{\numberline {5.15}SCI Pipeline}{25}
\contentsline {subsection}{\numberline {5.16}DPS Pipeline}{26}
\contentsline {subsection}{\numberline {5.17}EDP Pipeline}{26}
\contentsline {subsection}{\numberline {5.18}DTS Pipeline}{27}
\contentsline {section}{\numberline {6}Error Handling}{27}
\contentsline {section}{\numberline {7}Examples }{27}
\contentsline {subsection}{\numberline {7.1}Setting up the data flow in the TOP pipeline}{27}
\contentsline {subsection}{\numberline {7.2}Master Biases}{29}
\contentsline {subsection}{\numberline {7.3}Master Dome Flats}{30}
\contentsline {subsection}{\numberline {7.4}Static On-sky Exposures}{30}
\contentsline {subsection}{\numberline {7.5}On-sky Guided Exposures}{32}
\contentsline {subsection}{\numberline {7.6}Finishing up in the TOP pipeline}{33}
\contentsline {subsection}{\numberline {7.7}Example Pipeline State}{33}
\contentsline {section}{\numberline {8}Conclusions}{34}
