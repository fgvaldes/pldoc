47,52c14,21
< collected by the DCA from the various acquisition systems; specifically NOCS
< and Monsoon.  It provides a programmable interface between the acquisition
< systems and the science and engineering data handling systems so those
< "up-stream" systems don't need to concern themselves with keyword name
< spaces, value formats, derived metadata, or auxilary information not
< directly related to their function.
---
> collected by the DCA from NOCS and Monsoon.  It provides a programmable
> interface between the acquisition systems and the science and engineering
> data handling systems so those "up-stream" systems don't need to concern
> themselves with keyword name spaces, value formats, derived metadata, or
> auxilary information not directly related to their function.  An
> operation benefit of this architecture is that it was possible to
> fix metadata problems quickly during comissioning independently of
> the acquision components.
55,66c24,31
< and disposing of the metadata.  The primary result of this is populating the
< headers of the final multi-extension format (MEF) FITS files for each
< exposure.  The FITS files are the primary science data which flow to the
< observer, the archive, and the quick reduce pipeline.
< 
< One role of the KTM is to define what metadata is included with the science
< data.  The choice was made to limit the metadata to information of use to
< the three main consumers; the observer, the archive, and the pipeline.  What
< this means is that primarily engineering telemetry is excluded.  Instead the
< KTM provides another output file with all the raw telemetry.  NOAO is now
< beginning a project to flow this data to an engineering archive service that
< stores it in a database and provides access facilities for the engineers and
---
> and disposing of the metadata.  It ensures the metadata is complete and
> compatible with archive, pipeline, and user reductions systems such as IRAF.
> The primary output of KTM is the set of keywords and values for the headers
> of the final multi-extension format (MEF) FITS files.  Primarily engineering
> telemetry is excluded from the FITS files and, instead, the KTM provides
> another output file with all the raw telemetry.  NOAO is now beginning a
> project to flow this data to an engineering archive service that stores it
> in a database and provides access facilities for the engineers and
69,74d33
< Another important role of the KTM is to ensure the metadata is
< complete and compatible with archive, pipeline, and user reduction
< (IRAF) systems.  During NEWFIRM commisioning it was possible to
< quickly update and fix problems with the metadata without requiring
< changes in data acquisition systems.
< 
76,94c35,52
< accessed using an API with TCL bindings.  Note that the DCA simply passes
< this telemetry blindly and does not modify or interpret any of the content.
< The KTM is a TCL program.  The input is a set of "database" pointers and the
< output are files and database pointers for the primary and extension headers
< of the MEF FITS file.  The KTM also reads auxilary data files that control
< the behavior the KTM and provide additional metadata, such as iniital world
< coordinate descriptions.  When the KTM completes, the DCA populates the FITS
< file so the KTM does not need to be involved in the mechanics of FITS
< headers.  It does, however, have to generate the keywords and values that
< conform to the FITS standards.
< 
< Rather than code all the possible telemetry in the KTM TCL script,
< configuration files are used.  This file lists all the expected
< telemetry keywords and provides flags for whether the information is to be
< included in the science (FITS file) metadata.  It also provides a
< level of remapping for the telemetry keywords (which need not be FITS
< style or length) and the comment strings.  For the most part, the KTM
< simple passes on the information marked as science metadata to the
< final headers.
---
> accessed using an API with TCL bindings by the KTM TCL script.  Note that
> the DCA simply passes this telemetry blindly and does not modify or
> interpret any of the content.  The input to the KTM is a set of "database"
> pointers and the output are files and database pointers for the primary and
> extension headers of the MEF FITS file.  The KTM also reads auxilary data
> files that control the behavior the KTM and provide additional metadata,
> such as iniital world coordinate descriptions.  When the KTM completes, the
> DCA populates the FITS file so the KTM does not need to be involved in the
> mechanics of FITS headers.  It does, however, have to generate the keywords
> and values that conform to the FITS standards.
> 
> Rather than code all the possible telemetry in the KTM, configuration files
> are used.  This file lists all the expected telemetry keywords and provides
> flags for whether the information is to be included in the science (FITS
> file) metadata.  It also provides a level of remapping for the telemetry
> keywords (which need not be FITS style or length) and the comment strings.
> For the most part, the KTM simple passes on the information marked as
> science metadata to the final headers.
97,117c55,71
< acquistion systems.  The KTM includes any telemetry values not
< included in the configuration file to the FITS header.  In other
< words, if the KTM doesn't explicitly know about telemetry the default,
< and safe action, is to include in the final metadata.  Later, the KTM
< and configuration file can be updated for this new information.  This
< decouples the acquisition software from coordinating with the KTM
< in a tight fashion.
< 
< As noted earlier, the primary output of the KTM is the science metadata for
< the exposure FITS file.  Another output is a file of all the engineering
< telemetry.  And lastly, a file with the information for triggering the
< NEWFIRM Quick-Reduce Pipeline is written.  The KTM, and the file it writes,
< does not trigger the pipeline.  Instead, the contents of the file provide
< sequence information used by a post processing command to define the trigger
< filenames.  The KTM manages this information because the trigger filenames
< encode sequence and end of sequence information provided by the NOCS.  A
< complexity also handled by the KTM are situations where the sequences end
< permaturely, either by user initiative or by system failure.  Because the
< information about sequences which element of a sequence is being read out is
< part of the metadata, only the KTM is aware of sequences; the DCA is only
< concerned with the current exposure.
---
> acquistion systems.  The KTM includes any telemetry values not defined in
> the configuration file in the FITS header.  Later, the KTM and configuration
> file can be updated for this new telemetry.  This decouples the acquisition
> software from coordinating telemetry metadata with the KTM in a tight
> fashion.
> 
> Besides the FITS and engineering files, the KTM produces a file with
> information for triggering the NEWFIRM Quick-Reduce Pipeline.  The KTM, and
> the file it writes, does not trigger the pipeline.  Instead, the contents of
> the file provide sequence information used by a post processing command to
> define the trigger filenames.  The KTM manages this information because the
> trigger filenames encode sequence and end of sequence information provided
> by the NOCS.  A complexity also handled by the KTM are situations where the
> sequences end permaturely, either by user initiative or by system failure.
> Because the information about sequences and which element of a sequence is
> being read out is part of the metadata, only the KTM is aware of sequences;
> the DCA is only concerned with the current exposure.
123,126c77,80
< The NEWFIRM system uses this facility to queue exposure to the data
< transport system to the archive and to the quick reduce pipeline.  The
< latter makes use of a file produced by the KTM to specify filenames for the
< file triggers expected by the QRP.  The pipeline triggering requires sending
---
> The NEWFIRM system uses this facility to queue exposures to archive via the
> data transport system and to the quick reduce pipeline.  The latter makes
> use of a file produced by the KTM to specify filenames for the file triggers
> expected by the QRP.  The pipeline triggering requires sending
135,137c89,90
< observing environment.  In this section we call attention to one task that
< is commonly used during observing.  The task is called {\tt nffocus} and it
< analyzes focus sequences.
---
> observing environment.  The main task used in this environment is
> {\tt nffocus} to analyze focus sequences.
141,177c94,108
< the focus is changed as specified by the observer.  The focus value for each
< exposure is recorded along with the sequence information.  When the sequence
< is completed the observer runs the {\tt nffocus} task simply referencing a
< list of exposures, one exposure in the sequence or, most succinctly, a
< single exposure number.  The list method allows control over the input while
< the single image or exposure number is a simple way to analyze the whole
< sequence.
< 
< The brighter sources in each selected exposure are detected and cataloged.
< If a sky exposure is included in the input, the source detection is done in
< difference mode.  Difference mode is essentially source detection with an
< on-the-fly pair-wise sky subtraction (though there are some subtle
< distinctions related to handling sky levels and noise properties).  The
< cataloging measures a full width at half maximum (FWHM) for the sources and
< those with FWHM more than 2.5 times the mode are filtered from the output.
< 
< Once the catalogs created, a process that takes on the order of a minute for
< a typical sequence , they are analyzed and displayed.  Note that the
< program only creates the catalogs if need so that it may be rerun to
< quickly return to the analysis of the catalogs.
< 
< The sources in the catalogs are matched spatially.  Since the NEWFIRM focus
< recipe does not dither the exposures, the matching is a simple minimum
< distance threshold in pixel position.  Initially only those sources matched
< in all exposures are used, to avoid biases, and sigma clipping eliminates
< significant outliers which are bad detections or extended sources..  The
< FWHM values of the matched stars as function of focus value are displayed
< and the user has a number of options for deleting bad data and for examining
< the focus variations spatially.  At each step the task computes a "best"
< focus value which the observer may adopt or the observer may estimate from
< the graphs.
< 
< The routine generally works well, though when the number of sources is low,
< as in sparse fields, and the seeing is not stable special steps are taken by
< the program to accomodate incomplete matching This means when few or no
< sources are found in all exposures of the sequence then sources with some
< focus values missing are used.
---
> the focus is changed as specified by the observer.  The brighter sources in
> each (selected) exposure of the sequence are cataloged.  If a sky exposure
> is used the source detection is done in difference mode, which is
> essentially detection with sky subtraction.  The cataloged sources include a
> full width at half maximum (FWHM) and those with FWHM more than 2.5 times
> the mode are filtered from the output.
> 
> The sources in the catalogs are matched spatially.  Initially only those
> sources matched in all exposures are used, to avoid biases, and sigma
> clipping eliminates significant outliers from extended sources.  The FWHM
> values of the matched stars as function of focus value are displayed and the
> user has a number of options for deleting bad data and for examining the
> focus variations spatially.  At each step the task computes a "best" focus
> value which the observer may adopt or the observer may estimate from the
> graphs.
190,226c121,140
< stacked images with bad pixels and mosaic gaps removed.  Final science
< quality processing is performed after the data are transported and archived
< to a data center where a larger pipeline processing cluster is available.
< The science pipeline requirement is that final data products are available
< within 48 hours.
< 
< A consequence of the the sequence based design is that only limited
< processing is performed until the last exposure of the sequence is
< completed.  This limits how quickly results are provided and depends on the
< number of exposures in the sequences, the cadence of the observations, and
< the hardware dedicated to the processing.  The pipeline is built on a
< distributed and parallel processing framework \citep{PL001} which allows
< scaling by adding additional computing resources.  The current pipeline runs
< on two dual-core machines (see figure X).  For programs with modest
< cadences, such as 60 second exposures, the current pipeline runs somewhat
< slower than real-time.  A key feature of NEWFIRM to keep the observing
< cadence (and data volume) reasonable is internal coadding.  This also
< benefits the QRP.  There are plans to increase the amount of processing
< during the sequence and to increase the computational resources.
< 
< The QRP is made aware of a new exposure through a file trigger event.  In
< the observing environment the trigger events are initiated by a post
< processing script executed by the DCA after the exposure readout has
< completed.  Two light-weight content files are written to the QRP trigger
< directory with the path to the actual exposure FITS file and the directory
< specified by the user for receiving data products.  The trigger event is a
< third file created by a "touch".  A separate zero-length trigger file is
< used to avoid the pipeline attempting to access the content of the files
< before the information is completely written.
< 
< All the files share a common base name provides sequence information to the
< pipeline.  An additional file may be written to indicate the exposure is the
< last in the sequence, though additional logic will also detect a change in
< the sequence identifier and assume the previous sequence has completed.  The
< logic for responding to various sequence failures, such as a premature end
< of sequence, was a challenge and a potential problem is signaling the end of
< the last sequence of the night when it is terminated prematurely.
---
> stacked images with bad pixels and mosaic gaps removed.  A consequence of
> the the sequence based design is that only limited processing is performed
> until the last exposure of the sequence is completed.  This limits how
> quickly results are provided and depends on the number of exposures in the
> sequences, the cadence of the observations, and the hardware dedicated to
> the processing.  The pipeline is built on a distributed and parallel
> processing framework \citep{PL001} which allows scaling by adding additional
> computing resources.  The current pipeline runs on two dual-core machines
> (see figure X).  There are plans to increase the amount of processing during
> the sequence and to increase the computational resources.
> 
> The QRP is made aware of a new exposure through a file trigger event
> initiated by a post processing of the DCA.  Two light-weight content files
> are written to the QRP trigger directory with the path to the FITS file and
> the directory specified by the user for receiving data products.  The
> trigger event is a third file created by a "touch".  A separate zero-length
> trigger file is used to avoid the pipeline attempting to access the content
> of the files before the information is completely written.
> An additional file may be written to indicate the exposure is the
> last in the sequence.
234,245c148,157
< The primary data product of the QRP are single images constructed from the
< sequence.  Because a scripted sequence is quite flexible there may be
< multiple dithers resulting in multiple stacks from a single sequence.  The
< pipeline detects and creates stacks based on automatically identified
< overlaps after astrometric calibration.  The individual images are dark
< subtracted, linearity corrected, and flat fielded.  The dark and flat field
< calibrations use master calibrations derived from dark and flat field
< sequences.  The QRP also processes these calibration sequences and stores
< them in a calibration library.  Note that in keeping with the goal of quick
< best effort reductions, these calibrations are optional and skipped if no
< suitable calibration sequences have been received, processed, and checked
< into a calibration library.
---
> The primary data product of the QRP are single stacked images constructed
> from the sequence.  The pipeline detects and creates stacks based on
> automatically identified overlaps after astrometric calibration.  The
> individual images are dark subtracted, linearity corrected, and flat
> fielded.  The dark and flat field calibrations use master calibrations
> derived from dark and flat field sequences.  The QRP also processes these
> calibration sequences and stores them in a calibration library.  Note that
> in keeping with the goal of quick best effort reductions, these calibrations
> are optional and skipped if no suitable calibration sequences have been
> received, processed, and checked into a calibration library.
255,257c167,168
< Each exposure is astrometrically calibrated by fitting a world
< coordinate system (WCS) function.  The function is determined using
< 2MASS sources automatically matched with detected sources in the
---
> Each exposure is astrometrically calibrated using
> 2MASS sources automatically detected sources in the
268,269c179
< limits the provided data quality measurements, such as a photometric zero
< point.
---
> the data quality does not include the 2MASS calibrated zero point.
274,275d183
< \subsection{Pipeline Data Products}
< 
278,280c186
< individual exposures if needed.  Depending on the sequence this can result
< in one or multiple stacked images.  As part of the DHS the user specifies a
< directory for the QRP to deposit image data.  These are FITS files which may
---
> individual exposures if needed.  The images are FITS files which may
295,305c201,209
< \subsection{Pipeline User Tools}
< 
< The user is not expected to control or interact with the QRP.  The pipeline
< is intended to run automatically with minimal technical support.  In fact,
< there are currently no resources for night-time maintenance of the QRP.
< Starting, stopping, and responding to problems is handled remotely by
< pipeline personnel.  However, users do like to know whether the pipeline is
< running.  Therefore commands are provided to check the status of the
< pipeline ({\tt plstatus}), showing whether the pipeline machines and
< pipeline processes are up and running, and sequences ({\tt qrpstatus}),
< showing at what stage of processing each sequence is at.
---
> The user is not expected to control or interact with the QRP which is
> designed to run automatically with minimal technical support.  There are
> currently no resources for night-time maintenance of the QRP and starting,
> stopping, and responding to problems is handled remotely by pipeline
> personnel.  However, users do like to know whether the pipeline is running.
> Therefore commands are provided to check the status of the pipeline ({\tt
> plstatus}), showing whether the pipeline machines and pipeline processes are
> up and running, and sequences ({\tt qrpstatus}), showing at what stage of
> processing each sequence is at.
314,317d217
< \bibitem[Daly(2008)]{Daly} Daly, P., et al, 2008, The
< NEWFIRM Observing Software: From Design To Implementation, Proc. SPIE,
< submitted as 7019-40
< 
326d225
< \end{document}
