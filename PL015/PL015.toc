\contentsline {section}{Introduction}{2}
\contentsline {section}{Data Quality Categories}{2}
\contentsline {section}{Mosaic Cameras, Resampling, and Stacking}{5}
\contentsline {section}{\numberline {1}Unsuitable for Science }{6}
\contentsline {subsection}{\numberline {1.1}Detector }{6}
\contentsline {subsubsection}{\numberline {1.1.1}bad pixels }{6}
\contentsline {subsubsection}{\numberline {1.1.2}saturation }{6}
\contentsline {subsubsection}{\numberline {1.1.3}pixels affected by saturation }{6}
\contentsline {subsubsection}{\numberline {1.1.4}persistence }{6}
\contentsline {subsubsection}{\numberline {1.1.5}non-photon events }{7}
\contentsline {subsection}{\numberline {1.2}Electronics }{7}
\contentsline {subsubsection}{\numberline {1.2.1}noise }{7}
\contentsline {subsubsection}{\numberline {1.2.2}dropouts }{7}
\contentsline {subsubsection}{\numberline {1.2.3}telemetry }{7}
\contentsline {subsubsection}{\numberline {1.2.4}edge effects }{8}
\contentsline {subsubsection}{\numberline {1.2.5}crosstalk }{8}
\contentsline {subsection}{\numberline {1.3}Calibration }{8}
\contentsline {subsubsection}{\numberline {1.3.1}uncalibratable }{8}
\contentsline {subsubsection}{\numberline {1.3.2}bad calibration data }{9}
\contentsline {subsubsection}{\numberline {1.3.3}no data }{9}
\contentsline {subsubsection}{\numberline {1.3.4}non-science }{9}
\contentsline {section}{\numberline {2}Advisory }{9}
\contentsline {subsection}{\numberline {2.1}Poor or Uncertain Calibration }{9}
\contentsline {subsubsection}{\numberline {2.1.1}bias }{10}
\contentsline {subsubsection}{\numberline {2.1.2}linearity }{10}
\contentsline {subsubsection}{\numberline {2.1.3}replacement }{10}
\contentsline {subsection}{\numberline {2.2}Model Subtraction }{11}
\contentsline {subsubsection}{\numberline {2.2.1}crosstalk }{11}
\contentsline {subsubsection}{\numberline {2.2.2}persistence }{11}
\contentsline {subsubsection}{\numberline {2.2.3}reflection }{11}
\contentsline {subsubsection}{\numberline {2.2.4}psf }{11}
\contentsline {subsubsection}{\numberline {2.2.5}model }{12}
\contentsline {subsection}{\numberline {2.3}Stacking }{12}
\contentsline {subsubsection}{\numberline {2.3.1}pixels rejected by data quality }{12}
\contentsline {subsubsection}{\numberline {2.3.2}pixels rejected by statistical clipping }{12}
\contentsline {section}{\numberline {3}Classification }{13}
\contentsline {subsection}{\numberline {3.1}False (Ghost) Sources }{13}
\contentsline {subsubsection}{\numberline {3.1.1}detector/electronic }{13}
\contentsline {subsubsection}{\numberline {3.1.2}Instrument reflections }{13}
\contentsline {subsubsection}{\numberline {3.1.3}Telescope/dome reflection (e.g. stray light, scattering) }{13}
\contentsline {subsection}{\numberline {3.2}Sources }{13}
\contentsline {subsubsection}{\numberline {3.2.1}source }{14}
\contentsline {subsubsection}{\numberline {3.2.2}background }{14}
\contentsline {subsubsection}{\numberline {3.2.3}stellar }{14}
\contentsline {subsubsection}{\numberline {3.2.4}non-stellar }{14}
\contentsline {subsubsection}{\numberline {3.2.5}transient: single exposure source }{14}
\contentsline {subsubsection}{\numberline {3.2.6}transient: variable/burst multiple exposures }{14}
\contentsline {subsubsection}{\numberline {3.2.7}moving: terrestrial }{14}
\contentsline {subsubsection}{\numberline {3.2.8}moving: solar system }{15}
