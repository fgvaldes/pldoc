\documentclass{dpp_doc}
\usepackage{graphicx,url,natbib}

\title{Pixel Data Quality Flag Values for NOAO Pipelines \\
Single Exposures}
\author{F. Valdes$^1$}
\otheraffiliation{
$^1$NOAO Data Products Program, P.O. Box 26732, Tucson, AZ 85732}
\pubhistory{Rough draft: August 2008}
\reportnumber{PL015}
\keywords{pipeline, bad pixels, data quality}
\runningtitle{Data Quality Flags: Single Exposures}

\begin{document}
\frontmatter

\tableofcontents
\newpage 

%\listoffigures
%\newpage

%\listoftables
%\newpage

\mainbody

\begin{abstract}

This (working) document defines the data quality flag values used by
the NOAO pipelines.  It presents a categorization of various pixel
data quality situations that pertain to single exposures.  This
includes exposures from mosaic cameras.

\end{abstract}


\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

Pixel data quality flags are a useful data product of the NOAO pipelines.
This document defines data quality categories for single exposures to be
used by the current NOAO pipeline applications: MOSAIC and NEWFIRM.  Though
it is intended for NOAO pipeline application developers, it may serve a role
in discussions within the wider community of developers and data providers
for defining some shared definitions of pixel data quality categories.
Prior to final user documentation for the NOAO pipeline applications, this
document also serves as a description of the data quality flags produced by
the NOAO pipeline applications.  Because of the multiple audiences the text
varies between being NOAO pipeline specific and attempting to be more
general.

In addition to defining the categories, with some discussion, this
document also defines the data quality values to be used in the final
pixel data quality mask files.  The categories can be mapped to bit
codes to allow multiple classifications, mapped to a full set of
codes, or mapped to a limited set of codes.

For the pipeline
applications we will use the smaller set of codes for the primary
categories.  For example code 3 will identify saturated and bleed
trail pixels.  Note that this document is structured such that the
section numbers are the same as the codes to be used.  The table of
contents and tables~\ref{tbl:badpixels}~and~\ref{tbl:advisory}
summarize the categories and codes.

\begin{table}
\caption{Pixel Categories: Unsuitable for Science}
\label{tbl:badpixels}
\begin{center}
\begin{tabular}{llll}

1& \multicolumn{3}{l}{Unsuitable for Science (i.e. bad)}	\\
   &1.1& \multicolumn{2}{l}{Detector}				\\
	&&1.1.1& bad pixels (e.g. bad columns, leds)		\\
	&&1.1.2& saturation (e.g bright stars)			\\
	&&1.1.3& pixels affected by saturation (e.g. bleeding)	\\
	&&1.1.4& persistence					\\
	&&1.1.5& non-photon events (e.g. cosmic rays)		\\
   &1.2& \multicolumn{2}{l}{Electronics}			\\
	&&1.2.1& crosstalk					\\
	&&1.2.2& noise						\\
	&&1.2.3& signal dropouts				\\
	&&1.2.4& edge effects					\\
   &1.3& \multicolumn{2}{l}{Calibration}			\\
	&&1.3.1& uncalibratable					\\
	&&1.3.2& bad calibration data				\\
	&&1.3.3& no data (e.g. masks, resampling, gaps)		\\

\end{tabular}
\end{center}
\end{table}

\begin{table}
\caption{Pixel Categories: Adisory and Classification}
\label{tbl:advisory}
\begin{center}
\begin{tabular}{llll}

2& \multicolumn{3}{l}{Advisory}					\\
   &2.1& \multicolumn{2}{l}{Poor or Uncertain Calibration}	\\
	&&2.1.1& bias						\\
	&&2.1.2& linearity					\\
   &2.2& \multicolumn{2}{l}{Model Subtraction}			\\
	&&2.2.1& crosstalk					\\
	&&2.2.2& persistence					\\
	&&2.2.3& reflection					\\
	&&2.2.4& psf (e.g. clean algorithm)			\\
	&&2.2.5& model (e.g. galaxy model )			\\
\\
\\
3& \multicolumn{3}{l}{Classification}				\\
   &3.1& \multicolumn{2}{l}{Optics and False Sources}		\\
	&&3.1.1& instrument reflection (e.g. filter)		\\
	&&3.1.2& telescope/dome reflection (e.g. stray light)	\\
   &3.2& \multicolumn{2}{l}{Sources}				\\
	&&3.2.1& source						\\
	&&3.2.2& background					\\
	&&3.2.3& stellar (e.g. star)				\\
	&&3.2.4& non-stellar (e.g. galaxy)			\\
	&&3.2.5& transient: single exposure source		\\
	&&3.2.6& transient: variable/burst multiple exposure	\\
	&&3.2.7& moving: terrestrial (e.g. airplane/satellite)	\\
	&&3.2.8& moving: solar system (e.g. asteroid)		\\

\end{tabular}
\end{center}
\end{table}

A consequence of using codes rather than bit flags is that a pixel can
only have one data quality value even if more than one category might
apply.  This requires defining a precedence.  For the NOAO pipeline
applications the precedence will be the lowest code value.  For
instance, a cosmic ray at the position of a pixel affected by
persistence will be identified as a cosmic ray pixel.

We emphasize that the categories and actual data quality values are
linked through a mapping.  Additional categories and new mappings,
possibly to bit-flags, may occur in the future.

\section*{Pixel Categories}
\addcontentsline{toc}{section}{Pixel Categories}

This document attempts to define general pixel categories applicable to a
variety of digital detectors.  The scheme is intended to be extensible as
new categories are identified.  One thing that is apparent is that there are
multiple ways to catgorize pixel properties and within one scheme one has a
choice of categories for the same effect.  The descriptions of the
categories define here note some of these alternative choices.

The categories are not intended to define data quality flags.  The data
provider defines a mapping between the categories and the flags.  The
mapping can be defined as either bit flags or code flags.  The mapping may
also define multiple data quality files; particularly based on the highest
level categories.

The categories are organized in a hierarchical way.  This allows grouping
similar types of categories for description and for contracting application
pixel flag values.  The highest level grouping highlights an important
distinction between pixels that the instrument and pipeline developers
consider not suitable for science and those which are left for the end-user
to use as they see fit.  In particular, if a pixel cannot be calibrated to a
satisfactory level it falls into the first category, if the calibration may
be suspect due to limitations in the calibration algorithm or subtraction of
a model was performed it falls into the second category, and if a pixel may
be included or excluded for calibration, combining, and analysis purposes it
falls into the third category.

Pixel flags are more than informational.  They are often used by calibration
and analysis applications.  The inclusion of a classification category and
the separation into bad and advisory categories is driven by the typical
application use cases.  The top level categories make it easier for
applications to filter common groups of data quality flags from a single
data quality flag file or to suggest use of multiple data quality files.

One common type of application excludes bad pixels.  Another type
combines images either for creating a sky image for dark sky
flats or for a deeper image.  In these application classification into
sources and sky or cosmic ray and transient are important.  Source
detection is also a common application.  Things in the second category
may produce faint residuals which could be mistaken for sources and so
the pixel flags can be used to flag source detections for greater scrutiny.

\section*{Mosaic Cameras and Resampling}
\addcontentsline{toc}{section}{Mosaic Cameras and Resampling}

Detectors, CCDs and IR arrays, by their nature generally produce fully
filled image rasters.  Some cameras are composed of multiple detectors
which tile the focal plane.  A common way to record an exposure is
as a collection of detector images; often stored in a multi-extension
FITS format (MEF) file.  In this representation each image has its
own pixel data quality flags which can be stored as a complementary
MEF file.  This is the case for the NOAO CCD and IR imagers.
The NOAO pipeline applications generally process mosaic data as separate
images but build final data products, including the data quality
flags, as MEF files.

In the case of carrying through a mosaic camera exposure as separate images
there is no difference between a single detector and a mosaic in terms of
the data quality flags.  However, if the mosaic exposure is rendered into a
single image, either by simple tiling or by geometric resampling, then then
there are two additional data quality flag considerations.

One of these is flagging where there is no data because of gaps between the
detectors and because the resampled geometry will not generally fill a
rectangular raster.  This adds another detector data quality flag as
described in the next section.

The other consideration is how to propagate pixel data quality values when
geometrically resampling.  In this case multiple input pixels contribute to
an output pixel through some resampling or interpolation algorithm.  There
is not unique way to associate a final data quality flag with the output.
The NOAO pipeline applications consider the data quality flags of all pixels
that overlap and choose the one based on the precedence rule mentioned
earlier.  Note that this does not take into account the fact that the
resampling algorithm may actually use a larger set of input pixels than just
those the overlap the output pixel area.

As an aside, the pipelines have an alternate way to propagate data quality
flags by resampling a data quality array as an image using the same
resampling algorithm as the flux image.  If all contributing pixels have no
data quality flags (a value of zero) then the result is also a value of
zero.  Otherwise, the result will typically be a non-zero value.  The
problem is that this value does not represent a particular data
quality category.  So what is done is map all input data quality flags to 
zero or one and then map the resampled data quality value to zero or
one based on some threshold value.  We chose the overlap method
because we want to provide some data quality discrimination in the
resampled images, for instance areas of saturation verses bad detector
pixels, and treat different classes of pixels differently when
stacking single exposures.

\section{Unsuitable for Science \label{unsuitable}}

This category identifies pixels which are not suitable for scientific
purposes.  These are often called "bad pixels".  The problems include lack
of flux, bad or non-linear responses, saturation, electronic noise, cosmic
rays, and generally not calibratable.

\subsection{Detector \label{detector}}

\subsubsection{bad pixels \label{badpixels}}

Bad pixels are those that have little or no response or relation to the
photon flux.  For CCDs these are things like bad columns or columns with
charge traps.  This would also describe LEDs.  In infra-red arrays these
include cracks in the substrate and regions that have been physically
modified to eliminate "glowing".

\subsubsection{saturation \label{saturation}}

Saturation occurs when the photon flux is high and the detector cannot
accumulate all the signal.  This makes the pixel values unusable and
uncalibratable.  Saturation is generally characterized by a detector
dependent threshold in ADU.

For some infra-red cameras, such as NEWFIRM, saturation is more
difficult to identify because of double correlated sampling.
As the photon flux increases the measured ADU increases
up to some value and then decreases.  At the highest flux the measured
ADU actually returns to zero.

\subsubsection{pixels affected by saturation \label{bloom}}

Saturation sometimes affects pixels which are near by even though the
photon flux at that point is below what would cause saturation.  There
are two common effects.   One is a spill-over of the pixel capacities.  
The other is a consequence of the way CCDs are read out.  The latter
effect produces what is sometimes called bleed trails.

\subsubsection{persistence \label{persist1}}

Persistence is a phenomenon that is also generally associated with
saturation.  Instead of affecting near by pixels in the exposure
it affects the pixels in subsequent exposures for some period of time.
This effect occurs in IR arrays.  In this case the physical effect
is an increase in the dark current for a period of time.  

The main effect is faint ghosts.  It is important to identify these to
avoid confusion as real sources.  In this respect it is similar to
crosstalk which also produces faint sources.  If the persistence
signal can be removed  it can be identified by category~\ref{persist21}
or \ref{persist22}.

\subsubsection{non-photon events \label{cosmicrays}}

These are pixels affected by transient events which are not produced
by photons from the sky.
The main class of non-photon events comes from high energy particles
interacting with the detector.  The most common type is from
cosmic rays and so this category will often be referred to as
cosmic ray events.  However, sometimes the particles come from
radioactivity in the terrestrial environment; particularly in the
detector, instrument, or telescope materials.

To fall into this class the algorithm needs to also distinguish that the
observed source is not an astronomical photon source.  This is generally
done by considering the shape and intensity of the source.

\subsection{Electronics \label{electronics}}

\subsubsection{crosstalk \label{xtalk1}}

Crosstalk is effect within a single exposure where an apparent signal
in one place is caused by actual signal in another place.  The
signature of this effect is typically ghosts of bright sources though,
in principle, flux at all levels produces crosstalk signal.

In many cases the crosstalk may be removed by a calibration procedure.
Typically this consists of scaling the source signal by a crosstalk
coefficient and subtracting it from the pixels affected by the
crosstalk.

If the crosstalk can be removed the pixels which are significantly
affected by crosstalk can be identified by category~\ref{xtalk21} or
\ref{xtalk22}.  Significantly affected might mean the pixels whose
crosstalk is produced by pixels with values above some ADU threshold.

When the crosstalk cannot be calibrated because the signal is not
functionally related tothe source signal it is identified by this
category.

\subsubsection{crosstalk \label{xtalk2}}

In the corrected subclass the false signal is removed; typically by scaling
the observed signal and subtracting it from where the crosstalk appears.
Because this correction will not be perfect the data quality flag can
indicated the affected pixels.  As noted previously, crosstalk affects
all pixels in principle.  However, the science problem is primarily
the crosstalk from bright sources which produce detectable false
crosstalk sources.  So this class would use a threshold for bright
sources to flag pixels where a significant crosstalk signal was
removed.

\subsubsection{noise \label{noise}}

When it is possible to identify pixels with unacceptably high
electronic noise they are assigned to this category.

\subsubsection{dropouts \label{dropouts}}

There are two types of signal dropouts.  One is when part or all of
the detector is turned off.  This would usually be to eliminate
effects on neighbor parts of the detector or mosaic.  For instance
to remove light from a strong LED behavior.  Another might be
unacceptibly high crosstalk.

The second type is sporadic dropouts.  These might be relatively easy to
identify in a pipeline because of ADU dropping below the bias level.

This category is closely related to the no data category (\ref{nodata}).
That category could be used though the physical source of no data is clearly
distinguishable.  One situation where it is a toss-up is when it is not the
electronics that is turned off but, instead, the component that writes the
image does not to write the digital values from the electronics but instead
writes a "blank" value.

\subsubsection{edge effects \label{edgeeffects}}

The electronics sometimes introduce unusable pixels at the edges of the
image.  One type of effect are transients as rows or columns are first
readout through the electronics.

Detectors that can shift signal, notably CCDs, can be used in modes where
the edge signal is shifted off the edge during part of the exposure.
Examples of this are scan tables, strip scanning, and detector guiding (e.g.
with orthogonal transfer architectures).  These edge effect can produce
either spurious edge effects or reduced effective exposure.  This category
can be used to identify these regions which are not considered calibratable
or suitable for science.  In effect the pixel flags act as a mask on the
detector.

\subsection{Calibration \label{calibration}}

\subsubsection{uncalibratable \label{uncalibratable}}

This is what the name implies.

These are pixels whose response is deemed of poor scientific value.  One
driver for this subclass is that they sometimes represent large or amorphous
regions where replacement by interpolation, often done for pixels in the
previous category, is undesirable.  Examples of this are corner regions in
the NOAO Mosaic camera detectors.

Note that saturation could fall into this
category.  However, using \ref{saturation} is more descriptive.

\subsubsection{bad calibration data \label{badcaldata}}

Some calibration methods depend on external calibration data.  If that
calibration data is uncertain or unknown for a particular pixel then
this data quality flag is used.  An example of this would be flat fields
with pixels that are not of sufficient quality.

\subsubsection{no data \label{nodata}}

The category indicates pixels which have no data for a variety of reasons.
This category might be used for all bad pixels though this is not
recommended.  It could used in place of the drop out category
(\ref{dropouts}); particularly when the reason is an intentional
turning off of the electronics or read out.

There are two cases where this category is specifically applicable.  One is
when the detector is not illuminated, such as by masking.  The detector will
likely still produce ADUs for these pixels and one could consider them to be
uncalibratable.  But this category is the most appropriate one for this
situation.

The other case is when the geometry of the detector does fill a nice image
raster.    It might be because of the shape of the detector though it is
very rare to have a non-rectangular digital array detector.  Much more
common is the case of geometric resampling or an single image which is
created from a mosaic of detectors by tiling or mosaicking.  Resampling to
change the geometry often results in no data at the edges due to fitting a
non-rectangular geometry into a rectangular image raster.  A mosaic of
detectors also generally includes gaps between the detectors.  The gaps
would be categorized as no data.

In applications it is useful to discriminate between no data and bad data.
Bad data may be replaced by interpolation for esthetic and algorithmic
reasons.  Whether replaced or not, combining algorithms benefit from knowing
when there is pixel values (bad or not) and when there is no data.

\section{Advisory \label{advisory}}

The "Advisory" category is for data quality flags


\subsection{Poor or Uncertain Calibration \label{poorcal}}

This category provides a way to identify pixels which some users might find
acceptable for their science and others might not.  It is a "buyer beware"
type of data quality condition.  It is up to the provider to document what
this means.  The rigorous way to convey this type of data quality is with
uncertainity arrays.  However, this category provides a simple threshold in
uncertainty which a provider can use when an uncertainty array is not
produced or a user can use when they don't want to deal with an uncertainty
array but want to exclude the most uncertain data.

Most pixel calibrations are either corrections to the zero point (bias) or
the response to light (linearity).  The bias may be electronic, dark signal,
or extra light such as from reflections.  In virtually all detectors there
is a first order gain calibration.  For some detectors, particularly
infra-red detectors, this are higher order "linearity" calibrations.  The
two categories defined in this section indicate possible problems with one
or the other of these types of calibrations.

As just noted, there is a catagory of bias problems due to undesirable
extraneous light.  The removal of this light typically consists of
a model or template for the light which is registered and scaled to
fit the data and then the fit is subtracted.  This type of process
may introduce artifacts or, even if done very well, the user may
wish to know which pixel were affected.  This is the purpose of
the model subtraction category.

In some situations, such as crosstalk, signal may be removed from all
pixels.  In this case it doesn't make sense to flag all pixels.  Instead
a threshold corresponding to a "significant" signal is used.

\subsubsection{bias \label{bias}}

This category identifies pixels with poor or uncertain bias removal.
This is anything that affects the zero point of the pixel values.
This could include electronic overscan, dark current, or other
background.  It is up to the provide to define the level of
uncertainity which should affect a modest subset of the pixels.

\subsubsection{linearity \label{linearity}}

A primary use case for this category is for non-linearity calibrations where
the correction is progressively more uncertain as the non-linearity
increases.  What this means is that a pipeline may define more than one
threshold for non-linearity; one where the signal is too high for a
reasonable calibration (see~\ref{uncalibratable}) and one where the
calibration uncertainty exceeds some value.

\subsection{Model Subtraction \label{model}}

A number of calibration and analysis steps involve subtracting artifacts or
sources.  Typically the steps are defining a model, registering
the spatially, and matching intensity.
Any errors in these steps will leave residuals which the flags in
these categories will idenitfy.  But even if the subtraction is
perfect it is still useful to advise users that some source light was
subtracted.

\subsubsection{crosstalk \label{xtalk22}}

Crosstalk is essentially electronic signal (not actual light) at a pixel
which is related to the position of sources elsewhere in the detector.  
When present this affects all pixels.  The
relationship is complex and sometimes it is not possible to remove this
signal.  In this case category~\ref{xtalk1} would be used.

When the crosstalk can be removed or greatly reduced this generally
consists of a model for the extra signal that is subtracted from the
data.  Where the crosstalk is significant, that is when the true
sources are bright (usually defined as above some ADU threshold)
and the false signal is above the background noise, the data provider
can indicate this with this data quality category.

\subsubsection{persistence \label{persist22}}

Persistence is residual detector signal, such as charge which is
not fully cleared or increased dark current, that is related to
preceeding exposures.  


\subsubsection{reflection \label{reflection22}}
\subsubsection{fringing \label{fringing22}}
\subsubsection{psf \label{psf}}

This category is more for certain specialized application.  The model
in this case is for point sources which are characterized by a
point-spread-function (PSF).  The applications are generally when
the sources of interest are faint and near bright stars.

\subsubsection{model \label{model}}

\section{Classification \label{classification}}

\subsection{Optics \label{optics}}

This class consists of problems which are due to optical effects which
cannot be removed by instrumental calibration.  The typical effects
are those from reflections, especially double reflections, within
the camera and scattering from the telescope or dome.

\subsubsection{Correctable patterns}

As noted above, pixels only need to be flagged if they cannot be
corrected.  If the correction is questionable they can be flagged
by the "poor calibration" category.  However, it may be desirable to
flag corrected pixels for some processing or documentary purpose.

For some filters in the NOAO Mosaic CCD imager on the Mayall telescope
there is a large pupil pattern in the data from reflections involving
the corrector.  Algorithms have been developed to approximately remove
this feature.  Because this feature is large and mostly correctable
the NOAO Mosaic pipeline does not flag these pixels.  Instead the
calibration data, a template of the pattern and the scaling factor,
are recorded for the end user.

\subsubsection{Instrument reflection (e.g. filter, corrector, dewar window)}

This category identifies reflection features which are not removed by
instrumental calibration.  For example, there are reflection rings
near bright stars for some filters in NEWFIRM data.  Currently the
NOAO NEWFIRM pipeline does not identify these but in the future they
may be, in which case they would be flagged by this category.

\subsubsection{Telescope/dome reflection (e.g. stray light, scattering)}

Bright stars sometimes cause various patterns of scattered or stray
light.  The stars may be in the field or outside the field.  Moon
light may also produce "glints" from scattering.

The NOAO telescopes and cameras do suffer scattered light from bright
stars, both in and out of the field.  These are unfortunate features
which often produce large rays of light over the field.  The NOAO
pipeline applications may flag these in the future.

\subsection{Sources \label{sources}}

This category is not precisely one of data quality but of separating
static sources from non-static sources.  It is related to data quality
in that flagging the non-static sources is required to produce the
best quality stacks of multiple exposures to obtain deeper imaging of
the static sky.

The affected pixels are typically found using difference detection
algorithms from multiple exposures of the same area; though other
algorithms are certainly possible.  The difference is either between
single exposures or, more commonly, a single exposure and a stack.

One class of data quality classification is between source and
non-source or background.  This is called segmentation and the
flags would correspond to a segmentation map.  

\subsubsection{source \label{source}}

Sources are defined by the segmentation algorithm and parameters.
This category would likely be used for simple segmentation maps.

ACE defines ...

\subsubsection{background \label{background}}

This defines pixels that are outside of sources where sources are defined by
the segmentation algorithm and parameters.  A category is defined here but
since this is the converse of the source category the non-source background
pixels would generally not be identified.

\subsubsection{stellar \label{stellar}}

This category would be used if source are classified as stellar and
non-stellar.  This classification really means sources that agree with
an adopted point-spread-function (PSF) and those that do not.

\subsubsection{non-stellar \label{non-stellar}}

\subsubsection{transient: single exposure source \label{transient1}}

Sources detected on a single exposure are most likely to be cosmic
rays.  However, they should be assigned this subcategory unless they
are classified by some algorithm that determines the source is
inconsistent with the PSF.  This means that initial detection using a
technique like difference imaging classifies these single exposure events
as bursts until some measure of sharpness is computed.

Depending on the time scales of the exposures, the other type of
source might be variables or novae.  The NOAO pipelines are not
intended for long time baselines but typically for dither sequences where
the overlapping exposures are either consecutive or on the same
night. So this type of source will be rare.

\subsubsection{transient: variable/burst multiple exposures \label{transient2}}

Obviously, this is a simple extension of the single exposure source category.
However, it has significant science content in that it helps to classify
the nature of the source.

\subsubsection{moving: terrestrial \label{streak}}

This is a category for sources detected as streaks which are long enough
that they would not be categorized as asteroids.  They would only occur in one
exposure, except possibly for persistence.  These are generally either
airplanes or Earth orbiting satellites.  Subcategories for these two types
of sources is possible though this seems to be useful only for very specific
applications.  Current work in the NOAO pipelines is expected to detect and
flag streaks.

\subsubsection{moving: solar system \label{asteroid}}

Asteroids may be flagged by their shape in a single exposure  or by
their motion or transient nature in multiple exposures. 

\appendix
\section{NOAO Pipeline Application Notes}

\subsection{Pixel value replacement}

We will replace the pixel values in the science exposures for all
categories except 1.2, 2.1, 5.3, 7.1, 8.3, and 8.4.

For the NOAO pipelines the first two subclasses have been defined primarily to
separate pixels to be replaced by interpolation during processing from
those which are not replaced.  Both subclasses are still considered to
be not scientifically useful and are mapped to a single bad pixel
code in the final data products.

\subsection{Cosmic rays}

Cosmic rays found by a cosmic ray specific algorithm, e.g. {\tt
crmedian}, should be categorized as 4.1 or 4 in the flat scheme.
Those found by difference detection, e.g. {\tt acediff}, should be
characterized as 8.2 or 8 in the flat scheme.  In either case they
will be masked in the second pass stack.

Table~/href{tbl:codes} defines the mapping of the data quality categories
to data quality flags for the NOAO Mosaic and NEWFIRM pipeline data
products.  The mapping is to a small number of codes rather than
bit-fields.  Therefore, a pixel may only have one value even if
multiple categories apply.  The order in the table sets the precedence
such that the earliest value that applies is used.

\begin{table}
\caption{NOAO Mosaic and NEWFIRM Data Quality Values}
\label{tbl:codes}
\begin{center}

\begin{minipage}[t]{2in}
\begin{tabular}{|c|c|c|}
\hline
Category	& Value	& Interp	\\ \hline \hline
1.1.1		& 1 	& Y		\\
1.1.2		& 3	& Y		\\
1.1.3		& 3	& Y		\\
1.1.4		& 4	& N		\\
1.1.5		& 5	&		\\
1.2.1		& 	&		\\
1.2.2		& 	&		\\
1.2.3		& 2	& N		\\
1.2.4		& 1	& Y		\\
1.3.1		& 1 	& N		\\
1.3.2		& 	&		\\
1.3.3		& 2	& N		\\ \hline
\end{tabular}
\end{minipage}
\begin{minipage}[t]{2in}
\begin{tabular}{|c|c|c|}
\hline
Category	& Value	& Interp	\\ \hline \hline
2.1.1		& 	&		\\
2.1.2		& 	&		\\
		& 	&		\\
2.2.1		& 	&		\\
2.2.2		& 	&		\\
2.2.3		& 	&		\\
2.2.4		& 	&		\\
2.4.5		& 	&		\\
		& 	&		\\
		& 	&		\\
		& 	&		\\
		& 	&		\\ \hline
\end{tabular}
\end{minipage}
\begin{minipage}[t]{2in}
\begin{tabular}{|c|c|c|}
\hline
Category	& Value	& Interp	\\ \hline \hline
3.1.1		& 6 	& N		\\
3.1.2		& 6 	& N		\\
		& 	&		\\
3.2.1		& 	&		\\
3.2.2		& 	&		\\
3.2.3		& 	&		\\
3.2.4		& 	&		\\
3.2.5		& 6	& N		\\
3.2.6		& 6 	& N		\\
3.2.7		& 6	& N		\\
3.2.8		& 6 	& N		\\
		& 	&		\\ \hline
\end{tabular}
\end{minipage}

\end{center}
\end{table}

\begin{thebibliography}{}
\bibitem[Valdes(2008)]{PL016} Valdes, F. 2008,
Pixel Data Quality Flag Values for NOAO Pipelines: Stacked Images,
NOAO DPP Document PL016, NOAO, Tucson, AZ
\end{thebibliography} 

\end{document}

The NOAO Mosaic pipeline flags saturation by a threshold obtained from
the calibration library.  
We are currently working on methods to
identify at least some of the saturated pixels in the NOAO NEWFIRM
pipeline.

For the Mosaic imagers the pipeline detects bleed trails, assigns a
data quality flag, and replaces the pixels by interpolation.  During
processing the two types of saturation effects are identified
separately but for the final data quality both direct saturation and bleed
trails are identify by the principle saturation flag.

Persistence is a problem with NEWFIRM.  We are currently exploring how
to handle this behavior.  It may be correctable but currently we plan
to flag pixels affected by persistence based on the sequence of
exposures.  The persistence effects will then be minimized or removed
in sets of data used for running sky subtraction and in dither stacks.

For the NOAO Mosaic CCD images the crosstalk is largely correctable.
However, for the data quality flags without subclasses we don't
provide corrected crosstalk identifications.

NOAO Mosaic CCD imagers
are largely correctable so we don't use this flag.  There is an NOAO
instrument, the WIYN Mini-mosaic camera, which does fall into this
category.

\subsection{noise}
One place
this might occur is with NEWFIRM where some "channels", which affect
32 pixel wide lines or columns are known to have high noise.  This was
the case in early data but currently noisy channels are not present or
mapped.

\subsubsection{uncalibratable}
We currently not have usage examples of 
this category though it might be used for NEWFIRM non-linearity
calibrations.
