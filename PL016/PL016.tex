\documentclass{dpp_doc}
\usepackage{graphicx,url,natbib}

\title{Pixel Data Quality Flag Values for NOAO Pipelines \\
Stacked Images}
\author{F. Valdes$^1$}
\otheraffiliation{
$^1$NOAO Data Products Program, P.O. Box 26732, Tucson, AZ 85732}
\pubhistory{Rough draft: August 2008}
\reportnumber{PL016}
\keywords{pipeline, bad pixels, data quality}
\runningtitle{Data Quality Flags: Stacked Images}

\begin{document}
\frontmatter

\tableofcontents
\newpage 

%\listoffigures
%\newpage

%\listoftables
%\newpage

\mainbody

\begin{abstract}

This (working) document defines the data quality flag values used by
the NOAO pipelines.  It presents a categorization of various pixel
data quality situations that pertain to stacked images.

\end{abstract}


\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

Pixel data quality flags are a useful data product of the NOAO pipelines.
This document defines data quality categories for stacked images to be
used by the current NOAO pipeline applications: MOSAIC and NEWFIRM.
Stacked images have somewhat different needs than single exposures.
Data quality categories for single images are given in \citet{PL015}.

Stacked images produce pixel values from overlapping single exposures.
The overlapping pixels may have a variety of data quality flags
ranging from good to bad and the resultant pixel value is generally
computed from only the pixels with no data quality flags.  The
criterion use here for the data quality of the result is that if at
least one good pixel contributed to the stack the stacked pixel data
quality is good.

Though this document is intended for the NOAO pipeline application
developers, it may serve a role in discussions within the wider community of
developers and data providers for defining some shared definitions of pixel
data quality categories.  Prior to final user documentation for the NOAO
pipeline applications this document will also functions as a description of
the data quality flags produced by the NOAO pipeline applications.  Because
of the multiple audiences the text varies between being NOAO pipeline
specific and attempting to be more general.

In addition to defining the categories, with some discussion, this
document also defines the data quality values to be used in the final
pixel data quality mask files.  The categories can be mapped to bit
codes to allow multiple classifications, mapped to a full set of
codes, or mapped to a limited set of codes.  For the NOAO pipeline
applications we will use the smaller set of codes for the primary
categories.  Note that this document is structured such that the
section numbers are the same as the codes to be used.  The table of
contents and table~\ref{tbl:codes} summarize the categories and codes.

A consequence of using codes rather than bit flags is that a pixel can
only have one data quality value even if more than one category might
apply.  This requires define a precedence.  For the NOAO pipeline
applications the precedence will be the lowest code value.

\begin{table}
\caption{DQ Flag Types and Codes for Stacked Images}
\label{tbl:codes}
\begin{center}
\begin{tabular}{lll}

1& \multicolumn{2}{l}{No data}	\\
2& \multicolumn{2}{l}{No good pixels}	\\
	&2.1& source saturation (e.g. saturated stars)	\\
	&2.2& mixture of bad data quality \\
	&2.3& all data rejected by combining algorithm \\
9& \multicolumn{2}{l}{Other}	\\

\end{tabular}
\end{center}
\end{table}

\section{No data}

The category of no data is when there is no pixel in any input exposures 
that observed the field position represented by the output pixel.  The
need for this class is a consequence of data being represented by
rectangular image rasters.  So any dithering and reprojecting of
exposures will inevitably need to fill some of the edges of the stack
with a "blank" value which is then given a "no data" data quality
flag.

\section{No good pixels}

This category is used when there is at least one pixel which contributed to
the output stacked pixel but all contributing pixels had data quality
flags which identify them as unsuitable for the stack.

While some subcategories are defined here, the NOAO pipeline
applications currently don't have the ability to categorize more than
that all input pixels were excluded for data quality or rejection
reasons.

\subsection{source saturation}

When the stack is produced from registered exposures the same sources
contribute to the same pixels except for PSF variations.  It is often
the case that very bright sources saturate the detector for all
exposures.  In this case it is useful to care through a data quality
flag indicating the source is saturated.

\subsection{mixture of bad data quality}

There can be situations where all pixels in the input have data
quality flags that result in no good data for the output.  If they
have a mixture of flag types or ones not defined b another category,
such as saturation, then the output pixel data quality flag falls into
this category.

\subsection{all data rejected by combining algorithm}

Stacking of data may be done using some kind of rejection based on the
sample of values at each pixel.  While most rejection algorithms will
not reject all values it is possible there may be cases where this
happens.  For the {\tt imcombine} task used by the NOAO pipelines
the only situation that can produce this is with the data threshold
parameters.  This situation does not occur with the NOAO pipeline
applications.

\setcounter{section}{8}
\section{Other}

This class provides a flag for identifying pixels that don't fall into
the other defined classes.  It is primarily a place to put new
classifications created during development of a pipeline prior to
expanding the data quality documentation.  It is also used when the
number of data quality attributes to be tracked exceeds the available
range of the data quality file format.

\appendix
\section{NOAO Pipeline Application Notes}

The NOAO pipeline applications rely primarily on IRAF's {\tt imcombine}
task for stacking.  This currently only supports the two
top level data quality flags for the stacks; no data and no good data.
We may explore changes to this task to pass through the input data
quality flags.  In particular, if all input data quality flags are of
the same value then the output could be that value.  In that case the
class of data quality values would merge, in some to be defined way,
with the flags for single resampled exposures \citep{PL015}.

\begin{thebibliography}{}
\bibitem[Valdes(2008)]{PL015} Valdes, F. 2008,
Pixel Data Quality Flag Values for NOAO Pipelines: Single Exposures,
NOAO DPP Document PL015, NOAO, Tucson, AZ
\end{thebibliography} 

\end{document}
