\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Algorithms}{2}
\contentsline {subsection}{\numberline {2.1}Pair Alignment Algorithm}{2}
\contentsline {subsection}{\numberline {2.2}Clustering}{3}
\contentsline {subsection}{\numberline {2.3}Hough Clustering}{3}
\contentsline {section}{\numberline {3}Implementation}{4}
\contentsline {subsection}{\numberline {3.1}Basic Tools}{4}
\contentsline {subsection}{\numberline {3.2}NOAO Wide-Field Imaging Pipelines}{4}
\contentsline {subsubsection}{\numberline {3.2.1}Moving Sources}{4}
\contentsline {section}{\numberline {4}Ellipticity}{6}
