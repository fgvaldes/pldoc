\contentsline {section}{Purpose of this Document}{2}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Concept of Operation}{3}
\contentsline {section}{\numberline {3}Selection of the Best Calibration}{4}
\contentsline {section}{\numberline {4}The Catalog}{4}
\contentsline {section}{\numberline {5}File Repository}{5}
\contentsline {section}{\numberline {6}Data Transport}{7}
\contentsline {section}{\numberline {7}Protocol}{7}
\contentsline {section}{\numberline {8}Request for a Calibration}{8}
\contentsline {subsection}{\numberline {8.1}The Request Options}{8}
\contentsline {subsection}{\numberline {8.2}Calibration Selection}{8}
\contentsline {subsection}{\numberline {8.3}The Return from a Calibration Request}{9}
\contentsline {section}{\numberline {9}Entering a Calibration}{10}
\contentsline {section}{\numberline {10}The Pipeline Interface Commands}{11}
\contentsline {subsection}{\numberline {10.1}getcal}{11}
\contentsline {subsection}{\numberline {10.2}putcal}{12}
\contentsline {section}{\numberline {11}The Software}{12}
\contentsline {section}{\numberline {12}Auxillary Tools}{13}
\contentsline {section}{\numberline {13}Server-less Pipeline Situations}{13}
