\contentsline {section}{Abstract}{1}
\contentsline {section}{Purpose of this Document}{2}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}The Demonstration Use Case}{4}
\contentsline {section}{\numberline {3}The ODI Demonstration Data }{4}
\contentsline {section}{\numberline {4}The Demonstration Pipeline}{5}
\contentsline {subsection}{\numberline {4.1}The Science Processing Modules}{6}
\contentsline {subsection}{\numberline {4.2}The NHPPS Pipelines}{7}
\contentsline {subsection}{\numberline {4.3}The OGCE Pipeline Services}{8}
\contentsline {section}{\numberline {5}The Demonstration Results}{8}
\contentsline {section}{\numberline {A}Figures }{11}
