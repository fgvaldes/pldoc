\documentclass{sdm_doc}
\usepackage{graphicx,url,natbib}

\title{The ODI Demonstration Tier 1 Pipeline}
\author{F. Valdes$^1$}
\otheraffiliation{
$^1$NOAO Data Products Program, P.O. Box 26732, Tucson, AZ 85732}
\pubhistory{V1.0: September 25, 2010}
\reportnumber{PL021}
\keywords{pipeline, ODI, NHPPS, OGCE, Teragrid}
\runningtitle{ODI Demonstration Pipeline}

\begin{document}
\frontmatter

\tableofcontents
\newpage 

\listoffigures
\newpage

%\listoftables
%\newpage

\mainbody

\addcontentsline{toc}{section}{Abstract}
\begin{abstract}

An ODI tier 1 demonstration workflow, performing basic flux calibration and
dither stacking of ODI data. is described.  One purpose for this
demonstration is to show processing of actual ODI format data under the
execution frameworks selected for ODI tier 1 processing.  Another purpose is
to highlight application of IRAF science processing tools for both
interactive users and pipeline modules.  In particular, to demonstrate IRAF
science modules running in a Teragrid environment.  This document describes
the simulated ODI data, processing workflow, high level structure of the
pipeline implementation, and results.

\end{abstract}

\section*{Purpose of this Document}
\addcontentsline{toc}{section}{Purpose of this Document}

The purpose of this document is to describe the elements of the ODI
demonstration tier 1 pipeline to astronomers and workflow scientists.  These
elements include the data being processed, the workflow being demonstrated,
the processing tools used to build a pipeline, the high level implementation
architecture, and the output results of the pipeline.  Since a pipeline is
essentially a "black-box" architecture, a real-time demonstration has little
visual information other than monitoring tools indicating the state of the
processing.  Therefore, this document provides a more astronomical and data
oriented view of the pipeline, particularly with the figures showing the
input and output image data as astronomers are used to visualizing them.

\newpage

\section{Introduction}

This document describes an ODI (One-Degree Imager) workflow demonstration
providing basic flux calibration of ODI format data and creation of a final
image with detector and cell gaps removed by dithering.  This workflow is
representative of the planned ODI tier 1 pipeline.  The "tier 1" qualifier,
which primarily means standard instrumental calibration and stacking, will
be dropped in the remainder of this document.  In this document we present a
description of the ODI demonstration data, the processing steps performed, a
high level outline of the demonstration pipeline structure, and the results
of running the pipeline on the demonstration ODI data.  This description
goes along with an available real-time demonstration.

Readers, particularly astronomers, may wish to go directly to
appendix~\ref{FigureAppendix} for the figures showing the raw and processed
data. As the adage goes -- \emph{a picture is worth a thousand words}.  The
structure and content of this simulated data is described in
\S\ref{DemonstrationData}.

The demonstration provides proofs of concept and capability in the
following areas:

\begin{itemize}
\item the creation of an ODI IRAF data reduction package
\item use of IRAF for an ODI workflow in an NHPPS/local cluster context
\item use of IRAF for an ODI workflow in an OGCE/NHPPS/Teragrid context
\item an NHPPS ODI pipeline application
\item an OGCE/NHPPS ODI pipeline application
\end{itemize}

IRAF is a well-known, highly capable and widely used astronomical data
reduction system.  The rapid development of a initial ODI package for IRAF,
one specifically designed to handle the unique orthogonal transfer arrays
(OTA) in the ODI data format, is significant and demonstrates why IRAF is a
good choice for the underlying ODI science processing component.  It
also provides a standard path to distributing user-oriented processing tools
to ODI users.

Use of IRAF in NOAO High Performance Pipeline System (NHPPS) pipeline
applications is already well demonstrated by existing production pipelines
for NOAO mosaic instruments.  These applications run on local clusters at
NOAO.  The proof of concept embodied in this ODI demonstration is use of
IRAF in a Teragrid environment.

Similarly an NHPPS ODI pipeline application is a clearly achievable goal in
as much as ODI is simply a much larger mosaic camera than existing NOAO
instruments.  The special features of the orthogonal transfer array (OTA)
detectors only add modest requirements. The NHPPS version of the
demonstration pipeline shows the ability to handle the OTA format.  This
provides a low-risk fallback to the more ambitious OGCE/Teragrid
version as well as a high performance development and test capability.

Finally, demonstrating the packaging an NHPPS pipeline application in the
Open Grid Computing Environment (OGCE) framework as a workflow of services
running in a Teragrid environment provides a real proof of concept for the
ultimate goal of an ODI science pipeline using the full resources of the
Teragrid.

\section{The Demonstration Use Case}

The use case to be demonstrated is taking a set of raw ODI science exposures
from a dither sequence (multiple exposure with the telescope slightly offset
between exposures), applying standard instrumental flux calibrations (bias
removal and sensitivity calibration), and combining them to produce a final
image of the field of view with instrument gaps filled in.

Prerequisites for this case are master bias and dome flat calibration data
available from a calibration library.  This demonstration by-passes
astrometric calibration by arranging that after flux calibration the
exposure data are correctly registered apart from simple integer offsets.
In other words, the steps of coordinate calibration and remapping to a
common sky sampling is implied but not included in the demonstration.

\section{The ODI Demonstration Data \label{DemonstrationData}}

The demonstration data is based on the current understanding of the expected
ODI data format and basic instrumental characteristics.  An ODI exposure
consists of 64 files, one per orthogonal charge transfer (OTA) detector.
The files are multiextension format (MEF) FITS files with a data-less
primary header containing global metadata (i.e. keywords) and 64 image
extensions for the "cells" making up the OTA.  The cell images are 624x608
pixels.  The first 590x598 pixels are actual detector pixels, the last 34
columns and last 10 rows are electronic overscan.  The active pixel regions
are physically related by the structure of the OTA to having gaps of 18
columns and 10 rows.  This physical relation has the consequence that a
single image, with cell gaps, can be constructed without further geometric
calibration or metrology.

Figure~\ref{cell.eps} shows a cell image including the overscan regions.
The pixel data are simply noise in both the data and overscan regions.
Figure~\ref{zero00.eps} illustrates the layout of the cells in an OTA.  The
64 OTAs are arranged in an 8x8 grid.

The demonstration data is produced using Poisson noise, electronic overscan
and zero bias levels, and cell amplifier sensitivity variations.  The bias
and sensitivity levels include slopes to mimic non-constant spatial
structure.  The bias and sensitivity variations are drawn from a random
distribution.  For signal a background rate (i.e. a lamp for flat fields or
the sky for science data) is specified along with an exposure time.  Finally
a source sky scene, provided as an image, may be used.  In the demonstration
science data a deep stack from the KPNO Mosaic Imager is used as the sky
scene.  Two exposures are simulated using the same scene but with a dither
offset between them.

The bias and flat field data are generated with instrumental signatures and
then processed with the IRAF ODI package to make master calibrations.
Figure~\ref{zero00.eps} shows a single OTA from a raw bias calibration.  A
calibrated bias version, which is no shown, would look much the same but
with the mean bias differences between cells removed.
Figure~\ref{Flat00.eps} shows a calibrated OTA from the master flat field.
This shows the dominant effect of different amplifier responses from each
cell.

A view of four OTAs from a single raw science exposure is given in
figure~\ref{quadrawA.eps}.  This is one of the science exposures used
in the demonstration pipeline processing.

The sample ODI data can be created with a full complement of OTAs.
But for the purposes of the demonstration a 2x2 grid of OTAs (a
quad-OTA or QUOTA format) is created.  This also makes display of the
data, as in the figures presented here, sensible.

\section{The Demonstration Pipeline}

The ODI demonstration pipeline application consists of three NHPPS pipelines
which are executed as four OGCE services. Before describing the IRAF
modules, the NHPPS pipelines, and the OGCE services we start with a brief
explanation of the architecture.

We begin with individual host-callable programs called modules.  These
modules are generally pipeline specific scripts using a general underlying
language and tools.  For example IRAF is both a scripting language and a
large set of astronomical image processing tools.  Modules could also be
Python or Unix shell scripts.  The modules are typically of two kinds; those
that orchestrate or organize data, for example by grouping or separating
data for parallel processing, and those that do some kind of astronomical or
instrumental transformation or analysis on the data.

The modules are connected together by an execution framework.  If we don't
consider scripting languages as execution frameworks there are two used in
our architecture.

The first runs a set of modules, called a sub-pipeline or
simply pipeline, on a single node to perform some higher level function such
as calibrating a single piece of data.  To provide workflow logic with
asynchronous and parallel execution of the modules, we use the NOAO High
Performance Pipeline System (NHPPS).  NHPPS also supports distributed
processing on a local cluster which provides a fallback and special
processing capability.  Figure~\ref{fig:nhpps} shows a block diagram
of the demonstration pipeline in terms of NHPPS pipelines.

However, to use the resources of the Teragrid, with its complexities of
scheduling and management, a second execution framework, the Open Grid
Computing Environment (OGCE), is added.  It executes the NHPPS pipelines and
connects them together across a distributed grid computing network.
Figure~\ref{fig:ogce} shows how the the NHPPS pipelines are wrapped as
OGCE services and two components which handle parallelization.

\subsection{The Science Processing Modules}

We now turn to the actual content of the demonstration pipeline in this
architecture.  As noted earlier, there are various orchestration steps
which are basically implementation details.  The important elements of the
pipelines are three IRAF science modules described below.

\begin{description}
\item[otafluxcal]

This module operates on a single OTA from a single exposure.  It
accesses the matching master bias and flat field calibration files.  It
performs the following calibration steps:

\begin{itemize}
\item for each cell collapse the overscan to a one dimensional vector,
fit a function, evaluate the function for each row and subtract that
value from all pixels in the row
\item for each cell trim the overscan corrected pixels to just the data region
\item for each cell subtract the matching master bias calibration cell
\item for each cell divide by the matching master flat field cell
\item produce a calibrated MEF version of the raw data
calibration cell
\end{itemize}

\noindent While the operations are described separately, they are performed
in a single pass through the raw file.  An optimized IRAF task from the IRAF
ODI package is used within an IRAF host-callable script.

\item[otamerge]

This module converts the calibrated MEF format to a simple single
image.  Each cell raster is added to the output image at the correct
location as known from the detector structure.  The gaps are filled
in with a blank value.  This is also an IRAF host-callable script using
an optimized IRAF task from the ODI package.

\item[stkstack]

This module takes all the OTA images from the dithered exposures and
creates the output simple single stacked image.  The pixels are
combined by appropriate scaling and averaging while taking the offsets and
gaps into account.  This is also an IRAF host-callable script using a
standard IRAF technique with a task from the ODI package.

\end{description}

\subsection{The NHPPS Pipelines}

The total workflow is structured into three NHPPS pipelines described
below.

\begin{description}
\item[dit] The dither pipeline takes a list of ODI dither exposures.
It sends all the OTAs to parallel instances of the OTA calibration pipeline.
It also extracts the references to the calibration data from the headers and
includes the appropriate URIs in the data sent to the OTA pipelines.
After all the OTAs are calibrated and returned it sends the calibrated OTA
data to the dither stacking pipeline. (Note data is not explicitly
sent; instead URIs are passed.  It is up to the modules and
execution frameworks to provide the appropriate access.)
\item[ota] The OTA pipeline processes a single raw OTA file.  In the
demonstration it applies basic flux calibration and merges the
calibrated cells into a single OTA image as described previously.  
\item[stk] The dither stacking pipeline takes a list of single image
OTA images which have header information defining the dither offsets.
The images are put together into a single image of the field with
overlapping pixels averaged.
\end{description}

Figure~\ref{fig:nhpps} illustrates the workflow as an NHPPS pipeline
application using these pipelines.  Implicit in this figure is that within
the pipelines NHPPS can execute multiple datasets and different modules in
parallel (like process level threads) on a node.  Also the arrows indicate a
one-to-many relation such that the many are distributed across nodes.  So in
the demonstration pipeline a group of OTAs (say four) can be processed on a
multiple core node and the full set of OTAs, broken up in the small
groups, can be distributed in parallel across multiple nodes.

\begin{figure}[p]
\begin{center}
\setlength{\unitlength}{0.4in}
\begin{picture}(8,3)
\thicklines
\put(1,0){\framebox(2,1){ota}}
\put(5,0){\framebox(2,1){stk}}
\put(2,2){\framebox(4,1){dit}}
\put(2,1){\vector(3,2){1.5}}
\put(6,1){\vector(-3,2){1.5}}
\put(3.5,2){\vector(-3,-2){1.5}}
\put(4.5,2){\vector(3,-2){1.5}}
\put(0,2){\makebox(2,1)[l]{input}}
\put(6,2){\makebox(2,1)[r]{output}}
\put(1,2.5){\vector(1,0){0.9}}
\put(6.1,2.5){\vector(1,0){0.8}}
\end{picture}
\end{center}
\caption{NHPPS pipeline tree. The connections between pipelines
are one-to-many which includes one-to-one.
\label{fig:nhpps}}
\end{figure}

\subsection{The OGCE Pipeline Services}

The OGCE pipeline services wrap all or parts of the NHPPS pipelines.
The four OGCE pipeline services convert the hierarchical pipeline
structure of the NHPPS pipelines into a directed acyclic graph (DAG).
Since NHPPS pipelines are typically connected in a hierarchical
fashion this means that pipelines that connect to other pipelines and
wait for a return are separated at those points into OGCE services;
hence the different number of services.  The services and their
relation to the NHPPS pipelines are described next.

\begin{description}
\item[dit\_to\_ota] Execute the first part of the dit pipeline up to calling
the ota pipeline.
\item[ota\_calibrate] Run one or more instances of the ota pipeline.
\item[dit\_to\_stk] Take the results from the ota\_calibrate service and
input them to the dither stacking pipeline.
\item[stack\_dither] Take the results of the ota\_calibrate service and
create the dither stack.
\end{description}

Figure~\ref{fig:ogce} shows the OGCE services wrapping the NHPPS pipelines.
First note that to convert the hierarchical tree-like structure of the NHPPS
workflow into a directed acyclic graph, the NHPPS pipelines are split
wherever double-ended arrow transitions to other pipelines occur.  Within
the NHPPS pipelines the parallelism on a single node is still present.  The
parallelism across many nodes, in this case the Teragrid, is implemented by
OGCE components\footnotemark[1] that expand and merge the workflow based on output data from
the service to which it is connected.

\footnotetext[1]{The current demonstration does not use the
parallelization components.  Instead, the OTA stage
internally processes all the pieces in parallel on one node.}

\begin{figure}[p]
\begin{center}
\setlength{\unitlength}{0.4in}
\begin{picture}(9,6.2)

\thicklines
\put(1.7,4){\framebox(2.2,1.6)[t]{dit\_to\_ota}}
\put(1.8,4.1){\thinlines\dashbox{0.1}(2,1){dit}}
\put(1.8,4.1){\thinlines\line(1,0){2}}
\put(1.8,5.1){\thinlines\line(1,0){2}}
\put(1.8,4.1){\thinlines\line(0,1){1}}

\put(4.1,4){\framebox(2.2,1.6)[t]{dit\_to\_stk}}
\put(4.2,4.1){\thinlines\dashbox{0.1}(2,1){dit}}
\put(4.2,4.1){\thinlines\line(1,0){2}}
\put(4.2,5.1){\thinlines\line(1,0){2}}
\put(6.2,4.1){\thinlines\line(0,1){1}}

\put(1,0){\framebox(2.2,1.6)[t]{ota\_calibrate}}
\put(1.1,0.1){\thinlines\framebox(2,1){ota}}

\put(5,0){\framebox(2.2,1.6)[t]{stack\_dither}}
\put(5.1,0.1){\thinlines\framebox(2,1){stk}}

\put(2.4,2.8){\circle{2.5}}
\put(1.8,2.7){foreach}

\put(4.2,2.8){\circle{2.5}}
\put(3.7,2.7){merge}

\put(2.8,3.9){\vector(-1,-3){0.13}}
\put(2.21,2.08){\vector(-1,-3){0.13}}

\put(3.3,0.8){\vector(1,2){0.65}}
\put(4.55,3.45){\vector(1,2){0.25}}

\put(5.3,3.9){\vector(1,-3){0.73}}

\put(0,4){\makebox(2,1)[l]{input}}
\put(1,4.5){\vector(1,0){0.6}}

\put(7,0){\makebox(2,1)[r]{output}}
\put(7.3,0.5){\vector(1,0){0.6}}

\end{picture}
\end{center}
\caption{OGCE pipeline DAG.  The NHPPS dit pipeline is broken in two and the
OGCE foreach and merge components provide the distributed parallelization
and merging.
\label{fig:ogce}}
\end{figure}

\section{The Demonstration Results}

The workflow implemented by the modules, pipelines, and services
just described begins with a dither sequence list of raw ODI science exposures,
specified by the root name of the multiple OTA MEF files per exposure.
The calibration data associated with each is exposure is
defined in the headers of the science exposures.  In a production system
this assignment of calibrations would be handled by pipelines and services
that interact with a calibration library.

The end goal of the demonstration is to show these representative raw ODI
dithered science exposures processed into final science images.  There
are two types of data products produced by the demonstration pipeline.
One is the instrumentally calibrated version of each single exposure.
Figure~\ref{quadrawA.eps} shows one such calibrated exposure.

The other data product is a single "dither stack" image of all the input
exposures with the gaps between cells and OTAs filled in.  This data product
from the two dithered science exposures is presented in figure~\ref{stack.eps}.

As noted in section~\ref{DemonstrationData}, it is possible to create
data with a full complement of OTAs.  Similarly, the demonstration
pipeline is capable of processing such data.  However, in this paper
and in the live demonstration we limit the input and results to a QUOTA scale
dataset.


\newpage

% Use bib files if possible.
\bibliography{PLSeries}
\bibliographystyle{abbrv}

% Otherwise use:
%\begin{thebibliography}{}
%\bibitem[Author(DATE)]{REF} Author, A., et al., DATE, PUB, 1234, 56
%\end{thebibliography} 

\newpage

\appendix

\section{Figures \label{FigureAppendix}}

Note that the figures (except \ref{cell.eps}) are at much lower resolution
than the data.  This means fine detail is lost or minimized and there can be
some minor aliasing artifacts.  Also while the figures show a single
picture, this does not mean that the actual data is necessarily structured
as a single image.  A display tool is used that tiles the pieces of the data
format to produce the visualization shown.

\begin{figure}[h]
\begin{center}
\includegraphics{cell.eps}
\end{center}
\caption{OTA cell (624x608 pixels) showing data and overscan regions.
\label{cell.eps}}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=6.5in]{zero00.eps}
\end{center}
\caption{A single OTA (4846x4854 pixels) from a single raw bias exposure.
The overscan regions are not shown but the differing bias levels are
reflected in the cell levels.  A calibrated bias would appear as pure noise
around a uniform level near zero.
\label{zero00.eps}}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=6.5in]{Flat00.eps}
\end{center}
\caption{A single OTA (4846x4854 pixels) from a master flat field
calibration.  The variations between the cells are due to
sensitivity differences in the amplifiers for each cell.  The
amplitude of these differences is such that the grayscale stretch
does not show the pixel noise.  A raw flat field exposure would not
appear different because the overscan and bias levels would also be
lost in the grayscale rendering.
\label{Flat00.eps}}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=6.5in]{quadrawA.eps}
\end{center}
\caption{Four OTAs (9692x9708 pixels) from a raw science exposure. The
principle variations between cells are due to the sensitivity differences
which are calibrated by flat fielding.
\label{quadrawA.eps}}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=6.5in]{quadcalA.eps}
\end{center}
\caption{Four OTAs (9692x9708 pixels) from a calibrated science exposure.
The calibrations include overscan and zero bias subtraction and flat field
normalization.
\label{quadcalA.eps}}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=6.5in]{stack.eps}
\end{center}
\caption{A dither stack (9922x9938 pixels) using just four OTAs from two
calibrated science exposures.  A full ODI field would be sixteen times
bigger.  The rectangular dark regions are places where a two exposure dither
is insufficient.  Dither sets typically have four or more exposures to fill
in the entire field except at the edges.  The grayscale stretch is the
same as in figure~\ref{quadcalA.eps} for comparison.
\label{stack.eps}}
\end{figure}

\end{document}
