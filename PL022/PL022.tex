\documentclass{../latex/sdm_doc}
\usepackage{graphicx,url,natbib}

\title{Note on Processing of ODI OTA Guided Data}
\author{
F. Valdes$^1$}
\otheraffiliation{
$^1$NOAO Data Products Program, P.O. Box 26732, Tucson, AZ 85732}
\pubhistory{V1.0: September 29, 2010}
\reportnumber{PL022-Note}
\keywords{pipeline, ODI, OTA}
\runningtitle{OTA Guiding}

\begin{document}
\frontmatter

%\tableofcontents
%\newpage 

%\listoffigures
%\newpage

%\listoftables
%\newpage

\mainbody

%\begin{abstract}
%
%ABSTRACT
%
%\end{abstract}

%\section*{Purpose of this Document}
%\addcontentsline{toc}{section}{Purpose of this Document}
%
%PURPOSE OR ADD OTHER section* SECTIONS.
%
%\newpage

\section{Introduction}

The detector guiding capability of the orthogonal transfer arrays (OTA) is an important feature of the One Degree Imager (ODI).  Because of its novelty many people are uncertain about what special processing is required to calibrate guided data.  They are concerned that this involves significant new software and algorithms over standard vertical transfer CCDs.  This note briefly describes the guiding modes and the steps that need to be taken  with the goal to dispel these concerns about the difficulties involved.

To summarize how OTA guiding works, one to many "guide stars" in the field
of view are read out rapidly and the positions are measured using some
centering algorithm.  The first set of position measurments acts as the
reference and the subsequent position changes are used to shift the charges
in the pixels back to the reference position.  The history of shifts is
record with the exposure for use in the subsequent instrumental processing.
The way the guide stars may be used by the system define guiding modes.
There are three basic modes:

\begin{description}
\item[static:]
guide stars are not used and no charge shifting takes place during the
exposure
\item[coherent:]
the guide star shifts are averaged and all pixels in the exposure are
identifially shifted
\item[local:]
different sets of guide stars define shifts for different parts of the
exposure
\begin{description}
\item[ota:]
all pixels in a single OTA are shifted identically
\item[cell:]
subsets of cells in an OTA are shifted independently
\end{description}
\end{description}

In the following discussion we will not discuss the static mode since the
calibration of such exposures is identical to the standard CCD calibrations.

The only calibration steps where the guiding must be taken into account are
the application of basic calibration images and the effects of multiple
guiding regions in the world coordinate system determination.  The following
sections describe these and demonstrate that very simple modifications to
the standard processing and pipeline workflow are required.  There may still
be new instrumental effects related to guiding discovered or the author may
be mistaken in this analysis, but in the absence of the unknown, the known
effects are not a matter for concern.

\section{Pixel Calibrations}

The charge shifting has the effect that the final charge read out to produce
a pixel value has a history of the characteristics of each pixel visited by
the charge.  Since each exposure has a unique guiding history the cumulative
characteristic for the output pixel is different for each exposure.  To
create calibrations matching each exposure, static calibration images --
biases, dome flat fields, fringe templates, dark sky flat fields, etc. --
are numerically shifted using the shift history for each exposure.  The
different guiding modes simply means that the shift histories are applied
coherently or locally in the same way as the science data.  This unique
version of the calibration is then applied in the usual way.

This numerical shifting is equivalent to a convolution.  Therefore,
the unique step required to apply bias and flat field calibrations is
simply to take the appropriate static calibration data, convolve it
using the shift history, and then apply it in the standard way.

The development of a convolution program making use of the shift
histories recorded with an observation is a straightforward
programming problem.  Other than doing this in a computationally
efficient manner there is no mystery and the amount of additional
developement for a pipeline is small.

The pipeline workflow is modified by the simple addition of the
convolution step prior to the standard application of bias and flat
field frames.  The static mode can be handled in a simple way either
by a control flow option to skip the convolution stage or to make the
convolution program leave the static calibration unchanged.

\subsection{Binned Exposures}

A minor varient of this is the case of exposures that are binned on
read out.  This requires that the static calibrations be unbinned.
They are then convolved as before and then digitally binned to match
the binning of the target exposure.  A tool to bin image data already
exists and there is no additional novelty.

\section{World Coordinate System Calibration}

The thing about the guiding is that the locations of the sources have been
offset from where they would have been in the absence of guiding to the
position of the first speckle (the term for an image of the guide star taken
with a very short exposure) in the ensemble of guide stars used.  With a
single coherent guiding applied over the entire detector the offset is
trivially part of the WCS tangent point determination.  In other words, it
is part of determining the telescope pointing.

It is when there is more than one independent shift, i.e. local guiding,
that the potential for a "rubber sheet" distortion is introduced.  Such
stretching would cause problems in the functional representation of the
world coordinate system being defined unless the stretch is first removed.

Local OTA guiding, where entire OTAs are shifted coherently, is also
relatively simple to handle.  The shift history is processed to determine
the average offset of the OTA exposure relative to the initial speckle
reference.  In this local case different OTAs may have different offsets.
Applying the average offset to the coordinate reference pixel for the OTA
(the position of the common tangent point for the WCS relative to the first
pixel of the OTA) is mathematically equivalent to shifting the position of
the OTA back to where it should be after averaging out the details of the
speckle motion.  After applying these offsets to the reference pixel values
the WCS calibration proceeds normally.

The last mode of local cell guiding, where different sets of cells are
shifted independently, is the only subtle case.  As with the
local OTA guiding, the goal is to determine the offset from the first
speckle to the average that would have occurred without guiding.  The
question is then how to "shift" the pixels to remove the offset.

In the other modes, after the overscan is extracted for each cell image and
used to remove the electronic bias, the multiple recorded cell images are
tiled into a single image using the known geometry of the cells on the OTA
detector.  When this image is constructed for the case of local cell
guiding, the different offsets for each group of cells is applied in the
reconstruction.  The arrangement of cells will then not look like the
physical structure of the OTA cells but the source positions will be as
they would have been without guiding; i.e. with no "rubber sheet distortion.
The WCS calibration again can proceed as usual.

Note that we use the nearest integer offset in the tiling to avoid
interpolation effects.  One may worry about the remaining fraction of a
pixel.  In the author's opinion, applying an algorithm to fractionally shift an image
degrades the resolution, that one is attempting to maximize by guiding, more
than this small remainder.

Figure~\ref{fig:guiding} shows a sketch of what is happening with the local
cell guiding and the resulting correction.  The cell shifts are exagerated
for illustration.

The software needed to implement these steps prior to the
determination of the WCS are also trivial.  In fact, no new software
is required because header adjustment or tiling offsets are already
part of the existing tools used in the absence of guiding.

\begin{figure}[h]
\begin{center}
\includegraphics[width=3in]{Guiding.eps}
\end{center}
\caption{Sketch of the local cell guiding.  The top boxes represent a
set of cells with two independent guiding cells.  The dot indicates
the position of the first speckle and the random walk the shift
history.  The shift history is purposely show with the first speckle
clearly offset from the ensemble position of the exposure.  The bottom
panels illustrate the retiling of the OTA cell images into a single
OTA image where the sources will be located where they would be in the
absence of guiding.
\label{fig:guiding}}
\end{figure}

\subsection{Pixel Binning}

The logic for this type of data is the same and is largely a matter of
coordinate system conversions.

%\newpage
%
%% Use bib files if possible.
%\bibliography{PLSeries}
%\bibliographystyle{abbrv}
%
%% Otherwise use:
%%\begin{thebibliography}{}
%%\bibitem[Author(DATE)]{REF} Author, A., et al., DATE, PUB, 1234, 56
%%\end{thebibliography} 

\end{document}
