\documentclass[11pt,twoside]{article}
\usepackage{asp2010}

\resetcounters

\bibliographystyle{asp2010}

\markboth{Valdes and Marru}{Marriage of NHPPS and OGCE}

\begin{document}

\title{The Marriage of Mario (NHPPS) and Luigi (OGCE)}
\author{Francisco~Valdes$^1$, Suresh~Marru$^2$}
\affil{
$^1$NOAO Science Data Management, P.O. Box 26732, Tucson, AZ 85732\\
$^2$Pervasive Technology Institue, Indiana University, Bloomington, IN
47408}

\begin{abstract}

Pipeline systems, like the plumbing characters of video game fame,
have their strengths and weaknesses. This is generally due to the
different niches they occupy in the landscape of pipeline systems.
This paper describes a marriage between two pipeline systems, NHPPS
and OGCE, which capitalizes on combining their strengths.

People frequently discuss interchange of application elements between
pipeline systems.  In other words, extracting some pipeline pieces
(modules and algorithms) orchestrated by one pipeline system for
orchestration by another. However, pipeline systems themselves are
applications in their own right.  This paper describes an interesting
architectural marriage where one pipeline system orchestrates another
pipeline system to take advantage of the strengths and niches of both.
We point out this concept in general terms since there may be other
pipeline marriages possible, but describe a specific example that
forms the basis of a major new pipeline application for the WIYN
One-Degree Imager (ODI).

The NOAO High Performance Pipeline System (NHPPS), in use for many
years at NOAO, is good at orchestrating legacy software, IRAF
specifically, into multi-process workflows that effectively make use
of a dedicated cluster of multi-core machines without needing to write
explicit threaded applications.  The Open Grid Computing Environment
(OGCE) is designed to wrap and manage applications into Teragrid
workflows.  For the ODI project we will wrap single-node NHPPS
pipelines into OGCE services for orchestration into distributed
Teragrid workflows.


\end{abstract}

\section{Introduction}

The WIYN Observatory is building a forefront mosaic camera with a one-degree
field of view, hence the name One-Degree Imager (ODI), and a resolution 0.1
arcsec per pixel for its 3.5 meter telescope \citep{2002SPIE.4836..217J}.
This gigapixel camera means observers will obtain large amounts of data
which most will be unable to store, calibrate, and analyze with their own
resources.  Therefore, Indiana University (IU) and NOAO have partnered to
develop a Pipeline, Portal, and Archive (PPA) system to support this
instrument.  In this contribution we describe the plans and synergies within
this partnership for developing an ODI calibration pipeline.
pipeline.  This pipeline will be a marriage of two pipeline or workflow
system,

Before describing this marriage we mention a few things about the IU/NOAO
partnership.  The collaborating groups are the Pervasive Technology
Institute (PTI) at IU and the Science Data Management (SDM) group at NOAO.
PTI is an information technology group while SDM is a traditional
astronomical data management group.  The differences in expertise between
these groups is one of the features of the partnership.  SDM brings
experience with pipeline processing of astronomical image data similar to
ODI and managing a principle investigator and community archive.  PTI brings
experience with the IT problems of data transport, storage, computations on
a large scale as well multidisciplinary science gateways.  The blending of
expertise is needed to address the larger volume of ODI data than other
current NOAO instruments with the astronomical needs of the ODI users.

Though we don't have space to describe other aspects of the PPA project one
additional goal is to provide higher level workflows, driven by the
observers, on the standard data products.  This concept is called Tier~2 and
Tier~3 and will again make use of the data processing expertise of NOAO/SDM
and the IT expertise of PTI.  It is possible that parts of the solution for
the Tier~1 pipeline will useful in the higher (more PI-driven) workflows.

The marriage we describe here is between the NOAO High Performance Pipeline
System (NHPPS) and the Open Grid Computing Environment (OGCE).  At NOAO the
NHPPS is a product of the "Mario" project, named for the video game plumber.
To complement this tongue-in-check description we associate OGCE with Luigi,
another video game plumber.  In the following sections we describe the
personalities of Mario and Luigi and then how their strengths are combined
in a complementary manner, a marriage, for an ODI Tier~1 pipeline.

\section{NOAO High Performance Pipeline System (NHPPS)}

The NOAO High Performance Pipeline System (NHPPS) has been described in
previous ADASS contributions \citep{2007ASPC..376..265S} and in a more
detailed paper \citep{PL001}.  In summary, NHPPS is an event-driven executor
of host commands (aka modules) where any number of these may be run at the
same time.  Because it manages many processes in an asynchronous manner the
compute resources, such as multiple cores, can be efficiently utilized by
the operating system even when the processes are not threaded or MPI
applications.

NOAO uses this framework to build pipeline applications consisting of a
number of pipeline services.  A pipeline service is a sequence of possibly
parallel steps running on one type of data object and on a single node to
perform a piece of the overall application.  These pipeline services
interact with each other in a workflow.  The workflow can be on a single
node or distributed across peer nodes in a dedicated cluster. In NOAO
applications the individual steps in the pipeline services consist primarily
of IRAF-shell scripts calling standard IRAF data processing tools.  One of
the main advantages of NHPPS in these applications is the ability to run
many of these IRAF modules in parallel to fully utilize compute resources.

In a high performance application the pipeline services are deployed on a
set of compute nodes.   The workflow is structured into a hierarchy such
that higher level pipeline services operate on larger data objects and call
multiple instances of lower level services to process smaller data
objects in parallel.  The calling pipeline service waits for the results of
the lower level pipelines.  This type of workflow structure is generally
known as Map/Reduce.

To understand the marriage a key point to understand is that the NHPPS
framework is basically a server, called the Node Manager, which runs
on each compute node and responds to events by executing modules on
particular dataset objects.  The efficiency occurs because it manages
many processes at the same time so that operating system can keep the
CPU cores which it operates busy.

When NHPPS runs as a distributed pipeline system the Node Managers
communicate with each other and manage multiple instances of the same
pipeline or different pipelines on different nodes depending on the
configuration design.

\section{Open Grid Computing Environment (OGCE)}

The Open Grid Computing Environment (OGCE) is a collection of middleware
that execute host commands, wrapped as web services, deployed on the
TeraGrid (or potentially any grid) and orchestrated a workflow application.
There are a variety of components and aspects of OGCE which are described in
\citet{OGCE}.

A key point is that OGCE does not do any workflow orchestration on a node.
So primary it launches a single host command on a grid node.  The command
may be multi-threaded, an MPI application, a simple shell script, etc.  So
the utilization of the node resources, such as multiple cores, resides in
the application.  It also does not facilitate any communication between
applications and nodes except for the input and output parameters of the web
services.

\section{The Marriage}

The fundamental technical question for the NOAO/IU partners with regards to
the Tier~1 pipeline was how NOAO developed science pipeline software could
be integrated with the IU grid workflow system.

A primary consideration was to utilize the NOAO data reduction and pipeline
expertise and to capitalize on the large body of already developed software.
This means use of the IRAF toolkit and NOAO pipeline applications.  This
leads to using IRAF-shell modules with reuse of modules from similar NOAO
pipeline applications and addition of new modules as needed.  It also means
using the same high performance map/reduce workflow approach.

A secondary consideration was to have the potential to run the pipeline
application on both the TeraGrid and on a dedicated cluster of machines.
This provides a development framework, a fallback, and increased flexibility.
This again leads to using the NOAO pipeline applications model.

The first step was to develop an ODI data flow design \citep{PL013} which
identifies the map/reduce strategies and estimates for the number of modules
(\~150) and pipeline services (\~20).  Then the first integration concept
was to use the individual modules in an OGCE workflow.  After studying this
option there were a number of problems.  These include the large number of
modules, the heterogeneity of function and parameters, and the inefficiency
in deploying many light-weight modules as grid jobs.

After further though came the "epiphany" that NHPPS pipeline services
would satisfy all of the considerations.  The number of services is
relatively small, the services have consistent input and output
parameters, are not too-fine grained and efficiently utilize node
resources, and are easily orchestrated both by the dedicated cluster
framework of NHPPS and the TeraGrid framework of OGCE.

The main challenge was that a pipeline service uses NHPPS as the
execution framework.  Normally the NHPPS framework is a daemon on the
nodes where the pipeline services are deployed.  However, it was
realized that with some minor changes a captive NHPPS framework can be
started for one or more pipeline services within a single host application.
This host application is then wrapped as an OGCE service.

So here is the core of this contribution.  The marriage of NHPPS with
OGCE consists of wrapping NHPPS pipeline services as single applications.
Because these pipeline services have a standard structure a single basic
wrapper is all that is needed.  This wrapper accepts input parameters
and produces output parameters (which include status) that OGCE
services expect.  The wrapper also takes care of starting the NHPPS
framework, supplying the triggers that start the pipeline, wait for
the pipeline to complete, and shutdown the framework.

For OGCE, one web service wrapper is created for each of the pipeline
service.  These web services are then orchestrated by OGCE as a Grid
workflow.  OGCE provides the workflow monitoring and collecting and managing
of the pipeline input and output in addition to the typical submission and
security aspects of a Grid application.

There was one other detail to resolve.  An NHPPS pipeline service
implements the map/reduce structure by calling other pipelines and
waiting for results.  The OGCE workflows must be described by a
directed acyclic graph (DAG) which means there is no "return" to
a service.  Without details, the solution was to automatically split
pipeline services at their call and return steps and promote the
list of call requests and the collecting of map results to the OGCE
workflow description in between the split pipeline services.

An organizational benefit of this design is that the interface, and
resulting division of work, between NOAO and IU is simple and clear.
NOAO can concentrate on the NHPPS pipelines knowing the wrapper
strategy is easy and clear and IU can concentrate on the map/reduce
workflow with little need to know about the modules and algorithms.

\bibliography{O12_1}

\end{document}
