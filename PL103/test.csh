#!/bin/csh -f

if (0) then
sqlite3 callib.db << EOF
drop table callib;
create table callib (
fileref		char,
caltype		char,
mjdobs		float,
dmjdlow		float,
dmjdhigh	float,
otaid		char,
ccdbin1		int,
ccdbin2		int,
filter		char,
exptime		float,
plquality	float,
opquality	float,
createtime	timestamp
);
EOF

sqlite3 callib.db << EOF
insert into callib values (
'file1','dflat',365.,10.,10.,'ota00',1,1,'V',3.,3.14,0.,'2011-11-14T12:00:00');
insert into callib values (
'file2','dflat',366.,10.,10.,'ota00',1,1,'V',3.,3.19,0.,'2011-11-14T12:10:00');
EOF
endif

set caltype = 'dflat'
set mjd = 360.
set ota = 'ota00'
set exptime = 2.
set dexptime = 1.
set dmjd = 100.5
set filter = 'V'
set w1 = 10.
set w2 = 1.
set w3 = 1.
set w4 = 0.

sqlite3 callib.db << EOF
select fileref, opquality, plquality,
    abs(${mjd}-mjdobs) as dmjd, abs(${exptime}-exptime) as dt,
    (${w1}*opquality+${w2}*plquality-${w3}*abs(${mjd}-mjdobs)-${w4}*abs(${exptime}-exptime)) as q
from callib
where caltype='$caltype'
and (mjdobs-${mjd}) <= min (dmjdlow, ${dmjd})
and (${mjd}-mjdobs) <= min (dmjdhigh, ${dmjd})
and filter='$filter'
and abs(${exptime}-exptime) <= $dexptime
and opquality >= 0.
order by q DESC
;
EOF
