\documentclass{../latex/sdm_doc}
\usepackage{graphicx,url,natbib,alltt}

\title{The ODI Pipeline Scheduling Agent ICD}
\author{F. Valdes$^1$ and R. Swaters$^1$}
\otheraffiliation{
$^1$NOAO Science Data Management, P.O. Box 26732, Tucson, AZ 85732}
\pubhistory{DRAFT December 12, 2011}
\reportnumber{PL104/CDR-46}
%\reportnumber{CDR-46}
\keywords{ODI, PSA}
\runningtitle{The ODI Pipeline Scheduling Agent ICD}

\begin{document}
\frontmatter

%\tableofcontents
%\newpage 

%\listoftables
%\newpage

\listoffigures
%\newpage

\mainbody

%\begin{abstract}

%\end{abstract}

%\section*{Purposes of this Document}
%\addcontentsline{toc}{section}{Purpose of this Document}

%\newpage

\section{Introduction}

The Pipeline Scheduling Agent (PSA) is the element of the PPA which manages
the submission and resubmission of data to be processed by the Automatic
Calibration Pipeline (AuCaP).  Managing means defining sets of exposures to
process, defining when they are processed, and keeping track of what has
been processed and archived.

The key concepts are \emph{datasets} and \emph{subsets}.  Both of
these refer to a set of ODI exposures with the latter being a subset
of the former.  A dataset is generally defined as all exposures from
one or more nights.  A subset is generally defined as exposures which
are logically independent of each other either by type of exposure --
calibration, static science, guided science -- or by filter.

Datasets are defined externally by a pipeline scientist based on the
telescope schedule.  Subsets are defined by the calibration planner
as a dataset is broken down into a sequence of workflows.

Datasets and subsets are managed using a database called the Pipeline
Scheduling Queue Database (PSQDB).  There are two tables corresponding
to datasets and subsets.  Initial datasets are entered by the pipeline
scientist, subsets are entered by messages from the data engine, and
additional "reprocessing" datasets are generated as needed by the
operator.

The PSA submits datasets to be processed to the data engine.  Datasets are
submitted in three ways; automatically based on the time the exposures in
the dataset are available, automatically when datasets to be processed are
pending, and manually by a request from the operator.  The PSA acts as a
processing throttle in that it normally limits processing to a single
dataset at a time though the operator can override this.

A design feature is that reprocessing is controlled at the subset
level.  It is typically the case that only some subsets of a dataset
fail for some reason.  By the definition of a subset, it is possible
to reprocess it independently of other subsets.  So it would be
inefficient to reprocess an entire dataset just to reprocess a
subset.


\section{Database Schema}

Figure~\ref{fig:PSQDBschema} shows the database schema for the dataset
and subset tables.  The database engine used is not critical and specific
schema details may depend on the engine.

\begin{figure}[p]
\caption{PSQDB database schema. \label{fig:PSQDBschema}}
\begin{alltt}
\scriptsize
CREATE TABLE DATASETS (
    dataset     char(32),
    nights      varchar(512),
    procafter   char(19),
    status      char(16)        DEFAULT 'pending',
    priority    int             DEFAULT 1,
    submitted   char(19),
    completed   char(19),
    comments    varchar(512),
    PRIMARY KEY (dataset, procid)
    );

CREATE TABLE SUBSETS (
    dataset     char(32),
    subset      char(32) CHARACTER SET binary,
    procid      char(16),
    status      char(16),
    submitted   char(19),
    completed   char(19),
    archived    char(19),
    comments    varchar(512),
    PRIMARY KEY (dataset, procid, subset)
    );
\end{alltt}
\end{figure}

\subsection{Examples}

\begin{figure}[p]
\caption{DATASETS Table: Example record before processing.}
\begin{alltt}
\scriptsize
dataset   = 20111031
nights    = 2011-10-31,2011-11-01,2011-11-02
procafter = 2011-11-02T12:00:00
status    = pending
priority  = 1
procid    = NULL
submitted = NULL
completed = NULL
comments  = Example
\end{alltt}
\end{figure}


\begin{figure}[p]
\caption{DATASETS Table: Example record after processing.}
\begin{alltt}
\scriptsize
dataset   = 20111031
nights    = 2011-10-31,2011-11-01,2011-11-02
procafter = 2011-11-02T12:00:00
status    = completed
priority  = 1
procid    = 7decd3a  
submitted = 2011-11-15T21:10
completed = 2011-11-16T03:30
comments  = Example
\end{alltt}
\end{figure}

\begin{figure}[p]
\caption{SUBSETS Table: Example records from subsets.}
\scriptsize
\begin{alltt}
dataset   = 20111031  
subset    = Bias     
procid    = 7decd3a  
status    = archived
submitted = 2011-11-04T09:50:47 
completed = 2011-11-04T10:05:25 
archived  = 2011-11-04T12:19:44 
comments  = NULL       
 
dataset   = 20111031  
subset    = BF       
procid    = 7decd3a  
status    = archived
submitted = 2011-11-04T10:05:30 
completed = 2011-11-04T10:20:04 
archived  = 2011-11-04T12:21:22 
comments  = NULL       
 
dataset   = 20111031  
subset    = B        
procid    = 7decd3a  
status    = archived
submitted = 2011-11-15T15:34:56 
completed = 2011-11-15T18:19:07 
archived  = 2011-11-18T22:59:55 
comments  = NULL       
 
dataset   = 20111031  
subset    = BS       
procid    = 7decd3a  
status    = reprocess
submitted = 2011-11-15T18:49:09 
completed = 2011-11-15T19:12:55 
archived  = NULL
comments  = Reprocess without applying a sky flat.
\end{alltt}
\end{figure}

\begin{figure}[p]
\caption{DATASETS and SUBSETS Tables: Reprocessing.}

\vspace{1pc}  When a subset is marked as \texttt{reprocess} a new dataset
record is created.  When it is run a new process ID is generated.  Below is
an example during reprocessing.

\scriptsize
\begin{alltt}
dataset   = 20111031
nights    = 2011-10-31,2011-11-01,2011-11-02
procafter = 2011-11-02T12:00:00
status    = submitted
priority  = 1
procid    = 7decff1  
submitted = 2011-11-17T11:10
completed = NULL
comments  = Example. B filter reprocessing.

dataset   = 20111031  
subset    = BS        
procid    = 7decff1  
status    = processing
submitted = 2011-11-17T11:34:56 
completed = NULL
archived  = NULL
comments  = Reprocess without applying a sky flat.       
\end{alltt}
\end{figure}

\clearpage

\section{Messages}

Communication between the data engine, and potentially other
components, is through the PPA messaging system.  In this section we
illustrate messages generically.

Figure~\ref{fig:msgDataset} shows the principle PSA-DataEngine messages for
submitting a dataset as a set of nights, for receiving a completion message,
and for reprocessing subsets.  The subset names are those previously defined
by the calibration planner.  The \texttt{replace} part of the message can be
a little confusing.  Reprocessing can occur both before archiving and after
archiving.  In the former case, the results are provisional and the data
engine is first notified that the provisional results can be deleted prior
to reprocessing.  In the latter case, the results of previous processing
were accepted for archiving and are in the mass storage system of the PPA.
The replace request is used to override the default behavior of the data
engine that previously completed and archived data, that is raw exposures
which have associated archived data products, be skipped.  The objective is
that the provisional buffering of data products for acceptance by the
operator will make replacing archived data a rare event.

Figure~\ref{fig:msgArchive} illustrates messages to the data engine to
accept or reject provisional data products.

Figure~\ref{fig:msgSubset} shows messages dealing with subsets.

\begin{figure}[p]
\caption{Dataset-level messages \label{fig:msgDataset}}
\begin{alltt}
\scriptsize
MESSAGE PSA -> DATA ENGINE: # Submit a set of nights.
    time      = 2011-11-04:09:40:10
    dataset   = 20111031_7decd3a
    procid    = 7dec3a
    nights    = 2011-10-31,2011-11-01,2011-11-02
    subsets   = *
    replace   = no

MESSAGE DATA ENGINE -> PSA: # All processing done with some failures.
    time      = 2011-11-04T10:06:30 
    dataset   = 20111031_7decd3a
    status    = incomplete

MESSAGE PSA -> DATA ENGINE: # Resubmission for reprocessing of subsets
    time      = 2011-11-06T10:06:30 
    dataset   = 20111031_7decff1
    procid    = 7decff1
    nights    = 2011-10-31,2011-11-01,2011-11-02
    subsets   = BS,VS,RS
    replace   = no
\end{alltt}
\end{figure}

\begin{figure}[p]
\caption{Subset accept and archive messages \label{fig:msgArchive}}
\begin{alltt}
\scriptsize
MESSAGE PSA -> DATA ENGINE: # Accept and archive
    time      = 2011-11-05T10:06:30 
    dataset   = 20111031_7decff1-calB
    archive   = yes

MESSAGE PSA -> DATA ENGINE: # Reject
    time      = 2011-11-04T12:05:21 
    dataset   = 20111031_7decff1-ftrBS
    archive   = no
\end{alltt}
\end{figure}

\begin{figure}[p]
\caption{Subset-level messages \label{fig:msgSubset}}
\begin{alltt}
\scriptsize
MESSAGE DATA ENGINE -> PSA: # Bias workflow started.
    time      = 2011-11-04T09:50:47 
    dataset   = 20111031_7decd3a-calB
    status    = submitted

MESSAGE DATA ENGINE -> PSA: # Bias workflow completed.
    time      = 2011-11-04T10:05:25 
    dataset   = 20111031_7decd3a-calB
    status    = completed

MESSAGE DATA ENGINE -> PSA: # B flat workflow started.
    time      = 2011-11-04T10:05:30 
    dataset   = 20111031_7decd3a-calBF
    status    = submitted

MESSAGE DATA ENGINE -> PSA: # B flat workflow failed.
    time      = 2011-11-04T10:05:30 
    dataset   = 20111031_7decd3a-calBF
    status    = failed
\end{alltt}
\end{figure}

%\newpage
%
% Use bib files if possible.
%\bibliography{PLSeries,CDR}
%\bibliographystyle{abbrv}

% Otherwise use:
%\begin{thebibliography}{}
%\bibitem[Author(DATE)]{REF} Author, A., et al., DATE, PUB, 1234, 56
%\end{thebibliography} 

\end{document}
